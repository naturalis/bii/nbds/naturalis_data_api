package nl.naturalis.nba.rest.resource;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import org.apache.logging.log4j.Logger;
import nl.naturalis.nba.api.BatchQueryResult;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.dao.MultiMediaObjectDao;
import nl.naturalis.nba.dao.NbaDao;
import nl.naturalis.nba.dao.SpecimenDao;
import nl.naturalis.nba.dao.TaxonDao;
import nl.naturalis.nba.rest.exception.HTTP404Exception;
import nl.naturalis.nba.rest.exception.RESTException;
import nl.naturalis.nba.rest.util.HttpQuerySpecBuilder;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static nl.naturalis.nba.dao.DaoUtil.getLogger;
import static nl.naturalis.nba.rest.util.ResourceUtil.JSON_CONTENT_TYPE;
import static nl.naturalis.nba.rest.util.ResourceUtil.handleError;

@Stateless
@LocalBean
@Path("/{type:(specimen|multimedia|taxon)}/batchQuery")
public class BatchQueryMixInResource {

  private static final String TOKEN = "_token";

  private static final String ERR0 =
      "%s parameter \"%s\" cannot be combined with other form or query parameters";

  private static final Logger logger = getLogger(BatchQueryMixInResource.class);

  @Context private UriInfo uriInfo;

  public BatchQueryMixInResource() {
    super();
  }

  @GET
  @Produces(JSON_CONTENT_TYPE)
  @Path("/")
  public BatchQueryResult<?> batchQuery(@PathParam("type") String type) {
    logger.info("Receiving batchQuery request for {} index", type);
    try {
      MultivaluedMap<String, String> params = uriInfo.getQueryParameters();
      if (params.containsKey(TOKEN)) {
        boolean ok =
            params
                .keySet()
                .stream()
                .filter(p -> !(p.equals(TOKEN) || p.startsWith("__")))
                .findFirst()
                .isEmpty();
        if (ok) {
          return dao(type).batchQuery(params.getFirst(TOKEN));
        }
        throw new RESTException(uriInfo, BAD_REQUEST, String.format(ERR0, "query", TOKEN));
      }
      QuerySpec qs = new HttpQuerySpecBuilder(uriInfo).build();
      return dao(type).batchQuery(qs);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  @Path("/")
  public BatchQueryResult<?> batchQueryPost(
      @PathParam("type") String type, MultivaluedMap<String, String> params) {
    try {
      if (params.containsKey(TOKEN)) {
        boolean ok =
            params
                .keySet()
                .stream()
                .filter(p -> !(p.equals(TOKEN) || p.startsWith("__")))
                .findFirst()
                .isEmpty();
        ok = ok && uriInfo.getQueryParameters().isEmpty();
        if (ok) {
          return dao(type).batchQuery(params.getFirst(TOKEN));
        }
        throw new RESTException(uriInfo, BAD_REQUEST, String.format(ERR0, "form", TOKEN));
      }
      QuerySpec qs = new HttpQuerySpecBuilder(params, uriInfo).build();
      return dao(type).batchQuery(qs);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  @Path("/")
  public BatchQueryResult<?> batchQuery(
      QuerySpec qs, @PathParam("type") String type, @Context UriInfo uriInfo) {
    try {
      return dao(type).batchQuery(qs);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  private NbaDao<?> dao(String type) {
    switch (type) {
      case "specimen":
        return new SpecimenDao();
      case "multimedia":
        return new MultiMediaObjectDao();
      case "taxon":
        return new TaxonDao();
    }
    throw new HTTP404Exception(uriInfo, "Invalid document type: " + type);
  }
}
