package nl.naturalis.nba.rest.util;

import static nl.naturalis.log.LogImpl.LOG4J;

import java.io.ByteArrayOutputStream;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.log.appenders.LiveLogStream;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.common.json.JsonDeserializationException;
import nl.naturalis.nba.rest.exception.HTTP400Exception;
import nl.naturalis.nba.rest.exception.RESTException;
import org.apache.commons.text.StringEscapeUtils;

public class ResourceUtil {

  /**
   * The JSON media type including the charset specification: "application/json;charset=UTF-8". Some
   * (older) browsers and REST clients seem to require the explicit charset specification in order
   * to process the JSON response properly. Therefore, resource methods should not {@link Produces
   * produce} {@link MediaType#APPLICATION_JSON}, since that constant does not include the charset
   * specification.
   */
  public static final String JSON_CONTENT_TYPE = "application/json;charset=UTF-8";

  /**
   * The NDJSON format (also called Newline delimited JSON) is a convenient format for storing or
   * streaming structured data that may be processed one record at a time. Its specs ({@link
   * Produces <a href="https://github.com/ndjson/ndjson-spec#33-mediatype-and-file-extensions">...</a>}) define the
   * media-type as: "application/x-ndjson". We have chosen to follow the specs, although there
   * appears to be some discussion over whether the type shouldn't be defined as:
   * "application/ndjson".
   */
  public static final String NDJSON_CONTENT_TYPE = "application/x-ndjson;charset=UTF-8";

  public static final String TEXT_CONTENT_TYPE = "text/plain;charset=UTF-8";

  public static final String ZIP_CONTENT_TYPE = "application/zip";

  private ResourceUtil() {
    throw new IllegalStateException("Utility class");
  }

  public static RESTException handleError(UriInfo request, Throwable throwable) {
    if (throwable instanceof JsonDeserializationException) {
      return new HTTP400Exception(request, ExceptionMethods.getRootCause(throwable));
    } else if (throwable instanceof RESTException) {
      return (RESTException) throwable;
    } else if (throwable instanceof InvalidQueryException) {
      return new HTTP400Exception(request, throwable);
    }
    return new RESTException(request, ExceptionMethods.getRootCause(throwable));
  }

  /**
   * Quotes and escapes the specified {@link String} so it becomes a JSON string value. Strangely,
   * when resource methods that {@link Produces produce} application/json return a string,
   * Wildfly/RESTeasy does not automatically JSONify the string. Hence, this method was added to
   * do just that.
   *
   * @param s - ...
   * @return String
   */
  @SuppressWarnings("unused")
  public static String stringAsJson(String s) {
    return "\"" + StringEscapeUtils.escapeJson(s) + "\"";
  }

  public static boolean isExplainRequest(UriInfo uriInfo) {
    String val = uriInfo.getQueryParameters().getFirst("__explain");
    return val != null && (val.isEmpty() || val.equalsIgnoreCase("true"));
  }

  public static LiveLogStream createLiveLogStream(UriInfo uriInfo) {
    String level = uriInfo.getQueryParameters().getFirst("__level");
    ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
    LiveLogStream.Builder builder =
        LiveLogStream.builderFor(LOG4J).withOutputStream(out).withLoggers("nl.naturalis.*");
    if (level == null) {
      return builder.build();
    }
    return builder.withLogLevel(level).build();
  }
}
