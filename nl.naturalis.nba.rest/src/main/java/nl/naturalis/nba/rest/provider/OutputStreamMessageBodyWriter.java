package nl.naturalis.nba.rest.provider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

@Provider
public class OutputStreamMessageBodyWriter implements MessageBodyWriter<ByteArrayOutputStream> {

  public OutputStreamMessageBodyWriter() {}

  @Override
  public boolean isWriteable(
      Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
    return type == ByteArrayOutputStream.class;
  }

  @Override
  public void writeTo(
      ByteArrayOutputStream baos,
      Class<?> type,
      Type genericType,
      Annotation[] annotations,
      MediaType mediaType,
      MultivaluedMap<String, Object> httpHeaders,
      OutputStream out)
      throws IOException {
    baos.writeTo(out);
  }
}
