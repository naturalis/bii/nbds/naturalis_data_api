package nl.naturalis.nba.rest.resource;

import static java.nio.charset.StandardCharsets.UTF_8;
import static nl.naturalis.common.ObjectMethods.ifNull;
import static nl.naturalis.nba.common.json.JsonUtil.deserialize;
import static nl.naturalis.nba.rest.util.ResourceUtil.JSON_CONTENT_TYPE;
import static nl.naturalis.nba.rest.util.ResourceUtil.NDJSON_CONTENT_TYPE;
import static nl.naturalis.nba.rest.util.ResourceUtil.TEXT_CONTENT_TYPE;
import static nl.naturalis.nba.rest.util.ResourceUtil.ZIP_CONTENT_TYPE;
import static nl.naturalis.nba.rest.util.ResourceUtil.createLiveLogStream;
import static nl.naturalis.nba.rest.util.ResourceUtil.handleError;
import static nl.naturalis.nba.rest.util.ResourceUtil.isExplainRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;
import nl.naturalis.log.appenders.LiveLogStream;
import nl.naturalis.nba.api.*;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.SpecimenDao;
import nl.naturalis.nba.rest.exception.HTTP400Exception;
import nl.naturalis.nba.rest.exception.RESTException;
import nl.naturalis.nba.rest.util.HttpGroupByScientificNameQuerySpecBuilder;
import nl.naturalis.nba.rest.util.HttpQuerySpecBuilder;
import nl.naturalis.nba.utils.ConfigObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;

@Stateless
@LocalBean
@Api(value = "specimen")
@Path("/specimen")
@Produces({"application/json", "application/xml"})
public class SpecimenResource extends NbaResource<Specimen, SpecimenDao> {

  private static final Logger logger = LogManager.getLogger(SpecimenResource.class);

  SpecimenResource() {
    super(new SpecimenDao());
  }

  @Override
  @GET
  @GZIP
  @Path("/download")
  @ApiOperation(
      value = "Dynamic download service: Query for specimens and return result as a stream ...",
      response = Response.class,
      notes = "Query with query parameters or querySpec JSON. ...")
  @Produces(NDJSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "collectionType",
        value = "Example query param",
        dataType = "string",
        paramType = "query",
        defaultValue = "Crustacea",
        required = false)
  })
  public Response downloadQueryHttpGet(@Context UriInfo uriInfo) {
    return super.downloadQueryHttpGet(uriInfo);
  }

  @Override
  @POST
  @GZIP
  @Path("/download")
  @ApiOperation(
      value = "Dynamic download service: Query for specimens and return result as a stream ...",
      response = Response.class,
      notes = "Query with query parameters or querySpec JSON. ...")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(NDJSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "collectionType",
        value = "Example query param",
        dataType = "string",
        paramType = "query",
        defaultValue = "Crustacea",
        required = false)
  })
  public Response downloadQueryHttpPostForm(
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.downloadQueryHttpPostForm(form, uriInfo);
  }

  @Override
  @POST
  @GZIP
  @Path("/download")
  @ApiOperation(
      value = "Dynamic download service: Query for specimens and return result as a stream ...",
      response = Response.class,
      notes = "Query with query parameters or querySpec JSON. ...")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(NDJSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "collectionType",
        value = "Example query param",
        dataType = "string",
        paramType = "query",
        defaultValue = "Crustacea",
        required = false)
  })
  public Response downloadQueryHttpPostJson(
      @ApiParam(value = "querySpec", required = false) QuerySpec qs, @Context UriInfo uriInfo) {
    return super.downloadQueryHttpPostJson(qs, uriInfo);
  }

  @Override
  @GET
  @Path("/find/{id}")
  @ApiOperation(
      value = "Find a specimen by its Elasticsearch document ID",
      response = Specimen.class,
      notes =
          "If found, returns a single specimen. Note that NBA document IDs are not auto-generated, so they are stable across dataset imports.")
  @ApiResponses(value = {@ApiResponse(code = 404, message = "id not found")})
  @Produces(JSON_CONTENT_TYPE)
  public Specimen find(
      @ApiParam(value = "id of specimen", required = true, defaultValue = "RMNH.MAM.17209.B@CRS")
          @PathParam("id")
          String id,
      @Context UriInfo uriInfo) {
    return super.find(id, uriInfo);
  }

  @Override
  @GET
  @Path("/findByIds/{ids}")
  @ApiOperation(
      value = "Find specimens by their Elasticsearch document IDs",
      response = Specimen[].class,
      notes = "Given multiple ids, returns a list of specimen")
  @Produces(JSON_CONTENT_TYPE)
  public Specimen[] findByIds(
      @ApiParam(
              value = "ids of multiple specimen, separated by comma",
              required = true,
              defaultValue = "RMNH.MOL.326483@CRS,ZMA.MAM.4211@CRS",
              allowMultiple = true)
          @PathParam("ids")
          String ids,
      @Context UriInfo uriInfo) {
    return super.findByIds(ids, uriInfo);
  }

  @GET
  @Path("/findByUnitID/{unitID}")
  @ApiOperation(
      value = "Find a specimen by unitID",
      response = Specimen[].class,
      notes =
          "Get a specimen by its unitID. Returns a list of specimens since unitIDs are not strictly unique")
  @Produces(JSON_CONTENT_TYPE)
  public Specimen[] findByUnitID(
      @ApiParam(
              value = "the unitID of the specimen to query",
              required = true,
              defaultValue = "RMNH.MAM.17209.B")
          @PathParam("unitID")
          String unitID,
      @Context UriInfo uriInfo) {
    try {
      SpecimenDao dao = new SpecimenDao();
      return dao.findByUnitID(unitID);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @GET
  @Path("/exists/{unitID}")
  @ApiOperation(
      value = "Returns whether or not the provided unitID is a valid (in-use) unitID",
      response = boolean.class,
      notes = "Returns either true or false")
  @Produces(TEXT_CONTENT_TYPE)
  public boolean exists(
      @ApiParam(
              value = "the unitID of the specimen to query",
              required = true,
              defaultValue = "RMNH.MAM.17209.B")
          @PathParam("unitID")
          String unitID,
      @Context UriInfo uriInfo) {
    try {
      SpecimenDao dao = new SpecimenDao();
      return dao.exists(unitID);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @Override
  @GET
  @Path("/query")
  @ApiOperation(
      value = "Query for specimens",
      response = QueryResult.class,
      notes = "Search for specimens (GET) using query parameters or a querySpec JSON")
  @Produces(JSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "collectionType",
        value = "Example query param",
        dataType = "string",
        paramType = "query",
        defaultValue = "Crustacea",
        required = false)
  })
  public QueryResult<Specimen> queryHttpGet(@Context UriInfo uriInfo) {
    return super.queryHttpGet(uriInfo);
  }

  @Override
  @POST
  @Path("/query")
  @ApiOperation(
      hidden = true,
      value = "Query for specimens",
      response = QueryResult.class,
      notes = "Search for specimens (POST) using query parameters")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public QueryResult<Specimen> queryHttpPostForm(
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.queryHttpPostForm(form, uriInfo);
  }

  @Override
  @POST
  @Path("/query")
  @ApiOperation(
      value = "Query for specimens",
      response = QueryResult.class,
      notes = "Search for specimens (POST) using a JSON-serialized QuerySpec in de request body")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public QueryResult<Specimen> queryHttpPostJson(
      @ApiParam(value = "querySpec", required = false) QuerySpec qs, @Context UriInfo uriInfo) {
    return super.queryHttpPostJson(qs, uriInfo);
  }

  @GET
  @Path("/queryWithNameResolution")
  @Produces({JSON_CONTENT_TYPE, MediaType.TEXT_PLAIN})
  public Response queryWithNameResolutionHttpGet(
      @QueryParam("_querySpec") String json, @Context UriInfo uriInfo) {
    try {
      NameResolutionQuerySpec qs = deserialize(ifNull(json, "{}"), NameResolutionQuerySpec.class);
      if (isExplainRequest(uriInfo)) {
        try (LiveLogStream stream = createLiveLogStream(uriInfo)) {
          return explainNameResQuery(qs, stream);
        }
      }
      return executeNameResQuery(qs);
    } catch (InvalidQueryException e) {
      throw new HTTP400Exception(uriInfo, e);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Path("/queryWithNameResolution")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces({JSON_CONTENT_TYPE, MediaType.TEXT_PLAIN})
  public Response queryWithNameResolutionHttpPostForm(
      @FormParam("_querySpec") String json, @Context UriInfo uriInfo) {
    try {
      NameResolutionQuerySpec qs = deserialize(ifNull(json, "{}"), NameResolutionQuerySpec.class);
      if (isExplainRequest(uriInfo)) {
        try (LiveLogStream stream = createLiveLogStream(uriInfo)) {
          return explainNameResQuery(qs, stream);
        }
      }
      return executeNameResQuery(qs);
    } catch (InvalidQueryException e) {
      throw new HTTP400Exception(uriInfo, e);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Path("/queryWithNameResolution")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces({JSON_CONTENT_TYPE, MediaType.TEXT_PLAIN})
  public Response queryWithNameResolutionHttpPostJson(
      @ApiParam(value = "query", required = true) NameResolutionQuerySpec query,
      @Context UriInfo uriInfo) {
    try {
      if (isExplainRequest(uriInfo)) {
        try (LiveLogStream stream = createLiveLogStream(uriInfo)) {
          return explainNameResQuery(query, stream);
        }
      }
      return executeNameResQuery(query);
    } catch (InvalidQueryException e) {
      throw new HTTP400Exception(uriInfo, e);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @Override
  @GET
  @Path("/count")
  @ApiOperation(
      value = "Get the number of specimens matching a given condition",
      response = long.class,
      notes = "Conditions given as query parameters or a querySpec JSON")
  @Produces(TEXT_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "collectionType",
        value = "Example query param",
        dataType = "string",
        paramType = "query",
        defaultValue = "Crustacea",
        required = false)
  })
  public long countHttpGet(@Context UriInfo uriInfo) {
    return super.countHttpGet(uriInfo);
  }

  @Override
  @POST
  @Path("/count")
  @ApiOperation(
      hidden = true,
      value = "Get the number of specimens matching a given condition",
      response = long.class,
      notes = "Conditions given as query parameters or a querySpec JSON")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(TEXT_CONTENT_TYPE)
  public long countHttpPostForm(
      @ApiParam(value = "query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.countHttpPostForm(form, uriInfo);
  }

  @Override
  @POST
  @Path("/count")
  @ApiOperation(
      value = "Get the number of specimens matching a given condition",
      response = long.class,
      notes = "Conditions given as query parameters or a querySpec JSON")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(TEXT_CONTENT_TYPE)
  public long countHttpPostJson(
      @ApiParam(value = "QuerySpec in JSON form", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    return super.countHttpPostJson(qs, uriInfo);
  }

  @Override
  @GET
  @Path("/countDistinctValues/{field}")
  @ApiOperation(
      value = "Count the distinct number of values that exist for a given field",
      response = long.class,
      notes = "")
  @Produces(TEXT_CONTENT_TYPE)
  public long countDistinctValuesHttpGet(
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValues/" + field);
    return super.countDistinctValuesHttpGet(field, uriInfo);
  }

  @POST
  @Path("/countDistinctValues/{field}")
  @ApiOperation(
      value = "Count the distinct number of values that exist for a given field",
      response = long.class,
      notes = "")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(TEXT_CONTENT_TYPE)
  public long countDistinctValuesHttpPost(
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @ApiParam(value = "Query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValues/" + field);
    return super.countDistinctValuesHttpPostForm(field, form, uriInfo);
  }

  @POST
  @Path("/countDistinctValues/{field}")
  @ApiOperation(
      value = "Count the distinct number of values that exist for a given field",
      response = long.class,
      notes = "")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(TEXT_CONTENT_TYPE)
  public long countDistinctValuesHttpJson(
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @ApiParam(value = "QuerySpec in JSON form", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValues/" + field);
    return super.countDistinctValuesHttpPostJson(field, qs, uriInfo);
  }

  @Override
  @GET
  @Path("/countDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Count the distinct number of field values that exist per the given field to group by",
      response = List.class,
      notes = "")
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> countDistinctValuesPerGroupHttpGet(
      @ApiParam(
              value = "Name of field in the specimen object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValuesPerGroup/" + group + "/" + field);
    return super.countDistinctValuesPerGroupHttpGet(group, field, uriInfo);
  }

  @Override
  @POST
  @Path("/countDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Count the distinct number of field values that exist per the given field to group by",
      response = List.class,
      notes = "")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> countDistinctValuesPerGroupHttpPostForm(
      @ApiParam(
              value = "Name of field in the specimen object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @ApiParam(value = "Query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValuesPerGroup/" + group + "/" + field);
    return super.countDistinctValuesPerGroupHttpPostForm(group, field, form, uriInfo);
  }

  @Override
  @POST
  @Path("/countDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Count the distinct number of field values that exist per the given field to group by",
      response = List.class,
      notes = "")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> countDistinctValuesPerGroupHttpPostJson(
      @ApiParam(
              value = "Name of field in the specimen object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @ApiParam(value = "QuerySpec in JSON form", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValuesPerGroup/" + group + "/" + field);
    return super.countDistinctValuesPerGroupHttpPostJson(group, field, qs, uriInfo);
  }

  @Override
  @GET
  @Path("/getDistinctValues/{field}")
  @ApiOperation(
      value = "Get all different values that exist for a field",
      response = Map.class,
      notes =
          "A list of all fields for specimen documents can be retrieved with /metadata/getFieldInfo")
  @Produces(JSON_CONTENT_TYPE)
  public Map<String, Long> getDistinctValuesHttpGet(
      @ApiParam(
              value = "Name of field in specimen object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    return super.getDistinctValuesHttpGet(field, uriInfo);
  }

  @Override
  @POST
  @Path("/getDistinctValues/{field}")
  @ApiOperation(
      value = "Get all different values that exist for a field",
      response = Map.class,
      notes =
          "A list of all fields for specimen documents can be retrieved with /metadata/getFieldInfo")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public Map<String, Long> getDistinctValuesHttpPostForm(
      @ApiParam(
              value = "Name of field in specimen object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @ApiParam(value = "Query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.getDistinctValuesHttpPostForm(field, form, uriInfo);
  }

  @Override
  @POST
  @Path("/getDistinctValues/{field}")
  @ApiOperation(
      value = "Get all different values that exist for a field",
      response = Map.class,
      notes =
          "A list of all fields for specimen documents can be retrieved with /metadata/getFieldInfo")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public Map<String, Long> getDistinctValuesHttpPostJson(
      @ApiParam(
              value = "Name of field in specimen object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @ApiParam(value = "QuerySpec in JSON form", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    return super.getDistinctValuesHttpPostJson(field, qs, uriInfo);
  }

  @Override
  @GET
  @Path("/getDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Get all distinct values (and their document count) for the field given divided per distinct value of the field to group by",
      response = List.class,
      notes = "")
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> getDistinctValuesPerGroupHttpGet(
      @ApiParam(
              value = "Name of field in the specimen object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    logger.info("getDistinctValuesPerGroup/" + group + "/" + field);
    return super.getDistinctValuesPerGroupHttpGet(group, field, uriInfo);
  }

  @POST
  @Path("/getDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Get all distinct values (and their document count) for the field given divided per distinct value of the field to group by",
      response = List.class,
      notes = "")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> getDistinctValuesPerGroupHttpPostForm(
      @ApiParam(
              value = "Name of field in the specimen object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @ApiParam(value = "Query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    logger.info("getDistinctValuesPerGroup/" + group + "/" + field);
    return super.getDistinctValuesPerGroupHttpPost(group, field, form, uriInfo);
  }

  @POST
  @Path("/getDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Get all distinct values (and their document count) for the field given divided per distinct value of the field to group by",
      response = List.class,
      notes = "")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> getDistinctValuesPerGroup(
      @ApiParam(
              value = "Name of field in the specimen object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "Name of field in the specimen object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @ApiParam(value = "QuerySpec JSON", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    logger.info("getDistinctValuesPerGroup/" + group + "/" + field);
    return super.getDistinctValuesPerGroupHttpJson(group, field, qs, uriInfo);
  }

  @GET
  @Path("/dwca/query")
  @ApiOperation(
      value =
          "Dynamic download service: Query for specimens and return result as Darwin Core Archive File",
      response = Response.class,
      notes =
          "Query with query parameters or querySpec JSON. Response saved to nba-specimens.dwca.zip")
  @Produces(ZIP_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "collectionType",
        value = "Example query param",
        dataType = "string",
        paramType = "query",
        defaultValue = "Crustacea",
        required = false)
  })
  public Response dwcaQueryHttpGet(
      @ApiParam(value = "query string", required = true) @Context UriInfo uriInfo) {
    try {
      QuerySpec qs = new HttpQuerySpecBuilder(uriInfo).build();
      StreamingOutput stream =
          new StreamingOutput() {

            @Override
            public void write(OutputStream out) {
              SpecimenDao dao = new SpecimenDao();
              try {
                dao.dwcaQuery(qs, out);
              } catch (Throwable e) {
                throw new RESTException(uriInfo, e);
              }
            }
          };
      ResponseBuilder response = Response.ok(stream);
      response.type(ZIP_CONTENT_TYPE);
      response.header("Content-Disposition", "attachment; filename=\"nba-specimens.dwca.zip\"");
      return response.build();
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Path("/dwca/query")
  @ApiOperation(
      value =
          "Dynamic download service: Query for specimens and return result as Darwin Core Archive File",
      response = Response.class,
      notes =
          "Query with query parameters or querySpec JSON. Response saved to nba-specimens.dwca.zip")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(ZIP_CONTENT_TYPE)
  public Response dwcaQueryHttpPostForm(
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    try {
      QuerySpec qs = new HttpQuerySpecBuilder(form, uriInfo).build();
      StreamingOutput stream =
          new StreamingOutput() {

            @Override
            public void write(OutputStream out) {
              SpecimenDao dao = new SpecimenDao();
              try {
                dao.dwcaQuery(qs, out);
              } catch (InvalidQueryException e) {
                throw new HTTP400Exception(uriInfo, e);
              }
            }
          };

      ResponseBuilder response = Response.ok(stream);
      response.type(ZIP_CONTENT_TYPE);
      response.header("Content-Disposition", "attachment; filename=\"nba-specimens.dwca.zip\"");
      return response.build();
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Path("/dwca/query")
  @ApiOperation(
      value =
          "Dynamic download service: Query for specimens and return result as Darwin Core Archive File",
      response = Response.class,
      notes =
          "Query with query parameters or querySpec JSON. Response saved to nba-specimens.dwca.zip")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(ZIP_CONTENT_TYPE)
  public Response dwcaQueryHttpPostJson(
      @ApiParam(value = "querySpec", required = false) QuerySpec qs, @Context UriInfo uriInfo) {
    try {
      StreamingOutput stream =
          new StreamingOutput() {

            @Override
            public void write(OutputStream out) {
              SpecimenDao dao = new SpecimenDao();
              try {
                dao.dwcaQuery(qs, out);
              } catch (InvalidQueryException e) {
                throw new HTTP400Exception(uriInfo, e);
              }
            }
          };

      ResponseBuilder response = Response.ok(stream);
      response.type(ZIP_CONTENT_TYPE);
      response.header("Content-Disposition", "attachment; filename=\"nba-specimens.dwca.zip\"");
      return response.build();
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @GET
  @Path("/dwca/getDataSet/{dataset}")
  @ApiOperation(
      value = "Download dataset as Darwin Core Archive File",
      response = Response.class,
      notes =
          "Available datasets can be queried with /specimen/dwca/getDataSetNames. Response saved to <datasetname>-<yyyymmdd>.dwca.zip")
  @Produces(ZIP_CONTENT_TYPE)
  public Response dwcaGetDataSet(
      @ApiParam(value = "name of dataset", required = true, defaultValue = "amphibia-and-reptilia")
          @PathParam("dataset")
          String name,
      @Context UriInfo uriInfo) {

    ConfigObject config = DaoRegistry.getInstance().getConfiguration();
    boolean cachedCopy = false;
    String path = config.get("nl.naturalis.nba.dwca.cache.path");
    if (path != null) {
      path = path.concat("/specimen/");
      path = path.concat(name);
      path = path.concat(".dwca.zip");
      cachedCopy = new File(path).exists();
    }
    if (cachedCopy) {
      logger.info("Using cached copy of DwC Archive file for data set {}", name);
      try {
        final String copy = path;
        StreamingOutput stream =
            new StreamingOutput() {

              @Override
              public void write(OutputStream out) throws IOException {
                try (InputStream in = new FileInputStream(copy)) {
                  int length;
                  byte[] bytes = new byte[1024];
                  while ((length = in.read(bytes)) != -1) {
                    out.write(bytes, 0, length);
                  }
                }
              }
            };

        ResponseBuilder response = Response.ok(stream);
        response.type(ZIP_CONTENT_TYPE);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String fmt = "attachment; filename=\"%s-%s.dwca.zip\"";
        String hdr = String.format(fmt, name, sdf.format(new Date()));
        response.header("Content-Disposition", hdr);
        return response.build();
      } catch (Throwable t) {
        throw handleError(uriInfo, t);
      }
    }
    logger.info("Generating a new DwC Archive for data set {}", name);
    try {
      StreamingOutput stream =
          new StreamingOutput() {

            @Override
            public void write(OutputStream out) {
              SpecimenDao dao = new SpecimenDao();
              try {
                dao.dwcaGetDataSet(name, out);
              } catch (NoSuchDataSetException e) {
                throw new HTTP400Exception(uriInfo, e);
              }
            }
          };

      ResponseBuilder response = Response.ok(stream);
      response.type(ZIP_CONTENT_TYPE);
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      String fmt = "attachment; filename=\"%s-%s.dwca.zip\"";
      String hdr = String.format(fmt, name, sdf.format(new Date()));
      response.header("Content-Disposition", hdr);
      return response.build();
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @GET
  @Path("/dwca/getDataSetNames")
  @ApiOperation(
      value = "Retrieve the names of all available datasets",
      response = String[].class,
      notes = "Individual datasets can then be downloaded with /dwca/getDataSet/{dataset}")
  @Produces(JSON_CONTENT_TYPE)
  public String[] dwcaGetDataSetNames(@Context UriInfo uriInfo) {
    try {
      SpecimenDao dao = new SpecimenDao();
      return dao.dwcaGetDataSetNames();
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @GET
  @Path("/groupByScientificName")
  @ApiOperation(
      value = "Aggregates Taxon and Specimen documents according to their scientific names",
      response = QueryResult.class,
      notes =
          "Returns a list with ScientificNameGroups, which contain Taxon and Specimen documents that share a scientific name")
  @Produces(JSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "collectionType",
        value = "Example query param",
        dataType = "string",
        paramType = "query",
        defaultValue = "Crustacea",
        required = false)
  })
  public GroupByScientificNameQueryResult groupByScientificNameHttpGet(@Context UriInfo uriInfo) {
    try {
      GroupByScientificNameQuerySpec qs =
          new HttpGroupByScientificNameQuerySpecBuilder(uriInfo).build();
      SpecimenDao dao = new SpecimenDao();
      return dao.groupByScientificName(qs);
    } catch (InvalidQueryException e) {
      throw new HTTP400Exception(uriInfo, e);
    }
  }

  @POST
  @Path("/groupByScientificName")
  @ApiOperation(
      value = "Aggregates Taxon and Specimen documents according to their scientific names",
      response = QueryResult.class,
      notes =
          "Returns a list with ScientificNameGroups, which contain Taxon and Specimen documents that share a scientific name")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public GroupByScientificNameQueryResult groupByScientificNameHttpPostForm(
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    try {
      GroupByScientificNameQuerySpec qs =
          new HttpGroupByScientificNameQuerySpecBuilder(form, uriInfo).build();
      SpecimenDao dao = new SpecimenDao();
      return dao.groupByScientificName(qs);
    } catch (InvalidQueryException e) {
      throw new HTTP400Exception(uriInfo, e);
    }
  }

  @POST
  @Path("/groupByScientificName")
  @ApiOperation(
      value = "Aggregates Taxon and Specimen documents according to their scientific names",
      response = QueryResult.class,
      notes =
          "Returns a list with ScientificNameGroups, which contain Taxon and Specimen documents that share a scientific name")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public GroupByScientificNameQueryResult groupByScientificNameHttpPostJson(
      @ApiParam(value = "querySpec JSON", required = false) GroupByScientificNameQuerySpec qs,
      @Context UriInfo uriInfo) {
    try {
      SpecimenDao dao = new SpecimenDao();
      return dao.groupByScientificName(qs);
    } catch (InvalidQueryException e) {
      throw new HTTP400Exception(uriInfo, e);
    }
  }

  private Response executeNameResQuery(NameResolutionQuerySpec qs) throws InvalidQueryException {
    return Response.status(Status.OK)
        .type(JSON_CONTENT_TYPE)
        .entity(dao.queryWithNameResolution(qs))
        .build();
  }

  private Response explainNameResQuery(NameResolutionQuerySpec qs, LiveLogStream stream)
      throws InvalidQueryException, IOException {
    QueryResult<Specimen> res = dao.queryWithNameResolution(qs);
    String divider = "\n\n----------------------- END OF LOG -----------------------\n\n";
    stream.getOutputStream().write(divider.getBytes(UTF_8));
    JsonUtil.toPrettyJson(stream.getOutputStream(), res);
    return Response.status(Status.OK)
        .type(MediaType.TEXT_PLAIN_TYPE)
        .entity(stream.getOutputStream())
        .build();
  }
}
