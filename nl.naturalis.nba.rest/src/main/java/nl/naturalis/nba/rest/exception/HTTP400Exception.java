package nl.naturalis.nba.rest.exception;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

/**
 * Thrown, for example, for syntactically or semantically incorrect query specifications.
 *
 * @author Ayco Holleman
 */
@SuppressWarnings("CheckStyle")
public class HTTP400Exception extends RESTException {

  private static final long serialVersionUID = 1L;

  public HTTP400Exception(UriInfo uriInfo, String message) {
    super(uriInfo, Status.BAD_REQUEST, "400 (BAD REQUEST)\n" + message);
  }

  public HTTP400Exception(UriInfo uriInfo, Throwable cause) {
    super(uriInfo, Status.BAD_REQUEST, cause);
  }
}
