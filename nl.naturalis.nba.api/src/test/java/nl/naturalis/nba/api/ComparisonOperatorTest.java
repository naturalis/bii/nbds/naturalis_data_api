package nl.naturalis.nba.api;

import static nl.naturalis.nba.api.ComparisonOperator.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class ComparisonOperatorTest {

  @Test(expected = IllegalArgumentException.class)
  public void testParse_01() {
    parse(null);
  }

  @Test
  public void testParse_02() {
    assertEquals("01", EQUALS, parse("EQUALS"));
    assertEquals("02", NOT_STARTS_WITH_IC, parse("NOT_STARTS_WITH_IC"));
  }

  @Test
  public void testParse_03() {
    assertEquals("01", EQUALS, parse("equals"));
    assertEquals("02", NOT_STARTS_WITH_IC, parse("not_starts_with_ic"));
  }

  @Test
  public void testParse_04() {
    assertEquals("01", EQUALS, parse("="));
    assertEquals("02", NOT_EQUALS, parse("!="));
    assertEquals("03", LT, parse("<"));
    assertEquals("04", LTE, parse("<="));
    assertEquals("05", GT, parse(">"));
    assertEquals("06", GTE, parse(">="));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testParse_05() {
    parse("NOT_EXISTING_OPERATOR");
  }

  @Test
  public void test_equals_operator() {
    QueryCondition c0 = new QueryCondition("foo", "=", "bar");
    QueryCondition c1 = new QueryCondition("foo", "EQUALS", "bar");
    QueryCondition c2 = new QueryCondition("foo", "EQUALS_IC", "bar");
    assertEquals(c0, c1);
    assertNotEquals(c0, c2);
  }

  @Test
  public void test_not_equals_operator() {
    QueryCondition c0 = new QueryCondition("foo", "!=", "bar");
    QueryCondition c1 = new QueryCondition("foo", "NOT_EQUALS", "bar");
    assertEquals(c0, c1);
  }

  @Test
  public void test_less_than_operator() {
    QueryCondition c0 = new QueryCondition("foo", "<", "bar");
    QueryCondition c1 = new QueryCondition("foo", "LT", "bar");
    assertEquals(c0, c1);
  }

  @Test
  public void test_less_than_equals_operator() {
    QueryCondition c0 = new QueryCondition("foo", "<=", "bar");
    QueryCondition c1 = new QueryCondition("foo", "LTE", "bar");
    assertEquals(c0, c1);
  }

  @Test
  public void test_greater_than_operator() {
    QueryCondition c0 = new QueryCondition("foo", ">", "bar");
    QueryCondition c1 = new QueryCondition("foo", "GT", "bar");
    assertEquals(c0, c1);
  }

  @Test
  public void test_greater_than_equals_operator() {
    QueryCondition c0 = new QueryCondition("foo", ">=", "bar");
    QueryCondition c1 = new QueryCondition("foo", "GTE", "bar");
    assertEquals(c0, c1);
  }


}
