package nl.naturalis.nba.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum representing an area classification. It Allows the recording of classification
 * categories for the class of the gathering named area (local or national subdivision
 * levels, geomorphological units, protected areas, etc.)
 *
 * <p>More information: {
 * <a href="https://terms.tdwg.org/wiki/abcd2:Gathering-NamedArea-AreaClass">
 *   https://terms.tdwg.org/wiki/abcd2:Gathering-NamedArea-AreaClass
 *   </a>
 *  }
 *
 */
public enum AreaClass implements INbaModelObject {
  CONTINENT("continent"),
  COUNTY("county"),
  HIGHERGEOGRAPHY("higherGeography"),
  ISLANDGROUP("islandGroup"),
  MUNICIPALITY("municipality"),
  WATERBODY("waterBody");

  private final String name;

  AreaClass(String name) {
    this.name = name;
  }

  /**
   * Tries to parse the string given as an AreaClass.
   *
   * @param name  the area class
   * @return the matching AreaClass
   */
  @JsonFormat(shape = Shape.OBJECT)
  public static AreaClass parse(@JsonProperty("name") String name) {
    if (name == null) {
      return null;
    }
    for (AreaClass areaClass : AreaClass.values()) {
      if (areaClass.name.equalsIgnoreCase(name)) {
        return areaClass;
      }
    }
    throw new IllegalArgumentException("Invalid areaClass: \"" + name + "\"");
  }

  @JsonValue
  @Override
  public String toString() {
    return name;
  }
}
