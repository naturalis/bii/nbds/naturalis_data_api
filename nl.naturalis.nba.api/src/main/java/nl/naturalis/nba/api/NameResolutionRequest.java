package nl.naturalis.nba.api;

import java.util.Set;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import nl.naturalis.common.util.EnumParser;
import nl.naturalis.common.check.Check;
import static nl.naturalis.common.check.CommonChecks.*;
import static nl.naturalis.common.check.CommonChecks.notBlank;

/**
 * Defines a request to resolve a search phrase to a list of names.
 *
 * @author Ayco Holleman
 */
public class NameResolutionRequest {

  /**
   * Specifies the different types of names that can be used for the name lookup. By default the
   * search phrase is searched for in all name types, but the user can specifiy that he/she wants
   * the name to be resolved as (for example) a vernacular name only.
   *
   * @author Ayco Holleman
   */
  public enum NameType {
    ACCEPTED_NAME,
    SYNONYM,
    VERNACULAR_NAME;

    public static final EnumParser<NameType> parser = new EnumParser<>(NameType.class);

    @JsonCreator
    public static NameType parse(String target) {
      return parser.parse(target);
    }

    @Override
    public String toString() {
      return name().toLowerCase().replace('_', ' ');
    }
  }

  /**
   * Specifies the type of search to be executed against CoL c.q. taxon index. NB CoL does not have
   * the equivalent of "CONTAINS", so for CoL this option has the same mapping as "STARTS_WITH".
   * Also note that when choosing EXACT search, the fuzzy matching option is ignored.
   *
   * @author Ayco Holleman
   */
  public static enum SearchType {
    EXACT,
    WHOLE_WORDS,
    STARTS_WITH,
    CONTAINS;

    public static final EnumParser<SearchType> parser = new EnumParser<>(SearchType.class);

    @JsonCreator
    public static SearchType parse(String target) {
      return parser.parse(target);
    }

    @Override
    public String toString() {
      return name().toLowerCase().replace('_', ' ');
    }
  }

  private Boolean useCoL;
  private String searchString;
  private SearchType searchType;
  private Set<NameType> nameTypes;
  private Boolean fuzzyMatching;
  private Integer from;
  private Integer size;

  @JsonIgnore // just to be sure
  public boolean useCoL() {
    return useCoL != null && useCoL;
  }

  @JsonIgnore // just to be sure
  public boolean fuzzyMatching() {
    return fuzzyMatching != null && fuzzyMatching;
  }

  /**
   * Whether or not to use the Catalogue of Life for name resolution rather than the NBA's own taxon
   * index.
   *
   * @return
   */
  public Boolean getUseCoL() {
    return useCoL;
  }

  public void setUseCoL(Boolean useColPlus) {
    this.useCoL = useColPlus;
  }

  /**
   * The (partial) name to search for in the taxon index.
   *
   * @return
   */
  public String getSearchString() {
    return searchString;
  }

  public void setSearchString(String searchString) {
    this.searchString = Check.that(searchString, "search string").is(notBlank()).ok();
  }

  public SearchType getSearchType() {
    return searchType;
  }

  public void setSearchType(SearchType searchType) {
    this.searchType = searchType;
  }

  /**
   * The fields to search within the taxon index (accepted names and/or synonyms and/or vernacular
   * names).
   *
   * @return
   */
  public Set<NameType> getNameTypes() {
    return nameTypes;
  }

  public void setNameTypes(Set<NameType> nameTypes) {
    this.nameTypes = nameTypes;
  }

  /**
   * Whether or not to apply fuzzy matching. With fuzzy matching the search phrase is normalized
   * using the GBIF name normalizer and then matched against normalized versions of the scientific
   * name. This will increase the chance of finding a match because common spelling variants or
   * errors (e.g. "silvestris" / "sylvestris") are normalized away. NB this only applies to
   * scientific name matching, not to vernacular name matching.
   *
   * @return
   */
  public Boolean getFuzzyMatching() {
    return fuzzyMatching;
  }

  public void setFuzzyMatching(Boolean fuzzyMatching) {
    this.fuzzyMatching = fuzzyMatching;
  }

  /**
   * The offset of the first taxon within the list of taxa returned by the name resolution query.
   * This parameter is simply passed on to the taxon query.
   *
   * @return
   */
  public Integer getFrom() {
    return from;
  }

  public void setFrom(Integer from) {
    this.from = Check.notNull(from, "from").is(gte(), 0).ok();
  }

  /**
   * The maximum number of taxa to return. NB one taxon usually contains multiple names: the
   * accepted name and possibly some synonyms. <i>Each</i> scientific name gets turned into a query
   * condition for the Specimen index. Therefore, if <code>size</code> is large, it may exceed the
   * maximum number of clauses in a boolean query (indices.query.bool.max_clause_count) and may
   * therefore cause an {@link InvalidQueryException}!
   *
   * @return
   */
  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    /*
     * indices.query.bool.max_clause_count = 1024 by default. Specifying
     * 1024 will probably still cause the service to fail because most taxa will
     * have synonyms, and _all_ names (both accepted names and synonyms) will be
     * converted into one big OR OR OR query condition for the specimen index.
     */
    this.size = Check.notNull(size, "size").is(gte(), 0).is(lt(), 1024).ok();
  }
}
