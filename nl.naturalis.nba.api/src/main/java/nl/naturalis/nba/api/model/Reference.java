package nl.naturalis.nba.api.model;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * A {@code Reference} represents a literature reference or common name.
 *
 * @see http://wiki.tdwg.org/twiki/bin/view/ABCD/AbcdConcept0282
 *
 */
public class Reference implements INbaModelObject {

  private String titleCitation;
  private String citationDetail;
  private String uri;
  private Person author;
  private OffsetDateTime publicationDate;

  public String getTitleCitation() {
    return titleCitation;
  }

  public void setTitleCitation(String titleCitation) {
    this.titleCitation = titleCitation;
  }

  public String getCitationDetail() {
    return citationDetail;
  }

  public void setCitationDetail(String citationDetail) {
    this.citationDetail = citationDetail;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public Person getAuthor() {
    return author;
  }

  public void setAuthor(Person author) {
    this.author = author;
  }

  public OffsetDateTime getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(OffsetDateTime publicationDate) {
    this.publicationDate = publicationDate;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder(64);
    if (titleCitation == null) {
      sb.append("### Title Not Available ###");
    } else {
      sb.append(titleCitation);
    }
    sb.append(';');
    if (citationDetail != null) {
      sb.append(' ').append(citationDetail);
    }
    sb.append(';');
    if (author != null) {
      if (author.getFullName() != null) sb.append(' ').append(author.getFullName());
      else if (author.getAgentText() != null) sb.append(' ').append(author.getAgentText());
    }
    sb.append(';');
    if (publicationDate != null) {
      sb.append(' ').append(publicationDate);
    }
    return sb.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(author, citationDetail, publicationDate, titleCitation, uri);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Reference other = (Reference) obj;
    return Objects.equals(author, other.author)
        && Objects.equals(citationDetail, other.citationDetail)
        && Objects.equals(publicationDate, other.publicationDate)
        && Objects.equals(titleCitation, other.titleCitation)
        && Objects.equals(uri, other.uri);
  }
}
