package nl.naturalis.nba.api;

import nl.naturalis.nba.api.model.MultiMediaObject;

/**
 * Specifies methods for accessing multimedia-related data.
 *
 * @author Ayco Holleman
 *
 */
public interface IMultiMediaObjectAccess extends INbaAccess<MultiMediaObject> {

  /**
   * Searches for multimedia while providing extra options to resolve a search
   * phrase to a scientific name. This is meant to be implemented as a two-phase
   * query. In the first phase the {@link NameResolutionRequest} within the
   * provided {@link NameResolutionQuerySpec} is used to generate a query against
   * a taxon database. This can be either the NBA's own taxon index or it can be
   * ColPlus. The scientific names coming back from that query are then used to
   * construct an extra query condition for the multimedia index. In the second
   * phase that query condition is added to the other query conditions of the
   * provided <code>querySpec</code>. The enriched query is executed and the
   * result is returned to the client.
   *
   * @param querySpec
   * @return
   * @throws InvalidQueryException
   */
  QueryResult<MultiMediaObject> queryWithNameResolution(NameResolutionQuerySpec querySpec)
      throws InvalidQueryException;
}
