package nl.naturalis.nba.api;

import java.util.Arrays;
import java.util.Objects;

/**
 * A generic filter definition. Used by {@link GroupByScientificNameQuerySpec}
 * to filter the groups (a.k.a. buckets) returned from the GROUP BY aggregation.
 *
 * @author Ayco Holleman
 *
 */
@SuppressWarnings("unused")
public class Filter {

  private String acceptRegexp;
  private String rejectRegexp;
  private String[] acceptValues;
  private String[] rejectValues;

  public Filter() {}

  /*
   * Copy constructor. Only used within copy constructor for
   * GroupByScientificNameQuerySpec, so package private.
   */
  Filter(Filter other) {
    acceptRegexp = other.acceptRegexp;
    rejectRegexp = other.rejectRegexp;
    if (other.acceptValues != null) {
      acceptValues = Arrays.copyOf(other.acceptValues, other.acceptValues.length);
    }
    if (other.rejectValues != null) {
      rejectValues = Arrays.copyOf(other.rejectValues, other.rejectValues.length);
    }
  }

  /**
   * Returns the (literal) values to be accepted.
   *
   * @return the accepted values
   */
  public String[] getAcceptValues() {
    return acceptValues;
  }

  /**
   * Sets the (literal) values to be accepted.
   */
  public void acceptValues(String[] values) {
    this.acceptValues = values;
  }

  /**
   * Returns the (literal) values to be rejected.
   *
   * @return the values to be rejected
   */
  public String[] getRejectValues() {
    return rejectValues;
  }

  /**
   * Sets the (literal) values to be rejected.
   */
  public void rejectValues(String[] values) {
    this.rejectValues = values;
  }

  /**
   * Returns the regular expression that values should match.
   *
   * @return the regular expression that values should match
   */
  public String getAcceptRegexp() {
    return acceptRegexp;
  }

  /**
   * Sets the regular expression that values should match.
   */
  public void acceptRegexp(String regexp) {
    this.acceptRegexp = regexp;
  }

  /**
   * Returns the regular expression that values should <i>not</i> match.
   *
   * @return the regular expression that values should <i>not</i> match
   */
  public String getRejectRegexp() {
    return rejectRegexp;
  }

  /**
   * Sets the regular expression that values should <i>not</i> match.
   */
  public void rejectRegexp(String regexp) {
    this.rejectRegexp = regexp;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Filter)) {
      return false;
    }
    Filter other = (Filter) obj;
    return Objects.equals(acceptRegexp, other.acceptRegexp)
        && Objects.equals(rejectRegexp, other.rejectRegexp)
        && Objects.deepEquals(acceptValues, other.acceptValues)
        && Objects.deepEquals(rejectValues, other.rejectValues);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        acceptRegexp,
        rejectRegexp,
        Arrays.hashCode(acceptValues),
        Arrays.hashCode(rejectValues)
    );
  }

}
