package nl.naturalis.nba.utils;

import static nl.naturalis.nba.utils.StringUtil.zpad;

import java.time.Duration;
import java.util.ArrayList;

public class TimeUtil {

  /**
   * Utility class containing some methods useful for when working with time.
   *
   */
  public TimeUtil() {}

  /**
   * Get the duration between {@code start} and now, formatted as HH:mm:ss.
   *
   * @param start Begin time
   * @return Duration as String
   */
  public static String getDuration(long start) {
    return getDuration(start, System.currentTimeMillis());
  }

  /**
   * Get the duration between {@code start} and {@code end}, formatted as HH:mm:ss.
   *
   * @param start Begin
   * @param end End
   * @return Duration as String
   */
  public static String getDuration(long start, long end) {
    int millis = (int) (end - start);
    int hours = millis / (60 * 60 * 1000);
    millis = millis % (60 * 60 * 1000);
    int minutes = millis / (60 * 1000);
    millis = millis % (60 * 1000);
    int seconds = millis / 1000;
    return zpad(hours, 2, ":") + zpad(minutes, 2, ":") + zpad(seconds, 2);
  }

  /**
   * Format the given amount of milliseconds as ".. days, .. hours, .. minutes,
   * .. seconds and .. milliseconds".
   *
   * <p>NOTE:
   * - if a time unit is 0, it will be excluded from the formatted string
   * - two time units are separated by "and"
   * - when there are more than two, the latter is separated by "and" the
   *   others by a comma (", ").
   *
   *<p>E.g.:
   * 184612024 milliseconds : 2 days, 3 hours, 16 minutes, 52 seconds and 24 milliseconds
   *  46849540 milliseconds : 13 hours, 49 seconds and 540 milliseconds
   *
   * @param milliseconds  the number of milliseconds
   * @return  the formatted string
   */
  public static String formatMilliseconds(long milliseconds) {

    System.out.println(milliseconds);
    // A duration is considered to be positive
    if (milliseconds < 0) {
      milliseconds = milliseconds * -1;
    }
    Duration duration = Duration.ofMillis(milliseconds);
    String durationStr = "";
    ArrayList<String> timeUnits = new ArrayList<>();

    long days = duration.toDaysPart();
    if (days == 1) {
      timeUnits.add("1 day");
    } else if (days > 0) {
      timeUnits.add(days + " days");
    }
    long hours = duration.toHoursPart();
    if (hours == 1) {
      timeUnits.add("1 hour");
    } else if (hours > 0) {
      timeUnits.add(hours + " hours");
    }
    int minutes = duration.toMinutesPart();
    if (minutes == 1) {
      timeUnits.add("1 minute");
    } else if (minutes > 0) {
      timeUnits.add(minutes + " minutes");
    }
    int seconds = duration.toSecondsPart();
    if (seconds == 1) {
      timeUnits.add("1 second");
    } else if (seconds > 0) {
      timeUnits.add(seconds + " seconds");
    }
    int millis = duration.toMillisPart();
    if (millis == 1) {
      timeUnits.add("1 millisecond");
    } else if (millis > 0) {
      timeUnits.add(millis + " milliseconds");
    }

    if (timeUnits.size() == 0) {
      return "0 milliseconds";
    }
    int n = 1;
    for (String timeUnit : timeUnits) {
      durationStr = durationStr.concat(timeUnit + addSeparator(timeUnits.size() - n++));
    }
    return durationStr;
  }

  /**
   * Utility method belonging to @formatMilliseconds.
   *
   * @param unitsLeft the number of time elements that follow
   * @return the suitable separator
   */
  private static String addSeparator(int unitsLeft) {
    if (unitsLeft <= 0) {
      return "";
    }
    if (unitsLeft == 1) {
      return " and ";
    } else {
      return ", ";
    }
  }

}
