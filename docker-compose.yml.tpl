---
services:
  elasticsearch:
    image: registry.gitlab.com/naturalis/lib/elasticsearch:7.12.0
    container_name: elasticsearch
    environment:
      cluster.name: "nba-cluster"
      node.name: "nba-node"
      discovery.type: "single-node"
      bootstrap.memory_lock: "true"
      xpack.security.enabled: "false"
      xpack.monitoring.enabled: "false"
      ES_JAVA_OPTS: "-Xms2g -Xmx2g"
      network.host: 0.0.0.0
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - /data/nba/es7/data:/usr/share/elasticsearch/data
    ports:
      - "9200:9200"
      - "9300:9300"
...
