#!/bin/bash

# Application dirs
application_dir="/opt/etl"
cnf_dir="${application_dir}/conf"
lib_dir="${application_dir}/lib"
log_dir="${application_dir}/log"
export_dir="${application_dir}/export"

data_dir="/data"
data_in="${data_dir}/incoming"
data_out="${data_dir}/done"


# The java package that is the root of all NBA ETL code. Used by the
# other scripts to specify fully qualified class names.
root_package=nl.naturalis.nba.etl

# Whether or not to enable error suppression (causes the suppression of
# ERROR and WARN messages while still letting through INFO messages).
suppress_errors=false

# The number of documents to index at once
queue_size=1000

# Whether or not to delete all documents from a particular source
# system and document type before importing the data for that source
# system and document type (currently only picked up by Brahms
# importers; other importers just do it).
# truncate=true
truncate=false

# Whether or not to do a dry run (transform/validate the source data
# but not index it)
dry_run=false

# Provide a comma-separated list of genera to import. This will create a
# test set with only the specified genera from COL, NSR, CRS and BRAHMS.
# test_genera=malus,parus,larus,bombus,rhododendron,felix,tulipa,rosa,canis,passer,trientalis
test_genera=

# Make ${confDir} the first entry on the classpath so that resource/config
# files will be found there first.
classpath="${cnf_dir}"

function now() {
  date +"%Y-%m-%d %T"
}

function log_debug() {
  echo "$(date -Iseconds)|DEBUG|$(basename -- "$0"): $1"
}

function log_info() {
  echo "$(date -Iseconds)|INFO |$(basename -- "$0"): $1"
}

function log_warning() {
  echo -e "$(date -Iseconds)|WARN |$(basename -- "$0"): $1"
}

log_debug "data in: ${data_in}"
log_debug "data out: ${data_out}"
log_debug "export dir: ${export_dir}"

# MAIN

# Select the oldest job file
mapfile -t files < <(ls "$data_in"/*.json -atr 2> /dev/null )
if [ ${#files[@]} -gt 0 ]; then
  job_file=${files[0]}
  job_file_name=${job_file#"$data_in/"}
  log_info "job file selected: $job_file_name"
else
  log_info "no job files: nothing to do"
  exit 0
fi

# Get data supplier and source directory
JOB_ID=$( jq -r '.id' "$job_file" )
DATA_SUPPLIER=$( jq -r '.data_supplier' "$job_file" )
SOURCE_DIR=$( jq -r '.source_directory' "$job_file" )

log_info "job id: $JOB_ID"
log_info "data supplier: $DATA_SUPPLIER"
log_info "data source directory: $SOURCE_DIR"

# Prepare CLASSPATH
while IFS= read -r -d '' lib
do
  classpath="${classpath}:${lib}"
done < <(find "${lib_dir}" -type f -print0)

# Generate log file with name and full path
log_file="${log_dir}/etl-import-log--${JOB_ID}--$(date +%Y%m%d%H%M%S)"
log_debug "Log file: ${log_file}.log"

# Initial (xms) and maximum (xmx) memory allocation pool for the Java Virtual Machine (JVM)
JAVA_OPTS="-Xms2048m -Xmx2048m"

# Include other JVM parameters
JAVA_OPTS="${JAVA_OPTS} -Dfile.encoding=UTF-8"
JAVA_OPTS="${JAVA_OPTS} -Dlog4j.configurationFile=${cnf_dir}/log4j2.xml"
JAVA_OPTS="${JAVA_OPTS} -Dnba.conf.file=${cnf_dir}/nba.properties"
JAVA_OPTS="${JAVA_OPTS} -Dnl.naturalis.nba.etl.logDir=${log_dir}"
JAVA_OPTS="${JAVA_OPTS} -Dnl.naturalis.nba.logFileBaseName=${log_dir}/etl-log-$JOB_ID"
JAVA_OPTS="${JAVA_OPTS} -Dnl.naturalis.nba.etl.suppressErrors=${suppress_errors}"
JAVA_OPTS="${JAVA_OPTS} -Dnl.naturalis.nba.etl.queueSize=${queue_size}"
JAVA_OPTS="${JAVA_OPTS} -Dnl.naturalis.nba.etl.truncate=${truncate}"
JAVA_OPTS="${JAVA_OPTS} -Dnl.naturalis.nba.etl.dry=${dry_run}"
JAVA_OPTS="${JAVA_OPTS} -Dnl.naturalis.nba.etl.testGenera=${test_genera}"
JAVA_OPTS="${JAVA_OPTS} -Detl.source.dir=$SOURCE_DIR"

# Generate the ETL java command
if [[ "$DATA_SUPPLIER" == "nsr" ]]; then
  cmd="java -cp ${classpath} ${JAVA_OPTS} ${root_package}.nsr.NsrImporter --jobId ${JOB_ID}"
elif [[ "$DATA_SUPPLIER" == "dcsr" ]]; then
  cmd="java -cp ${classpath} ${JAVA_OPTS} ${root_package}.dcsr.DcsrImporter --jobId ${JOB_ID}"
else
  log_info "Not implemented import method for $DATA_SUPPLIER yet ... Job halted"
  rm -R "${data_in}/${SOURCE_DIR:?}"
  jq ".status = \"STOPPED\" | etl_warning = \"ETL cannot transform source files from ${DATA_SUPPLIER} yet\" |.etl_finished = \"$(now)\"" "${job_file}" > "$data_out/${job_file_name}"
  rm "${job_file}"
  exit 0
fi

# Run the ETL module for the selected job
log_info "Start transforming ${DATA_SUPPLIER} data files from job ${JOB_ID}"

if ! $cmd >> /dev/null;
then
  log_warning "Transformation of source files failed"
  # 1. create an archive only when there is a report / log file
  if [[ $( ls "${log_dir}/*${JOB_ID}*" ) ]]; then
    mkdir "${export_dir}/${JOB_ID}/reports/"
    mv "${log_dir}/*${JOB_ID}*" "${export_dir}/${JOB_ID}/reports/."
    tar czf "${data_out}/${JOB_ID}-etl.tar.gz" -C "${export_dir}/${JOB_ID}" .
    etl_file="${JOB_ID}-etl.tar.gz"
  fi
  # 2. clean up source data and intermediate export data
  rm -R "${data_in}/${SOURCE_DIR:?}"
  rm -R "${export_dir}/${JOB_ID:?}"
  # 3. update the job file
  jq ".status = \"FAILED\" | .etl_output = \"${etl_file}\" | .etl_finished = \"$(now)\"" "${job_file}" > "$data_out/${job_file_name}"
else
  log_info "Transformation of source files finished successfully"
  # 1. include logs files and reports
  mkdir "${export_dir}/${JOB_ID}/reports/"
  move="mv ${log_dir}/*${JOB_ID}* ${export_dir}/${JOB_ID}/reports/."
  if ! $move;
  then
     log_warning "could not add reports"
  fi
  # 2. create a zipped archive with the result
  tar czf "${data_out}/${JOB_ID}-etl.tar.gz" -C "${export_dir}/${JOB_ID}" .
  # clean up source data and intermediate export data
  rm -R "${export_dir}/${JOB_ID:?}"
  rm -R "${data_in}/${SOURCE_DIR:?}"
  # 3. clean up source data and intermediate export data
  jq ".status = \"TRANSFORMED\" | .etl_output = \"${JOB_ID}-etl.tar.gz\" | .etl_finished = \"$(now)\"" "${job_file}" > "$data_out/${job_file_name}"
fi

rm "${job_file}"
log_info "Finished job ${JOB_ID}"
