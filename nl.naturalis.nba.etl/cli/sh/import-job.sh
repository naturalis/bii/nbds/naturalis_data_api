#!/bin/bash

. ./include.sh

data_dir_in="/data/incoming"
data_dir_out="/data/done"
data_dir_export="/opt/etl/export"

function log_debug() {
  echo "$(date -Iseconds)|DEBUG|$(basename -- "$0"): $1"
}

function log_error() {
  echo "$(date -Iseconds)|ERROR|$(basename -- "$0"): $1" 1>&2
}

function log_info() {
  echo "$(date -Iseconds)|INFO |$(basename -- "$0"): $1"
}

function log_warning() {
  echo -e "$(date -Iseconds)|WARN |$(basename -- "$0"): $1"
}

function exit_on_error() {
  log_error "$1"
  exit 1
}

function remove_failed_job() {
  end=$(date +"%Y-%m-%d %T")
  jq '.status = "FAILED" | .etl_finished = "$end"' "$data_dir_export/$job_file_name.in_progress" > "$data_dir_out/$job_file_name"
  rm "$data_dir_export/$job_file_name.in_progress"
  rm -R "$SOURCE_DIR"
}

log_info "Started ETL module"

# Select the oldest job file from the data directory
mapfile -t files < <( ls "$data_dir_in"/*.json -atr 2> /dev/null )
if [ ${#files[@]} -gt 0 ]; then
  job_file=${files[0]}
  job_file_name=${job_file#"$data_dir_in"}
  log_info "job file selected: $job_file_name"
else
  log_debug "no job files: nothing to do"
  end=$(date +"%Y-%m-%d %T")
  log_info "finished at $end"
  exit 0
fi

# Get the data supplier and the directory holding the source files
DATA_SUPPLIER=$( jq --raw-output '.data_supplier' "$job_file" )
JOB_ID=$( jq --raw-output '.id' "$job_file" )
SOURCE_DIR=$( jq --raw-output '.source_directory' "$job_file" )
SOURCE_DIR="$data_dir_in/$SOURCE_DIR"

log_debug "DATA_SUPPLIER: $DATA_SUPPLIER"
log_debug "JOB_ID: $JOB_ID"
log_debug "SOURCE_DIR: $SOURCE_DIR"
log_debug "JAVA_OPTS: $JAVA_OPTS"

# Import command
if [ "$DATA_SUPPLIER" == "nsr" ]; then
  IMPORT_COMMAND="java -cp ${classpath} $JAVA_OPTS ${root_package}.nsr.NsrImporter --jobId $JOB_ID --sourceFiles $SOURCE_DIR"
elif [ "$DATA_SUPPLIER" == "dcsr" ]; then
  IMPORT_COMMAND="java -cp ${classpath} $JAVA_OPTS ${root_package}.dcsr.DcsrImporter --jobId $JOB_ID --sourceFiles $SOURCE_DIR"
else
  exit_on_error "Unknown data supplier: $DATA_SUPPLIER"
fi

log_info "Running job with id $JOB_ID for $DATA_SUPPLIER"
log_debug "IMPORT_COMMAND: $IMPORT_COMMAND"

mv "$job_file" "$data_dir_export/$job_file_name.in_progress"
tar xzvf $data_dir_in/$SOURCE_DIR/$DATA_SUPPLIER*.tar.gz --directory $data_dir_in/$SOURCE_DIR
if ! "$IMPORT_COMMAND"; then
  remove_failed_job
  exit_on_error "ETL transform failed"
fi

if ! ( cd "$SOURCE_DIR" && tar czvf "$data_dir_out/$JOB_ID.tar.gz" . ); then
  remove_failed_job
  exit_on_error "Failed to create data archive"
else
  cd ..
  rm -R "$SOURCE_DIR"
fi

# Update successful job
end=$(date +"%Y-%m-%d %T")
jq '.status = "TRANSFORMED" | .etl_export_file = "$JOB_ID.tar.gz" | .etl_finished = "$end"' "$data_dir_export/$job_file_name.in_progress" > "$data_dir_out/$job_file_name"
rm "$data_dir_export/$job_file_name.in_progress"

log_info "finished at $end"
