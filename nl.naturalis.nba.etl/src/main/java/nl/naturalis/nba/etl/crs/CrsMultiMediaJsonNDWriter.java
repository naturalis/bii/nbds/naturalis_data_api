package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;

import java.io.IOException;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.JsonNDWriter;
import nl.naturalis.nba.etl.report.Report;

/**
 * The JsonNDWriter component in the ETL cycle for CRS multimedia objects.
 */
public class CrsMultiMediaJsonNDWriter extends JsonNDWriter<MultiMediaObject> {

  public CrsMultiMediaJsonNDWriter(String sourceFile, Report report, ETLStatistics stats) throws IOException {
    super(MULTI_MEDIA_OBJECT, "CRS", sourceFile, report, stats);
  }
}
