package nl.naturalis.nba.etl.geo;

import static nl.naturalis.nba.dao.DocumentType.GEO_AREA;

import java.io.IOException;
import nl.naturalis.nba.api.model.GeoArea;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.JsonNDWriter;
import nl.naturalis.nba.etl.report.Report;

/**
 * The JsonNDWriter component in the ETL cycle for Geo Areas.
 */
@SuppressWarnings("CheckStyle")
public class GeoJsonNDWriter extends JsonNDWriter<GeoArea> {

  public GeoJsonNDWriter(String sourceFile, Report report, ETLStatistics stats) throws IOException {
    super(GEO_AREA, "Geo", sourceFile, report, stats);
  }
}
