package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.etl.ETLUtil.hasValue;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.calcfullname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.classname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.familyname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.formaauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.genusname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.kingdom;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.ordername;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.phylum;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.speciesauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.speciesname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subclassname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subfamily;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subkingdom;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subphylum;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subspeciesauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subtribe;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.tribe;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.varietyauthorname;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.Monomial;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.TaxonomicRank;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.ETLRegistry;
import nl.naturalis.nba.etl.TransformUtil;
import org.apache.logging.log4j.Logger;

/**
 * Provides common functionality related to the Brahms ETL cycle.
 *
 * @author Ayco Holleman
 */
class BrahmsImportUtil {

  private static final Logger logger = ETLRegistry.getInstance().getLogger(BrahmsImportUtil.class);

  BrahmsImportUtil() {}

  /**
   * Provides a list of CSV files to process. Only files whose name end with {@code .csv}
   * (case-insensitive) will be processed.
   *
   * @return sorted array of csv file names
   */
  public static File[] getCsvFiles() {
    File[] files = getDataDir().listFiles((dir, name) -> name.toLowerCase().endsWith(".csv"));
    if (files != null) {
      Arrays.sort(files);
    }
    return files;
  }

  /**
   * Creates a backup of successfully processed CSV files by appending a datetime stamp and a {@code
   * .imported} file extension to their name.
   */
  public static void backup() {
    SimpleDateFormat fileNameDateFormatter = new SimpleDateFormat("yyyyMMdd");
    String ext = "." + fileNameDateFormatter.format(new Date()) + ".imported";
    for (File f : getCsvFiles()) {
      if (f.renameTo(new File(f.getAbsolutePath() + ext))) {
        logger.debug("Saved backup of {}", f.getName());
      } else {
        logger.debug("Failed to backup {}", f.getName());
      }
    }
  }

  /**
   * Removes the {@code .imported} file extension from files that have it, causing them to be
   * re-processed the next time an import is started.
   */
  public static void removeBackupExtension() {
    File dir = getDataDir();
    File[] files = dir.listFiles((dir1, name) -> name.toLowerCase().endsWith(".imported"));
    if (files == null) {
      return;
    }
    for (File file : files) {
      int pos = file.getName().toLowerCase().indexOf(".csv");
      String chopped = file.getName().substring(0, pos + 4);
      logger.info("{} ---> {}", file.getName(), chopped);
      chopped = dir.getAbsolutePath() + "/" + chopped;
      if (file.renameTo(new File(chopped))) {
        logger.debug("Removed .imported from file name {}", file.getName());
      }
    }
  }

  /**
   * Extracts a {@code ScientificName} instance from a raw CSV record.
   *
   * @param csvRecord Brahms CSV record
   * @return a ScientificName
   */
  public static ScientificName getScientificName(CSVRecordInfo<BrahmsCsvField> csvRecord) {
    ScientificName sn = new ScientificName();
    sn.setFullScientificName(csvRecord.get(calcfullname));
    sn.setGenusOrMonomial(csvRecord.get(genusname));
    sn.setSpecificEpithet(csvRecord.get(speciesname));
    sn.setAuthorshipVerbatim(getAuthorShipVerbatim(csvRecord));
    sn.setInfraspecificMarker(getInfraspecificMarker(csvRecord));
    sn.setInfraspecificEpithet(getInfraspecificEpithet(csvRecord));
    TransformUtil.setScientificNameGroup(sn);
    return sn;
  }

  /**
   * Constructs a {@code DefaultClassification} from a raw CSV record
   *
   * @param csvRecord a CSV record
   * @return a DefaultClassification
   */
  public static DefaultClassification getDefaultClassification(
      CSVRecordInfo<BrahmsCsvField> csvRecord) {
    DefaultClassification dc = new DefaultClassification();
    dc.setKingdom(csvRecord.get(kingdom));
    dc.setSubKingdom(csvRecord.get(subkingdom));
    dc.setPhylum(csvRecord.get(phylum));
    dc.setSubPhylum(csvRecord.get(subphylum));
    dc.setClassName(csvRecord.get(classname));
    dc.setSubClass(csvRecord.get(subclassname));
    dc.setOrder(csvRecord.get(ordername));

    // Is this still required?
    dc.setFamily(removeFungusPrefix(csvRecord.get(familyname)));

    dc.setSubFamily(csvRecord.get(subfamily));
    dc.setTribe(csvRecord.get(tribe));
    dc.setSubTribe(csvRecord.get(subtribe));
    dc.setGenus(csvRecord.get(genusname));
    dc.setSpecificEpithet(csvRecord.get(speciesname));
    dc.setInfraspecificEpithet(getInfraspecificEpithet(csvRecord));
    dc.setInfraspecificRank(getTaxonRank(csvRecord));
    return dc;
  }

  /*
   * Removes the prefix "Fungi-" of a family or class name.
   * NOTE: when the source value equals "fungi", NULL is returned because this
   * can never be the family or class name of a species.
   */
  private static String removeFungusPrefix(String str) {
    if (str == null || str.equalsIgnoreCase("fungi")) {
      return null;
    }
    String prefix = "(?i)(^fungi-)";
    str = str.replaceFirst(prefix, "");
    return str;
  }

  /**
   * Define the value to be used for setting infraspecificMarker.
   *
   * @param csvRecord the current Brahms csv record
   * @return infraspecificMarker
   */
  private static String  getInfraspecificMarker(CSVRecordInfo<BrahmsCsvField> csvRecord) {
    String forma = csvRecord.get(BrahmsCsvField.forma);
    if (hasValue(forma)) {
      return "f.";
    }
    String variety = csvRecord.get(BrahmsCsvField.variety);
    if (hasValue(variety)) {
      return "var.";
    }
    String subspecies = csvRecord.get(BrahmsCsvField.subspecies);
    if (hasValue(subspecies)) {
      return "subsp.";
    }
    return null;
  }

  /**
   * Define the value to be used for setting infraspecificEpithet.
   *
   * @param csvRecord the current Brahms csv record
   * @return infraspecificEpithet
   */
  private static String getInfraspecificEpithet(CSVRecordInfo<BrahmsCsvField> csvRecord) {
    String forma = csvRecord.get(BrahmsCsvField.forma);
    if (hasValue(forma)) {
      return forma.trim();
    }
    String variety = csvRecord.get(BrahmsCsvField.variety);
    if (hasValue(variety)) {
      return variety.trim();
    }
    String subspecies = csvRecord.get(BrahmsCsvField.subspecies);
    if (hasValue(subspecies)) {
      return subspecies.trim();
    }
    return null;
  }

  /**
   * Define the value to be used for setting identifications.taxonRank.
   *
   * @param csvRecord the current Brahms csv record
   * @return most precise taxon rank
   */
  public static String getTaxonRank(CSVRecordInfo<BrahmsCsvField> csvRecord) {
    String infraspecificMarker = getInfraspecificMarker(csvRecord);
    if (infraspecificMarker != null) {
      return infraspecificMarker;
    }
    if (csvRecord.get(speciesname) != null) {
      return TaxonomicRank.SPECIES.getEnglishName();
    }
    if (csvRecord.get(genusname) != null) {
      return TaxonomicRank.GENUS.getEnglishName();
    }
    if (csvRecord.get(subtribe) != null) {
      return TaxonomicRank.SUBTRIBE.getEnglishName();
    }
    if (csvRecord.get(tribe) != null) {
      return TaxonomicRank.TRIBE.getEnglishName();
    }
    if (csvRecord.get(subfamily) != null) {
      return TaxonomicRank.SUBFAMILY.getEnglishName();
    }
    if (csvRecord.get(familyname) != null) {
      return TaxonomicRank.FAMILY.getEnglishName();
    }
    if (csvRecord.get(ordername) != null) {
      return TaxonomicRank.ORDER.getEnglishName();
    }
    if (csvRecord.get(subclassname) != null) {
      return TaxonomicRank.SUBCLASS.getEnglishName();
    }
    if (csvRecord.get(classname) != null) {
      return TaxonomicRank.CLASS.getEnglishName();
    }
    if (csvRecord.get(subphylum) != null) {
      return TaxonomicRank.SUBPHYLUM.getEnglishName();
    }
    if (csvRecord.get(phylum) != null) {
      return TaxonomicRank.PHYLUM.getEnglishName();
    }
    if (csvRecord.get(subkingdom) != null) {
      return TaxonomicRank.SUBKINGDOM.getEnglishName();
    }
    if (csvRecord.get(kingdom) != null) {
      return TaxonomicRank.KINGDOM.getEnglishName();
    }
    return null;
  }

  protected static String getAuthorShipVerbatim(CSVRecordInfo<BrahmsCsvField> csvRecord) {
    String forma = csvRecord.get(BrahmsCsvField.forma);
    if (hasValue(forma) && !forma.equalsIgnoreCase("NULL")) {
      return csvRecord.get(formaauthorname);
    }
    String variety = csvRecord.get(BrahmsCsvField.variety);
    if (hasValue(variety) && !variety.equalsIgnoreCase("NULL")) {
      return csvRecord.get(varietyauthorname);
    }
    String subspecies = csvRecord.get(BrahmsCsvField.subspecies);
    if (hasValue(subspecies) && !subspecies.equalsIgnoreCase("NULL")) {
      return csvRecord.get(subspeciesauthorname);
    }
    return csvRecord.get(speciesauthorname);
  }

  /**
   * Converts a {@code DefaultClassification} instance to a system classification (which is just a
   * list of {@code Monomial}s).
   *
   * @param dc a DefaultClassification
   * @return list containing classification monomials
   */
  public static List<Monomial> getSystemClassification(DefaultClassification dc) {
    List<Monomial> sc = new ArrayList<>(8);
    if (dc.getKingdom() != null) {
      sc.add(new Monomial(TaxonomicRank.KINGDOM, dc.getKingdom()));
    }
    if (dc.getOrder() != null) {
      sc.add(new Monomial(TaxonomicRank.ORDER, dc.getOrder()));
    }
    if (dc.getFamily() != null) {
      sc.add(new Monomial(TaxonomicRank.FAMILY, dc.getFamily()));
    }
    if (dc.getGenus() != null) {
      sc.add(new Monomial(TaxonomicRank.GENUS, dc.getGenus()));
    }
    if (dc.getSpecificEpithet() != null) {
      sc.add(new Monomial(TaxonomicRank.SPECIES, dc.getSpecificEpithet()));
    }
    if (dc.getInfraspecificEpithet() != null) {
      sc.add(new Monomial(TaxonomicRank.SUBSPECIES, dc.getInfraspecificEpithet()));
    }
    return sc;
  }

  public static File getDataDir() {
    return DaoRegistry.getInstance().getConfiguration().getDirectory("brahms.data.dir");
  }
}
