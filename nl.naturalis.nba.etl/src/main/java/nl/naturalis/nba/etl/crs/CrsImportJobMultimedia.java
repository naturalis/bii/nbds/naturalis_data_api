package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.api.model.SourceSystem.CRS;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_ENRICH;
import static nl.naturalis.nba.etl.crs.CrsImportUtil.getXmlFiles;

import java.io.File;
import java.io.IOException;
import java.util.List;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLRegistry;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.MimeTypeCacheFactory;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.XMLRecordInfo;
import nl.naturalis.nba.etl.normalize.PhaseOrStageNormalizer;
import nl.naturalis.nba.etl.normalize.SexNormalizer;
import nl.naturalis.nba.etl.normalize.SpecimenTypeStatusNormalizer;
import nl.naturalis.nba.etl.report.ProcessingError;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Status;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * Class that manages the import of CRS multimedia.
 *
 * @author Tom Gilissen
 */
public class CrsImportJobMultimedia {

  private static final Logger logger;
  private static final boolean doEnrich;

  private final Report report;
  private final boolean suppressErrors;

  private ETLStatistics stats;
  private CrsMultiMediaTransformer transformer;
  private DocumentObjectWriter<MultiMediaObject> loader;

    static {
      logger = ETLRegistry.getInstance().getLogger(CrsImportJobMultimedia.class);
      doEnrich = DaoRegistry.getInstance().getConfiguration().get(SYSPROP_ETL_ENRICH, "false").equals("true");
    }

    /**
     * Offline processing of previously harvested specimen source files.
     */
    public CrsImportJobMultimedia(String jobId) {
      suppressErrors = ConfigObject.isEnabled("suppressErrors");
      this.report = new Report(jobId, CRS, MULTI_MEDIA_OBJECT);
      report.initialiseUrls();
      logger.info("ETL Multimedia report: {}", report.getReportName());
    }

    /**
     * Import multimedia from the data directory configured in nda-import.properties.
     */
    public void importMultimedia() {
      long start = System.currentTimeMillis();
      report.setStatus(Status.IN_PROGRESS);
      report.setThemesVersion(ThemeCache.getInstance().getThemesVersion());
      int cacheFailuresBegin = MimeTypeCacheFactory.getInstance().getCache().getMisses();
      stats = new ETLStatistics();
      stats.setOneToMany(true);

      File[] xmlFiles = getXmlFiles(MULTI_MEDIA_OBJECT);
      if (xmlFiles == null || xmlFiles.length == 0) {
        report.setStatus(Status.FAILED);
        logger.error("No multimedia oai.xml files found. Check nda-import.propties");
        return;
      }

      transformer = new CrsMultiMediaTransformer(report, stats);
      transformer.setSuppressErrors(suppressErrors);
      if (doEnrich) {
        transformer.setEnrich(true);
        report.doEnrich();
        logger.info("Taxonomic enrichment of Specimen documents: true");
      }

      SexNormalizer.getInstance().resetStatistics();
      SpecimenTypeStatusNormalizer.getInstance().resetStatistics();
      PhaseOrStageNormalizer.getInstance().resetStatistics();
      ThemeCache.getInstance().resetMatchCounters();

      try {
        for (File f : xmlFiles) {
          importFile(f);
          report.save();
        }
      } finally {
        logger.info("Finished importing {} files", xmlFiles.length);
        report.setFinished();
      }
      report.addThemesSummary(ThemeCache.getInstance().getThemesSummary());
      logger.info(report.save());

      SexNormalizer.getInstance().logStatistics();
      SpecimenTypeStatusNormalizer.getInstance().logStatistics();
      PhaseOrStageNormalizer.getInstance().logStatistics();
      ThemeCache.getInstance().logMatchInfo();
      stats.logStatistics(logger);
      ETLUtil.logDuration(logger, getClass(), start);
      int cacheFailuresEnd = MimeTypeCacheFactory.getInstance().getCache().getMisses();
      if (cacheFailuresBegin != cacheFailuresEnd) {
        int misses = cacheFailuresEnd - cacheFailuresBegin;
        String fmt = "%d mime type cache lookup failures for CRS multimedia";
        logger.warn(String.format(fmt, misses));
      }
    }

    private void importFile(File f) {
      logger.info("Processing file {}", f.getName());
      report.addSourceFile(f);

      try {
        loader = new CrsMultiMediaJsonNDWriter(f.getName(), report, stats);
      } catch (IOException e) {
        report.addProcessingError(ProcessingError.IO_ERROR, f);
        logger.error("Failed to create export file for source file {}: {}", f.getName(), e.getMessage());
      }

      CrsExtractor extractor;
      try {
        extractor = new CrsExtractor(f, stats);
      } catch (SAXException e) {
        report.addProcessingError(ProcessingError.PARSING_ERROR, e.getMessage() + ": " + f.getName());
        logger.error("Processing failed!");
        logger.error(e.getMessage());
        return;
      }

      for (XMLRecordInfo extracted : extractor) {
        List<MultiMediaObject> transformed = transformer.transform(extracted);
        loader.write(transformed);
        if (stats.recordsProcessed != 0 && stats.recordsProcessed % 50000 == 0) {
          logger.info("Records processed: {}", stats.recordsProcessed);
          logger.info("Documents indexed: {}", stats.documentsIndexed);
        }
      }
      IOUtil.close(loader);
    }

  }
