package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.api.model.SourceSystem.CRS;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.etl.crs.CrsImportUtil.callMultimediaService;

import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.etl.ETLConstants;
import nl.naturalis.nba.etl.ETLRegistry;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.XMLRecordInfo;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;

/**
 * Class that manages the import of CRS multimedia, sourced through "live" calls
 * to the CRS OAI service.
 *
 * @author Ayco Holleman
 *
 * @see CrsMultiMediaImportOffline
 *
 */
public class CrsMultiMediaImport {

  public static void main(String[] args) {
    try {
      CrsMultiMediaImport importer = new CrsMultiMediaImport();
      importer.importMultimedia();
    } finally {
      ESClientManager.getInstance().closeClient();
    }
  }

  private static final Logger logger;

  static {
    logger = ETLRegistry.getInstance().getLogger(CrsMultiMediaImport.class);
  }

  private final boolean suppressErrors;
  private final int esBulkRequestSize;

  private Report report;
  private ETLStatistics stats;
  private CrsMultiMediaTransformer transformer;
  private CrsMultiMediaLoader loader;

  public CrsMultiMediaImport() {
    suppressErrors = ConfigObject.isEnabled("crs.suppress-errors");
    report = new Report(UUID.randomUUID().toString(), CRS, MULTI_MEDIA_OBJECT);
    String key = ETLConstants.SYSPROP_LOADER_QUEUE_SIZE;
    String val = System.getProperty(key, "1000");
    esBulkRequestSize = Integer.parseInt(val);
  }

  /**
   * Import multimedia through repetitive calls to the CRS OAI service.
   */
  public void importMultimedia() {
    long start = System.currentTimeMillis();
    try {
    ETLUtil.truncate(MULTI_MEDIA_OBJECT, CRS);
    } catch (DaoException e) {
      logger.error("Failed to truncate the CRS multimedia index: {}", e.getMessage());
      System.exit(1);
    }
    stats = new ETLStatistics();
    stats.setOneToMany(true);
    transformer = new CrsMultiMediaTransformer(report, stats);
    transformer.setSuppressErrors(suppressErrors);
    loader = new CrsMultiMediaLoader(esBulkRequestSize, stats);
    ThemeCache.getInstance().resetMatchCounters();
    try {
      String resumptionToken = null;
      do {
        byte[] response = callMultimediaService(resumptionToken);
        resumptionToken = processResponse(response);
      } while (resumptionToken != null);
    } finally {
      IOUtil.close(loader);
    }
    ThemeCache.getInstance().logMatchInfo();
    stats.logStatistics(logger);
    ETLUtil.logDuration(logger, getClass(), start);
  }

  private String processResponse(byte[] bytes) {
    CrsExtractor extractor = new CrsExtractor(bytes, stats);
    for (XMLRecordInfo extracted : extractor) {
      List<MultiMediaObject> transformed = transformer.transform(extracted);
      loader.write(transformed);
      if (stats.recordsProcessed % 50000 == 0) {
        logger.info("Records processed: " + stats.recordsProcessed);
      }
    }
    return extractor.getResumptionToken();
  }
}
