package nl.naturalis.nba.etl.crs;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Date;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.ETLRegistry;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.http.SimpleHttpGet;
import org.apache.logging.log4j.Logger;

/**
 * Class providing common functionality for CRS imports.
 *
 * @author Ayco Holleman
 *
 */
class CrsImportUtil {

  static final ConfigObject config;
  static final SimpleDateFormat oaiDateFormatter;

  private static final Logger logger;

  static {
    logger = ETLRegistry.getInstance().getLogger(CrsImportUtil.class);
    config = DaoRegistry.getInstance().getConfiguration();
    oaiDateFormatter = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'Z\'");
  }

  private CrsImportUtil() {}

  /**
   * Generates and executes an initial request to the CRS OAI service for
   * specimens and returns the response as a byte array.
   *
   * @param fromDate   the start date of the harvest interval
   * @param untilDate  the end date of the harvest interval
   * @return the specimen harvest service
   */
  static byte[] callSpecimenService(Date fromDate, Date untilDate) {
    String url = config.required("crs.specimens.url.initial");
    if (fromDate != null) {
      url += "&from=" + oaiDateFormatter.format(fromDate);
    }
    if (untilDate != null) {
      url += "&until=" + oaiDateFormatter.format(untilDate);
    }
    return callService(url);
  }

  /**
   * Calls the CRS OAI service for specimens using the specified resumption
   * token and returns the response as a byte array.
   *
   * @param resumptionToken  the resumption token
   * @return the specimen harvest service
   */
  static byte[] callSpecimenService(String resumptionToken) {
    String url;
    if (resumptionToken == null) {
      url = config.required("crs.specimens.url.initial");
      int maxAge = config.required("crs.max_age", int.class);
      if (maxAge != 0) {
        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime wayback = now.minusHours(maxAge);
        url += "&from=" + oaiDateFormatter.format(LocalDate.from(wayback));
      }
    } else {
      String urlPattern = config.required("crs.specimens.url.resume");
      url = String.format(urlPattern, resumptionToken);
    }
    return callService(url);
  }

  /**
   * Generates and executes an initial request to the CRS OAI service for
   * specimens and returns the response as a byte array.
   *
   * @param fromDate   the start date of the harvest interval
   * @param untilDate  the end date of the harvest interval
   * @return the specimen harvest service

   */
  static byte[] callMultimediaService(Date fromDate, Date untilDate) {
    String url = config.required("crs.multimedia.url.initial");
    if (fromDate != null) {
      url += "&from=" + oaiDateFormatter.format(fromDate);
    }
    if (untilDate != null) {
      url += "&until=" + oaiDateFormatter.format(untilDate);
    }
    return callService(url);
  }

  /**
   * Calls the CRS OAI service for multimedia using the specified resumption
   * token and returns the response as a byte array.
   *
   * @param resumptionToken the resumption token
   * @return the multimedia harvest service
   */
  static byte[] callMultimediaService(String resumptionToken) {
    String url;
    if (resumptionToken == null) {
      url = config.required("crs.multimedia.url.initial");
      int maxAge = config.required("crs.max_age", int.class);
      if (maxAge != 0) {
        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime wayback = now.minusHours(maxAge);
        url += "&from=" + oaiDateFormatter.format(LocalDate.from(wayback));
      }
    } else {
      url = String.format(config.required("crs.multimedia.url.resume"), resumptionToken);
    }
    return callService(url);
  }

  private static byte[] callService(String url) {
    logger.info("Calling service: " + url);
    return new SimpleHttpGet().setBaseUrl(url).execute().getResponseBody();
  }

  protected static File[] getXmlFiles(DocumentType<?> dt) {
    ConfigObject config = DaoRegistry.getInstance().getConfiguration();
    String path = config.required("crs.data.dir").concat("/");
    if (dt.getName().equalsIgnoreCase("specimen")) {
      path = path.concat("specimen");
    } else {
      path = path.concat("media");
    }

    logger.info("Data directory for CRS specimen import: " + path);
    File[] files =
        new File(path)
            .listFiles(
                new FilenameFilter() {

                  public boolean accept(File dir, String name) {
                    //if (!name.startsWith("specimens.")) {
                    if (!name.startsWith("oai-pmh-request")) {
                      return false;
                    }
                    //if (!name.endsWith(".oai.xml")) {
                    return name.endsWith(".xml");
                  }
                });
    logger.debug("Sorting file list");
    if (files != null) {
      Arrays.sort(files);
      return files;
    }
    return null;
  }


}
