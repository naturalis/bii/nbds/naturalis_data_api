package nl.naturalis.nba.etl;

import static nl.naturalis.nba.etl.ETLConstants.PURL_SERVER_BASE_URL;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_TEST_GENERA;
import static nl.naturalis.nba.utils.TimeUtil.getDuration;

import java.io.StringWriter;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.util.es.ESUtil;
import nl.naturalis.nba.utils.ConfigObject;
import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;

/**
 * Utility class providing common functionality used throughout this library.
 *
 * @author Ayco Holleman
 * @author Tom Gilissen
 *
 */
@SuppressWarnings({"CheckStyle", "RegExpRedundantEscape"})
public final class ETLUtil {

  private static final Logger logger = getLogger(Loader.class);

  private static final URIBuilder purlBuilder;
  private static final String purlSpecimenPath;
  private static final TransformerFactory tf;
  private static final StringWriter writer;
  private static final String PATTERN01 = "\\>[\\n][\\s]+";
  private static final String PATTERN02 = "\\\"";

  static {
    purlBuilder = getPurlBuilder();
    if (purlBuilder.getPath() != null) {
      purlSpecimenPath = purlBuilder.getPath() + "/naturalis/specimen/";
    } else {
      purlSpecimenPath = "/naturalis/specimen/";
    }
    tf = TransformerFactory.newInstance();
    writer = new StringWriter(4096);
  }

  public ETLUtil() {}

  /**
   * Get root cause of the specified {@code Throwable}. Returns the {@code Throwable} itself if it
   * doesn't have a cause.
   *
   * @param t Throwable cause
   * @return Root cause
   */
  public static Throwable getRootCause(Throwable t) {
    while (t.getCause() != null) {
      t = t.getCause();
    }
    return t;
  }

  /**
   * Logs a nice message about how long an import program took.
   *
   * @param logger The logger to log to
   * @param cls The main class of the import program
   * @param start The start of the program
   */
  public static void logDuration(Logger logger, Class<?> cls, long start) {
    logger.info(cls.getSimpleName() + " took " + getDuration(start));
  }

  /**
   * Return the specimen purl for the given unitID.
   *
   * @param unitID  the unitID
   * @return specimen purl
   */
  @SuppressWarnings("checkstyle:AbbreviationAsWordInName")
  public static String getSpecimenPurl(String unitID) {
    try {
      purlBuilder.setPath(purlSpecimenPath + unitID);
      return purlBuilder.build().toString();
    } catch (URISyntaxException e) {
      throw new ETLRuntimeException(e);
    }
  }

  public static Logger getLogger(Class<?> forClass) {
    return ETLRegistry.getInstance().getLogger(forClass);
  }

  /**
   * Truncate or delete an index.
   *
   * @param dt The DocumentType of the index to be truncated
   * @throws DaoException if truncate fails (i.e. not all documents could be deleted)
   */
  public static <T extends IDocumentObject> void truncate(DocumentType<T> dt) throws DaoException {
    if (ConfigObject.isEnabled(ETLConstants.SYSPROP_DRY_RUN)) {
      logger.info("Truncate skipped dry run mode");
      return;
    }
    ESUtil.truncate(dt);
  }

  /**
   * Truncate or delete an index.
   *
   * @param dt The DocumentType of the index to be truncated
   * @param ss The SourceSystem of the index to be truncated
   * @throws DaoException if truncate fails (i.e. not all documents could be deleted)
   */
  public static <T extends IDocumentObject> void truncate(DocumentType<T> dt, SourceSystem ss) throws DaoException {
    if (ConfigObject.isEnabled(ETLConstants.SYSPROP_DRY_RUN)) {
      logger.info("Truncate skipped dry run mode");
      return;
    }
    ESUtil.truncate(dt, ss);
  }

  public static String[] getTestGenera() {
    String s = System.getProperty(SYSPROP_TEST_GENERA);
    if (s == null || s.isBlank()) {
      return null;
    }
    String[] testGenera = s.split(",");
    for (int i = 0; i < testGenera.length; i++) {
      testGenera[i] = testGenera[i].trim().toLowerCase();
    }
    return testGenera;
  }

  private static URIBuilder getPurlBuilder() {
    String value = null;
    try {
      value = DaoRegistry.getInstance().getConfiguration().get("purl.baseurl");
      if (value == null || value.trim().isEmpty()) {
        value = PURL_SERVER_BASE_URL;
      }
      return new URIBuilder(value);
    } catch (URISyntaxException e) {
      String fmt = "Could not create URIBuilder for PURL base URL \"%s\": %s";
      String msg = String.format(fmt, value, e.getMessage());
      throw new ETLRuntimeException(msg);
    }
  }

  /**
   * Utility method for printing objects to something readable.
   *
   * @param obj the object
   * @return readable string
   */
  public static String inputToString(Object obj) {
    if (obj == null) {
      return null;
    }
    if (obj instanceof XMLRecordInfo) {
      obj = ((XMLRecordInfo) obj).getRecord();
    }
    if (obj instanceof Element element) {
        try {
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, StandardCharsets.UTF_8.name());
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "no");
        writer.getBuffer().setLength(0); // reset the writer
        transformer.transform(new DOMSource(element), new StreamResult(writer));
        String output = writer.toString();
        output = output.replaceAll(PATTERN01, ">");
        output = output.replaceAll(PATTERN02, "'");
        return output;
        } catch (TransformerException e) {
          return "Invalid xml: " + e.getMessage();
        }
    }
    return JsonUtil.toJson(obj);
  }

  /**
   * Utility method for checking if a string is:
   * - not null, and
   * - has some value other than white spaces.
   *
   * @param str the string to be checked
   * @return true when it has some value (other that white spaces)
   */
  public static boolean hasValue(String str) {
    return (str != null && !str.isBlank() && !str.equalsIgnoreCase("NULL"));
  }
}
