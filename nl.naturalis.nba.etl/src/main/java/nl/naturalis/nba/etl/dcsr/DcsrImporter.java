package nl.naturalis.nba.etl.dcsr;

import static nl.naturalis.nba.api.model.SourceSystem.DCSR;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.dao.DocumentType.TAXON;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_SUPPRESS_ERRORS;
import static nl.naturalis.nba.etl.ETLUtil.getLogger;
import static nl.naturalis.nba.etl.dcsr.DcsrImportUtil.backupJsonFile;
import static nl.naturalis.nba.etl.dcsr.DcsrImportUtil.backupJsonFiles;
import static nl.naturalis.nba.etl.dcsr.DcsrImportUtil.getJsonFiles;
import static nl.naturalis.nba.etl.dcsr.DcsrImportUtil.removeBackupExtension;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.report.Issue;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Export;
import nl.naturalis.nba.etl.report.Report.Status;
import nl.naturalis.nba.etl.report.Sample;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;

/**
 * Driver class for the import of DCSR taxa and multimedia.
 *
 * @author Tom Gilissen
 */
public class DcsrImporter {

  private static final Logger logger = getLogger(DcsrImporter.class);

  private final boolean suppressErrors;

  /**
   * Main method.
   */
  public static void main(String[] args) {
    try {
      if (args.length == 0) {
        String batchJobId = UUID.randomUUID().toString();
        logger.info("No job id was provided. Starting etl job with id: {}", batchJobId);
        new DcsrImporter().importJob(batchJobId);
      }
      else if (args.length == 1 && args[0].equalsIgnoreCase("--reset")) {
        logger.info("Resetting the import files.");
        new DcsrImporter().reset();
      }
      else if (args.length == 1 && args[0].equalsIgnoreCase("--backup")) {
        logger.info("Backing up the import files.");
        new DcsrImporter().backup();
      }
      else if (args.length == 2 && args[0].equalsIgnoreCase("--jobid")) {
        String batchJobId = args[1];
        logger.info("Starting etl job with id: {}", batchJobId);
        new DcsrImporter().importJob(batchJobId);
      }
      else if (args.length == 4 && args[0].equalsIgnoreCase("--jobid") && args[2].equalsIgnoreCase("--sourceFiles")) {
        String batchJobId = args[1];
        String sourceDir = args[3];
        if (sourceDir != null && !sourceDir.isEmpty()) {
          System.setProperty("ETL_DATA_FILES_DIR", sourceDir);
        }
        logger.info("Starting etl job with id: {}", batchJobId);
        new DcsrImporter().importJob(batchJobId);
      }
      else {
        logger.info("Failed to start ETL job: missing job id!");
        System.exit(1);
      }
    } catch (Throwable t) {
      logger.error("DcsrImport terminated unexpectedly!", t);
      System.exit(1);
    }
  }

  /**
   * Initialises the NsrImporter.
   */
  public DcsrImporter() {
    suppressErrors = ConfigObject.isEnabled(SYSPROP_SUPPRESS_ERRORS);
  }

  /**
   * Import a job. This requires an jobId.
   *
   * @param jobId id of the job
   */
  private void importJob(String jobId) {

    Report taxonReport = new Report(jobId, DCSR, TAXON);
    Report multimediaReport = new Report(jobId, DCSR, MULTI_MEDIA_OBJECT);
    taxonReport.setStatus(Status.IN_PROGRESS);
    multimediaReport.setStatus(Status.IN_PROGRESS);

    long start = System.currentTimeMillis();
    taxonReport.setExport(Export.FILE);
    multimediaReport.setExport(Export.FILE);

    File[] files = getJsonFiles();
    Arrays.sort(files);
    if (files.length == 0) {
      logger.info("No source files to process");
      taxonReport.setFinished();
      multimediaReport.setFinished();
      taxonReport.save();
      multimediaReport.save();
      return;
    }

    ETLStatistics taxonStats = new ETLStatistics();
    ETLStatistics mediaStats = new ETLStatistics();
    mediaStats.setOneToMany(true);

    DcsrTaxonTransformer taxonTransformer = new DcsrTaxonTransformer(taxonReport, taxonStats);
    taxonTransformer.setSuppressErrors(suppressErrors);
    DcsrMultiMediaTransformer multimediaTransformer = new DcsrMultiMediaTransformer(multimediaReport, mediaStats);
    multimediaTransformer.setSuppressErrors(suppressErrors);

    DocumentObjectWriter<Taxon> taxonLoader = null;
    DocumentObjectWriter<MultiMediaObject> mediaLoader = null;
    try {
      for (File f : files) {
        logger.info("Processing file {}", f.getAbsolutePath());
        taxonLoader = new DcsrTaxonJsonNDWriter(f.getName(), taxonReport, taxonStats);
        mediaLoader = new DcsrMultiMediaJsonNDWriter(f.getName(), multimediaReport, mediaStats);
        taxonReport.addSourceFile(f);
        multimediaReport.addSourceFile(f);
        LineNumberReader lnr;
        try {
          FileReader fr = new FileReader(f);
          lnr = new LineNumberReader(fr, 4096);
          String json;
          while ((json = lnr.readLine()) != null) {
            List<Taxon> taxa = taxonTransformer.transform(json);
            taxonLoader.write(taxa);
            taxonReport.save();
            multimediaTransformer.setTaxon(taxa == null ? null : taxa.get(0));
            List<MultiMediaObject> multimedia = multimediaTransformer.transform(json);
            mediaLoader.write(multimedia);
            multimediaReport.save();
          }
        } catch (FileNotFoundException e) {
          logger.error("Missing file {}. "
              + "File has been skipped from importing.", f.getAbsolutePath());
        }

        // Summary after file has finished
        if (taxonStats.recordsProcessed != 0) {
          logger.info("Records processed: {}", taxonStats.recordsProcessed);
          logger.info("Taxon documents indexed: {}", taxonStats.documentsIndexed);
          logger.info("Multimedia documents indexed: {}", mediaStats.documentsIndexed);
        } else {
          logger.info("No record was processed");
        }

        try {
          taxonLoader.close();
          mediaLoader.close();
        } catch (IOException e) {
          logger.warn("Failed to close file. There may have been documents lost.");
        }
        taxonReport.save();
        multimediaReport.save();
        backupJsonFile(f);
      }
      taxonReport.setFinished();
      multimediaReport.setFinished();
      // Summery after entire import has finished
      logger.info(taxonReport.save());
      logger.info(multimediaReport.save());
      if (taxonStats.recordsProcessed != 0) {
        logger.info("DCSR Import complete");
        logger.info("Records processed: {}", taxonStats.recordsProcessed);
        logger.info("Taxon documents indexed: {}", taxonStats.documentsIndexed);
        logger.info("Multimedia documents indexed: {}", mediaStats.documentsIndexed);
      } else {
        logger.info("No record was processed");
      }
    } catch (Throwable t) {
      taxonReport.setFailed();
      taxonReport.addIssue(Issue.SOFTWARE_ERROR, new Sample(t.getMessage()));
      taxonReport.save();
      multimediaReport.setFailed();
      multimediaReport.addIssue(Issue.SOFTWARE_ERROR, new Sample(t.getMessage()));
      multimediaReport.save();
    } finally {
      IOUtil.close(taxonLoader, mediaLoader);
    }
    taxonStats.logStatistics(logger, "Taxa");
    mediaStats.badInput = taxonStats.badInput;
    mediaStats.logStatistics(logger, "Multimedia");
    ETLUtil.logDuration(logger, getClass(), start);
  }

  /**
   * Backs up the json files in the DCSR data directory by appending a "&#46;imported" extension to
   * the file name.
   */
  public void backup() {
    backupJsonFiles();
  }

  /**
   * Removes the "&#46;imported" file name extension from the files in the DCSR data directory. Nice
   * for repitive testing. Not meant for production purposes.
   */
  public void reset() {
    removeBackupExtension();
  }
}
