package nl.naturalis.nba.etl.dcsr;

import static nl.naturalis.nba.dao.DocumentType.TAXON;

import java.io.IOException;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.JsonNDWriter;
import nl.naturalis.nba.etl.report.Report;

/**
 * The JsonNDWriter component in the ETL cycle for DCSR taxon documents.
 */
public class DcsrTaxonJsonNDWriter extends JsonNDWriter<Taxon> {

  public DcsrTaxonJsonNDWriter(String sourceFile, Report report, ETLStatistics stats) throws IOException {
    super(TAXON, "Dcsr", sourceFile, report, stats);
  }
}
