package nl.naturalis.nba.etl.nsr;

import static nl.naturalis.nba.api.model.SourceSystem.NSR;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.dao.DocumentType.TAXON;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_OUTPUT;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_LOADER_QUEUE_SIZE;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_SUPPRESS_ERRORS;
import static nl.naturalis.nba.etl.ETLUtil.getLogger;
import static nl.naturalis.nba.etl.ETLUtil.logDuration;
import static nl.naturalis.nba.etl.nsr.NsrImportUtil.backupJsonFile;
import static nl.naturalis.nba.etl.nsr.NsrImportUtil.backupJsonFiles;
import static nl.naturalis.nba.etl.nsr.NsrImportUtil.getJsonFiles;
import static nl.naturalis.nba.etl.nsr.NsrImportUtil.removeBackupExtension;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.util.es.ESUtil;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.report.Issue;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Export;
import nl.naturalis.nba.etl.report.Report.Status;
import nl.naturalis.nba.etl.report.Sample;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;

/**
 * Driver class for the import of NSR taxa and multimedia. Also allows you to back up NSR source
 * files indepedently of the import procedure.
 *
 * @author Ayco Holleman
 * @author Tom Gilissen
 */
public class NsrImporter {

  private static final Logger logger = getLogger(NsrImporter.class);
  private static final boolean shouldUpdateES =
      !DaoRegistry.getInstance().getConfiguration().get(SYSPROP_ETL_OUTPUT, "es").equals("file");

  private final int loaderQueueSize;
  private final boolean suppressErrors;
  private final boolean toFile;

  private String batchJobId;
  private Report taxonReport;
  private Report multimediaReport;

  /** Initialises the NsrImporter. */
  public NsrImporter() {
    suppressErrors = ConfigObject.isEnabled(SYSPROP_SUPPRESS_ERRORS);
    String val = System.getProperty(SYSPROP_LOADER_QUEUE_SIZE, "1000");
    loaderQueueSize = Integer.parseInt(val);
    toFile =
        DaoRegistry.getInstance().getConfiguration().get(SYSPROP_ETL_OUTPUT, "file").equals("file");
  }

  /** Main method. */
  public static void main(String[] args) {
    try {
      if (args.length == 0) {
        new NsrImporter().importAll();
      } else if (args[0].equalsIgnoreCase("taxa")) {
        new NsrImporter().importTaxa();
      } else if (args[0].equalsIgnoreCase("multimedia")) {
        new NsrImporter().importMultiMedia();
      } else if (args[0].equalsIgnoreCase("backup")) {
        new NsrImporter().backup();
      } else if (args[0].equalsIgnoreCase("reset")) {
        new NsrImporter().reset();
      } else if (args[0].equalsIgnoreCase("--jobid")) {
        String batchJobId = args[1];
        if (batchJobId == null || batchJobId.equals("")) {
          logger.error("Missing job id. Please provide a job id when using parameter --jobid");
          System.exit(1);
        }
        if (args.length == 4
            && args[2].equalsIgnoreCase("--sourceFiles")
            && args[2].equalsIgnoreCase("--sourceFiles")) {
          String sourceDir = args[3];
          if (sourceDir == null || sourceDir.isEmpty()) {
            logger.error("Missing or empty --sourceFiles parameter");
            System.exit(1);
          }
          System.setProperty("ETL_DATA_FILES_DIR", sourceDir);
        }
        logger.debug("Starting batch job with id: {}", batchJobId);
        new NsrImporter().batchImport(batchJobId);
      }
    } catch (Throwable t) {
      logger.error("NsrImport terminated unexpectedly!", t);
      System.exit(1);
    } finally {
      if (shouldUpdateES && (args.length == 0 || args[0].equalsIgnoreCase("taxa"))) {
        ESUtil.refreshIndex(TAXON);
      }
      if (shouldUpdateES && (args.length == 0 || args[0].equalsIgnoreCase("multimedia"))) {
        ESUtil.refreshIndex(MULTI_MEDIA_OBJECT);
      }
      ESClientManager.getInstance().closeClient();
    }
  }

  /**
   * Imports both taxa and multimedia, processing each source file just once, and backing it up once
   * done.
   */
  public void importAll() {

    long start = System.currentTimeMillis();
    if (batchJobId == null) {
      batchJobId = UUID.randomUUID().toString();
      taxonReport = new Report(batchJobId, NSR, TAXON);
      multimediaReport = new Report(batchJobId, NSR, MULTI_MEDIA_OBJECT);
    }
    if (toFile) {
      taxonReport.setExport(Export.FILE);
      multimediaReport.setExport(Export.FILE);
    } else {
      taxonReport.setExport(Export.ES);
      multimediaReport.setExport(Export.ES);
    }
    File[] files = getJsonFiles();
    Arrays.sort(files);
    if (files.length == 0) {
      logger.info("No source files to process");
      taxonReport.setFinished();
      multimediaReport.setFinished();
      taxonReport.save();
      multimediaReport.save();
      return;
    }
    if (shouldUpdateES) {
      try {
        ETLUtil.truncate(TAXON, NSR);
        ETLUtil.truncate(MULTI_MEDIA_OBJECT, NSR);
      } catch (DaoException e) {
        logger.error("Failed to truncate the indices: {}", e.getMessage());
        logger.error("NSR Import failed.");
        System.exit(1);
      }
    }
    ETLStatistics taxonStats = new ETLStatistics();
    ETLStatistics mediaStats = new ETLStatistics();
    mediaStats.setOneToMany(true);
    NsrTaxonTransformer taxonTransformer = new NsrTaxonTransformer(taxonReport, taxonStats);
    taxonTransformer.setSuppressErrors(suppressErrors);
    NsrMultiMediaTransformer multimediaTransformer =
        new NsrMultiMediaTransformer(multimediaReport, mediaStats);
    multimediaTransformer.setSuppressErrors(suppressErrors);

    DocumentObjectWriter<Taxon> taxonLoader = null;
    DocumentObjectWriter<MultiMediaObject> mediaLoader = null;
    try {
      for (File f : files) {
        logger.debug("Processing file " + f.getAbsolutePath());
        if (toFile) {
          logger.info("ETL Output: Writing the documents to the file system");
          taxonLoader = new NsrTaxonJsonNDWriter(f.getName(), taxonReport, taxonStats);
          mediaLoader = new NsrMultiMediaJsonNDWriter(f.getName(), multimediaReport, mediaStats);
        } else {
          logger.info("ETL Output: loading documents into the document store");
          taxonLoader = new NsrTaxonLoader(loaderQueueSize, taxonStats);
          mediaLoader = new NsrMultiMediaLoader(loaderQueueSize, mediaStats);
        }
        logger.info("Processing file {}", f.getAbsolutePath());
        taxonReport.addSourceFile(f);
        multimediaReport.addSourceFile(f);
        LineNumberReader lnr;
        try {
          FileReader fr = new FileReader(f);
          lnr = new LineNumberReader(fr, 4096);
          String json;
          while ((json = lnr.readLine()) != null) {
            List<Taxon> taxa = taxonTransformer.transform(json);
            taxonLoader.write(taxa);
            taxonReport.save();
            multimediaTransformer.setTaxon(taxa == null ? null : taxa.get(0));
            List<MultiMediaObject> multimedia = multimediaTransformer.transform(json);
            mediaLoader.write(multimedia);
            multimediaReport.save();
          }
        } catch (FileNotFoundException e) {
          logger.error(
              "Missing file {}. " + "File has been skipped from importing.", f.getAbsolutePath());
        }

        // Summary after file has finished
        if (taxonStats.recordsProcessed != 0) {
          logger.info("Records processed: {}", taxonStats.recordsProcessed);
          logger.info("Taxon documents indexed: {}", taxonStats.documentsIndexed);
          logger.info("Multimedia documents indexed: {}", mediaStats.documentsIndexed);
        } else {
          logger.info("No record was processed");
        }
        if (toFile) {
          try {
            taxonLoader.close();
            mediaLoader.close();
          } catch (IOException e) {
            logger.warn("Failed to close file. There may have been documents lost.");
          }
        } else {
          taxonLoader.flush();
          mediaLoader.flush();
        }
        taxonReport.save();
        multimediaReport.save();
        backupJsonFile(f);
      }
      taxonReport.setFinished();
      multimediaReport.setFinished();
      // Summery after entire import has finished
      logger.info(taxonReport.save());
      logger.info(multimediaReport.save());
      if (taxonStats.recordsProcessed != 0) {
        logger.info("NSR Import complete");
        logger.info("Records processed: {}", taxonStats.recordsProcessed);
        logger.info("Taxon documents indexed: {}", taxonStats.documentsIndexed);
        logger.info("Multimedia documents indexed: {}", mediaStats.documentsIndexed);
      } else {
        logger.info("No record was processed");
      }
    } catch (Throwable t) {
      taxonReport.setFailed();
      taxonReport.addIssue(Issue.SOFTWARE_ERROR, new Sample(t.getMessage()));
      taxonReport.save();
      multimediaReport.setFailed();
      multimediaReport.addIssue(Issue.SOFTWARE_ERROR, new Sample(t.getMessage()));
      multimediaReport.save();
    } finally {
      IOUtil.close(taxonLoader, mediaLoader);
    }
    taxonStats.logStatistics(logger, "Taxa");
    mediaStats.badInput = taxonStats.badInput;
    mediaStats.logStatistics(logger, "Multimedia");
    ETLUtil.logDuration(logger, getClass(), start);
  }

  /** Extracts and imports just the taxa from the source files. Does not make backups. */
  public void importTaxa() throws IOException {
    long start = System.currentTimeMillis();
    File[] files = getJsonFiles();
    Arrays.sort(files);
    if (files.length == 0) {
      logger.info("No source files to process");
      return;
    }
    if (shouldUpdateES) {
      ETLUtil.truncate(TAXON, NSR);
    }
    ETLStatistics stats = new ETLStatistics();
    NsrTaxonTransformer transformer = new NsrTaxonTransformer(taxonReport, stats);
    transformer.setSuppressErrors(suppressErrors);
    DocumentObjectWriter<Taxon> loader = null;
    if (toFile) {
      logger.info("ETL Output: Writing the documents to the file system");
    } else {
      logger.info("ETL Output: loading documents into the document store");
    }
    try {
      for (File f : files) {
        logger.info("Processing file {}", f.getAbsolutePath());
        if (toFile) {
          loader = new NsrTaxonJsonNDWriter(f.getName(), stats);
        } else {
          loader = new NsrTaxonLoader(loaderQueueSize, stats);
        }
        LineNumberReader lnr;
        try {
          FileReader fr = new FileReader(f);
          lnr = new LineNumberReader(fr, 4096);
          String json;
          while ((json = lnr.readLine()) != null) {
            List<Taxon> taxa = transformer.transform(json);
            loader.write(taxa);
          }
        } catch (FileNotFoundException e) {
          logger.error(
              "Missing file {}. File has been skipped from importing.", f.getAbsolutePath());
        }
        if (toFile) {
          try {
            loader.close();
          } catch (IOException e) {
            logger.warn("Failed to close file. There may have been documents lost.");
          }
        } else {
          loader.flush();
        }
      }
    } finally {
      IOUtil.close(loader);
    }
    stats.logStatistics(logger, "Taxa");
    ETLUtil.logDuration(logger, getClass(), start);
  }

  /** Extracts and imports just the multimedia from the source files. Does not make backups. */
  public void importMultiMedia() throws IOException {
    long start = System.currentTimeMillis();
    File[] files = getJsonFiles();
    if (files.length == 0) {
      logger.info("No source files to process");
      return;
    }
    if (shouldUpdateES) {
      ETLUtil.truncate(MULTI_MEDIA_OBJECT, NSR);
    }
    ETLStatistics stats = new ETLStatistics();
    stats.setOneToMany(true);
    NsrMultiMediaTransformer transformer = new NsrMultiMediaTransformer(multimediaReport, stats);
    transformer.setSuppressErrors(suppressErrors);
    /*
     * For multimedia we will re-use our taxon transformer class to extract
     * taxon-related data from the XML records, so we don't have to
     * duplicate that functionality in the multimedia transformer. However,
     * we are not interested in the statistics maintained by the taxon
     * transformer (only whether it was able to produce an Taxon object or
     * not). Therefore we instantiate the taxon transformer with a trash
     * statistics object.
     */
    NsrTaxonTransformer ntt = new NsrTaxonTransformer(multimediaReport, new ETLStatistics());
    ntt.setSuppressErrors(suppressErrors);
    DocumentObjectWriter<MultiMediaObject> loader = null;
    if (toFile) {
      logger.info("ETL Output: Writing the documents to the file system");
    } else {
      logger.info("ETL Output: loading documents into the document store");
    }
    try {
      for (File f : files) {
        if (toFile) {
          loader = new NsrMultiMediaJsonNDWriter(f.getName(), stats);
        } else {
          loader = new NsrMultiMediaLoader(loaderQueueSize, stats);
        }
        logger.info("Processing file {}", f.getAbsolutePath());

        LineNumberReader lnr;
        try {
          FileReader fr = new FileReader(f);
          lnr = new LineNumberReader(fr, 4096);
          String json;
          while ((json = lnr.readLine()) != null) {
            List<Taxon> taxa = ntt.transform(json);
            transformer.setTaxon(taxa == null ? null : taxa.get(0));
            List<MultiMediaObject> multimedia = transformer.transform(json);
            loader.write(multimedia);
          }
        } catch (FileNotFoundException e) {
          logger.error(
              "Missing file {}. File has been skipped from importing.", f.getAbsolutePath());
        }

        if (toFile) {
          try {
            loader.close();
          } catch (IOException e) {
            logger.warn("Failed to close file. There may have been documents lost.");
          }
        } else {
          loader.flush();
        }
      }
    } finally {
      IOUtil.close(loader);
    }
    stats.logStatistics(logger, "Multimedia");
    logDuration(logger, getClass(), start);
  }

  /**
   * Start an import as a batch job. This requires an jobId
   *
   * @param batchJobId id of the job
   */
  private void batchImport(String batchJobId) {
    this.batchJobId = batchJobId;
    taxonReport = new Report(batchJobId, NSR, TAXON);
    multimediaReport = new Report(batchJobId, NSR, MULTI_MEDIA_OBJECT);
    taxonReport.setStatus(Status.IN_PROGRESS);
    multimediaReport.setStatus(Status.IN_PROGRESS);
    importAll();
  }

  /**
   * Backs up the XML files in the NSR data directory by appending a "&#46;imported" extension to
   * the file name.
   */
  public void backup() {
    backupJsonFiles();
  }

  /**
   * Removes the "&#46;imported" file name extension from the files in the NSR data directory. Nice
   * for repitive testing. Not meant for production purposes.
   */
  public void reset() {
    removeBackupExtension();
  }
}
