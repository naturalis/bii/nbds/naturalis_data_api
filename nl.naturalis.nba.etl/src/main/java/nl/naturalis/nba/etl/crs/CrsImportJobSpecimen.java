package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.api.model.SourceSystem.CRS;
import static nl.naturalis.nba.dao.DocumentType.SPECIMEN;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_ENRICH;
import static nl.naturalis.nba.etl.crs.CrsImportUtil.getXmlFiles;

import java.io.File;
import java.io.IOException;
import java.util.List;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLRegistry;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.XMLRecordInfo;
import nl.naturalis.nba.etl.normalize.AreaClassNormalizer;
import nl.naturalis.nba.etl.normalize.PhaseOrStageNormalizer;
import nl.naturalis.nba.etl.normalize.SexNormalizer;
import nl.naturalis.nba.etl.normalize.SpecimenTypeStatusNormalizer;
import nl.naturalis.nba.etl.normalize.TaxonRelationTypeNormalizer;
import nl.naturalis.nba.etl.report.ProcessingError;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Status;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * Class that manages the import of CRS specimens.
 *
 * @author Tom Gilissen
 */
public class CrsImportJobSpecimen {

  private static final Logger logger;
  private static final boolean doEnrich;

  private final Report report;
  private final boolean suppressErrors;

  private ETLStatistics stats;
  private CrsSpecimenTransformer transformer;
  private DocumentObjectWriter<Specimen> loader;

    static {
      logger = ETLRegistry.getInstance().getLogger(CrsImportJobSpecimen.class);
      doEnrich = DaoRegistry.getInstance().getConfiguration().get(SYSPROP_ETL_ENRICH, "false").equals("true");
    }

  /**
   * Import specimens from the data directory configured in nba-import.properties.
   */
  public CrsImportJobSpecimen(String jobId) {
      suppressErrors = ConfigObject.isEnabled("suppressErrors");
      this.report = new Report(jobId, CRS, SPECIMEN);
      logger.info("ETL Specimen report: {}", report.getReportName());
    }

    public void importSpecimens() {
      long start = System.currentTimeMillis();
      report.setStatus(Status.IN_PROGRESS);
      report.setThemesVersion(ThemeCache.getInstance().getThemesVersion());
      stats = new ETLStatistics();

      File[] xmlFiles = getXmlFiles(SPECIMEN);
      if (xmlFiles == null || xmlFiles.length == 0) {
        report.setStatus(Status.FAILED);
        logger.error("No specimen oai.xml files found. Check nba.properties");
        return;
      }

      transformer = new CrsSpecimenTransformer(report, stats);
      transformer.setSuppressErrors(suppressErrors);
      if (doEnrich) {
        transformer.setEnrich(true);
        report.doEnrich();
        logger.info("Taxonomic enrichment of Specimen documents: true");
      }

      SexNormalizer.getInstance().resetStatistics();
      SpecimenTypeStatusNormalizer.getInstance().resetStatistics();
      PhaseOrStageNormalizer.getInstance().resetStatistics();
      TaxonRelationTypeNormalizer.getInstance().resetStatistics();
      AreaClassNormalizer.getInstance().resetStatistics();
      ThemeCache.getInstance().resetMatchCounters();

      try {
        for (File f : xmlFiles) {
          importFile(f);
          report.save();
        }
      } finally {
        logger.info("Finished importing {} files", xmlFiles.length);
        report.setFinished();
      }
      report.addThemesSummary(ThemeCache.getInstance().getThemesSummary());
      logger.info(report.save());

      SexNormalizer.getInstance().logStatistics();
      SpecimenTypeStatusNormalizer.getInstance().logStatistics();
      PhaseOrStageNormalizer.getInstance().logStatistics();
      TaxonRelationTypeNormalizer.getInstance().logStatistics();
      AreaClassNormalizer.getInstance().logStatistics();
      ThemeCache.getInstance().logMatchInfo();
      stats.logStatistics(logger);
      ETLUtil.logDuration(logger, getClass(), start);
    }

    private void importFile(File f) {
      logger.info("Processing file " + f.getName());
      report.addSourceFile(f);
      try {
        loader = new CrsSpecimenJsonNDWriter(f.getName(), report, stats);
      } catch (IOException e) {
        report.addProcessingError(ProcessingError.IO_ERROR, f);
        logger.error("Failed to create export file for source file {}: {}", f.getName(), e.getMessage());
      }

      CrsExtractor extractor;
      try {
        extractor = new CrsExtractor(f, stats);
      } catch (SAXException e) {
        report.addProcessingError(ProcessingError.PARSING_ERROR, f);
        logger.error("Processing failed!");
        logger.error(e.getMessage());
        return;
      }
      for (XMLRecordInfo extracted : extractor) {
        List<Specimen> transformed = transformer.transform(extracted);
        loader.write(transformed);
        if (stats.recordsProcessed != 0 && stats.recordsProcessed % 50000 == 0) {
          logger.info("Records processed: {}", stats.recordsProcessed);
          logger.info("Documents indexed: {}", stats.documentsIndexed);
        }
      }
      IOUtil.close(loader);
    }

  }
