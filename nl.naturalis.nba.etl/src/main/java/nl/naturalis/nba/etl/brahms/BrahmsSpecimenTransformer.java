package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.api.model.SourceSystem.BRAHMS;
import static nl.naturalis.nba.dao.DocumentType.SPECIMEN;
import static nl.naturalis.nba.dao.util.es.ESUtil.getElasticsearchId;
import static nl.naturalis.nba.etl.ETLConstants.BRAHMS_ABCD_COLLECTION_TYPE;
import static nl.naturalis.nba.etl.ETLConstants.BRAHMS_ABCD_SOURCE_ID;
import static nl.naturalis.nba.etl.ETLConstants.LICENCE;
import static nl.naturalis.nba.etl.ETLConstants.LICENCE_TYPE;
import static nl.naturalis.nba.etl.ETLConstants.SOURCE_INSTITUTION_ID;
import static nl.naturalis.nba.etl.ETLUtil.getSpecimenPurl;
import static nl.naturalis.nba.etl.ETLUtil.hasValue;
import static nl.naturalis.nba.etl.MimeTypeCache.MEDIALIB_HTTPS_URL;
import static nl.naturalis.nba.etl.MimeTypeCache.MEDIALIB_HTTP_URL;
import static nl.naturalis.nba.etl.TransformUtil.guessMimeType;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.additionalcollectors;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.collectioneventid;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.collectors;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.descriptiontext;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.determinationday;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.determinationmonth;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.determinationyear;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.determinedby;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.fieldnumber;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.imagelist;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.oldbarcode;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.prefix;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.specimenaccession;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.specimencategory;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.suffix;
import static nl.naturalis.nba.etl.brahms.BrahmsImportAll.specimenObjectIdsCache;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getDefaultClassification;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getScientificName;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getTaxonRank;
import static nl.naturalis.nba.etl.enrich.EnrichmentUtil.createEnrichments;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nl.naturalis.nba.api.model.Agent;
import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.GatheringEvent;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.api.model.SpecimenIdentification;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.api.model.TaxonomicEnrichment;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.MedialibIdsCache;
import nl.naturalis.nba.etl.MimeTypeCache;
import nl.naturalis.nba.etl.MimeTypeCacheFactory;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.Transformer;
import nl.naturalis.nba.etl.enrich.EnrichmentUtil;
import nl.naturalis.nba.etl.report.Issue;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Sample;
import org.yaml.snakeyaml.util.UriEncoder;

/**
 * The transformer component in the Brahms ETL cycle for specimens.
 *
 * @author Ayco Holleman
 * @author Tom Gilissen
 */
class BrahmsSpecimenTransformer extends BrahmsTransformer<Specimen> {

  private static final ThemeCache themeCache;
  private static final String DEFAULT_IMAGE_QUALITY = "ac:GoodQuality";


  static {
    themeCache = ThemeCache.getInstance();
    MedialibIdsCache.getInstance();
  }

  private final MimeTypeCache mimetypeCache;
  private boolean enrich = false;

  BrahmsSpecimenTransformer(Report report, ETLStatistics stats) {
    super(report, stats);
    mimetypeCache = MimeTypeCacheFactory.getInstance().getCache();
  }

  private static void setConstants(Specimen specimen) {
    specimen.setSourceSystem(BRAHMS);
    specimen.setSourceInstitutionID(SOURCE_INSTITUTION_ID);
    specimen.setOwner(SOURCE_INSTITUTION_ID);
    specimen.setSourceID(BRAHMS_ABCD_SOURCE_ID);
    specimen.setLicenseType(LICENCE_TYPE);
    specimen.setLicense(LICENCE);
    specimen.setCollectionType(BRAHMS_ABCD_COLLECTION_TYPE);
  }

  void setEnrich() {
    this.enrich = true;
  }

  boolean doEnrich() {
    return enrich;
  }

  /**
   * This class provides a final implementation of the method defined by the {@link Transformer}
   * interface. At the same time, it provides a template for subclassess to do the heavy-lifting
   * through three template methods:
   *
   * <ol>
   *   <li>{@link #skipRecord()} to determine if the record should be skipped
   *   <li>{@link #getObjectID()} to extract the object ID from the currently processed record
   *       (needed for logging purposes)
   *   <li>{@link #doTransform()} to validate the input and produce the output
   * </ol>
   */
  @Override
  public final List<Specimen> transform(CSVRecordInfo<BrahmsCsvField> input) {
    this.input = input;
    report.recordsIngested++;
    stats.recordsProcessed++;
    if (skipRecord()) {
      report.addIssue(
          Issue.SKIPPED, new Sample(input.getLine(), null, null, input.getLineNumber()));
      report.recordsSkipped++;
      stats.recordsSkipped++;
      return Collections.emptyList();
    }
    objectID = getObjectID();
    if (objectID == null) {
      report.addIssue(Issue.MISSING_ID, new Sample(input.getLine()));
      report.recordsRejected++;
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record rejected: Missing object ID");
      }
      return Collections.emptyList();
    }
    return doTransform();
  }

  @Override
  protected List<Specimen> doTransform() {
    // No record-level validations, so:
    stats.recordsAccepted++;
    stats.objectsProcessed++;

    try {
      Specimen specimen = new Specimen();
      specimen.setId(getElasticsearchId(BRAHMS, objectID));
      specimen.setSourceSystemId(objectID);
      specimen.setUnitID(objectID);
      specimen.setUnitGUID(getSpecimenPurl(objectID));
      setConstants(specimen);
      List<String> themes = themeCache.lookup(objectID, SPECIMEN, BRAHMS);
      specimen.setTheme(themes);
      String category = input.get(specimencategory);
      specimen.setRecordBasis(Objects.requireNonNullElse(category, "Preserved Specimen"));
      specimen.setAssemblageID(input.get(collectioneventid));
      String notes = input.get(descriptiontext, true);
      if (notes != null) {
        specimen.setNotes(notes.replace("\u00001", ""));
      }
      specimen.setPreviousUnitsText(getPreviousUnitsText());
      specimen.setObjectPublic(true);

      specimen.setCollectorsFieldNumber(getCollectorsFieldNumber());
      specimen.setGatheringEvent(getGatheringEvent(input));
      specimen.addIdentification(getSpecimenIdentification(input));
      if (doEnrich()) {
        enrichIdentification(specimen);
      }
      specimen.setAssociatedMultiMediaUris(getServiceAccessPoints());
      stats.objectsAccepted++;
      report.recordsProcessed++;
      specimenObjectIdsCache.add(objectID);
      return Collections.singletonList(specimen);
    } catch (Throwable t) {
      report.addIssue(
          Issue.SOFTWARE_ERROR,
          new Sample(input.getLine(), t.getMessage(), null, input.getLineNumber()));
      stats.objectsRejected++;
      if (!suppressErrors) {
        error(t.getMessage());
        error(input.getLine());
      }
      return Collections.emptyList();
    }
  }

  /*
   * Temporary (?) modification to allow for enrichment during the specimen import
   *
   * Retrieve taxonomic data from CoL and NSR and add it to the identification(s)
   */
  private void enrichIdentification(Specimen specimen) {

    // A specimen can have one or more identifications
    // We need to check all identifications

    for (SpecimenIdentification identification : specimen.getIdentifications()) {
      List<Taxon> taxa = EnrichmentUtil.findEnrichingTaxa(identification);
      List<TaxonomicEnrichment> enrichment = createEnrichments(taxa);
      if (enrichment != null) {
        identification.setTaxonomicEnrichments(enrichment);
      }
    }
  }

  private GatheringEvent getGatheringEvent(CSVRecordInfo<BrahmsCsvField> csvRecord) {
    GatheringEvent ge = new GatheringEvent();
    populateGatheringEvent(ge, csvRecord);
    return ge;
  }

  private SpecimenIdentification getSpecimenIdentification(
      CSVRecordInfo<BrahmsCsvField> csvRecord) {
    SpecimenIdentification identification = new SpecimenIdentification();
    identification.setTypeStatus(getTypeStatus());
    String s = csvRecord.get(determinedby);
    if (s != null) {
      identification.addIdentifier(new Agent(s));
    }
    String y = csvRecord.get(determinationyear);
    String m = csvRecord.get(determinationmonth);
    String d = csvRecord.get(determinationday);
    identification.setDateIdentified(getDate(y, m, d));
    ScientificName sn = getScientificName(csvRecord);
    identification.setScientificName(sn);
    DefaultClassification dc = getDefaultClassification(csvRecord);
    identification.setDefaultClassification(dc);
    identification.setTaxonRank(getTaxonRank(csvRecord));
    return identification;
  }

  private String getCollectorsFieldNumber() {
    CSVRecordInfo<BrahmsCsvField> rec = input;
    ArrayList<String> arrayOfStrings = new ArrayList<>();
    String collectorsStr = rec.get(collectors, true);
    if (collectorsStr != null) {
      arrayOfStrings.add(collectorsStr.trim());
    }
    String additionalCollectorsStr = rec.get(additionalcollectors, true);
    if (additionalCollectorsStr != null) {
      arrayOfStrings.add("; ");
      arrayOfStrings.add(additionalCollectorsStr.trim());
    }
    String prefixStr = rec.get(prefix, true);
    if (prefixStr != null) {
      arrayOfStrings.add(prefixStr.trim());
    }
    String fieldNumberStr = rec.get(fieldnumber, true);
    if (fieldNumberStr != null) {
      arrayOfStrings.add(fieldNumberStr);
    }
    String suffixStr = rec.get(suffix, true);
    if (suffixStr != null) {
      arrayOfStrings.add(suffixStr);
    }
    String str = String.join(" ", arrayOfStrings).trim();
    // Remove superfluous white spaces
    str = str.replaceAll("\\s;", ";");
    return str.replaceAll("\\s+", " ");
  }

  /*
   * Returns the value from OLDBARCODE concatenated with the value of
   * ACCESSION separated by ' | '. Empty values or null are excluded.
   */
  private String getPreviousUnitsText() {
    CSVRecordInfo<BrahmsCsvField> rec = input;
    String oldBarcode = rec.get(oldbarcode, false);
    String accession = rec.get(specimenaccession, false);
    ArrayList<String> arrayOfStrings = new ArrayList<>();
    if (hasValue(oldBarcode)) {
      arrayOfStrings.add(oldBarcode.trim());
    }
    if (hasValue(accession)) {
      arrayOfStrings.add(accession.trim());
    }
    if (arrayOfStrings.size() == 1) {
      return arrayOfStrings.get(0);
    }
    return String.join(" | ", arrayOfStrings);
  }

  private List<ServiceAccessPoint> getServiceAccessPoints() {
    String images = input.get(imagelist);
    if (images == null) {
      return Collections.emptyList();
    }
    String[] urls = images.split(",");
    return calculateAccessPoints(urls);
  }

  /**
   * Convert an array of Strings to a List of ServiceAccessPoints.
   *
   * @param urls an array of urls as string
   * @return list of ServiceAccessPoints
   */
  private List<ServiceAccessPoint> calculateAccessPoints(String[] urls) {

    List<ServiceAccessPoint> saps = new ArrayList<>(urls.length);
    for (String urlStr : urls) {

      // Convert to URI
      URI uri = convertToURI(urlStr);
      if (uri == null) {
        continue;
      }
      // Guess the mimetype cache from the URI
      String mimeType = guessMimeType(uri.toString());
      ServiceAccessPoint sap = new ServiceAccessPoint(uri, mimeType, DEFAULT_IMAGE_QUALITY);
      if (inMedialibrary(sap)) {
        saps.add(sap);
      }
    }
    return saps.isEmpty() ? null : saps;
  }

  /**
   * Check wheter or not this accesspoint is available from the medialibrary.
   * This check is done not against the live medialibrary, but a cache from
   * it.
   *
   * @param sap the service accesspoint to check
   * @return true when in medialibrary; false otherwise
   */
  private boolean inMedialibrary(ServiceAccessPoint sap) {
    String mediaObjectId;
    URI uri = sap.getAccessUri();
    Pattern pattern = Pattern.compile("^.*id/(.*)/format/.*$");
    Matcher matcher = pattern.matcher(uri.toString());
    if (matcher.matches()) {
      mediaObjectId = matcher.group(1);
      if (!MedialibIdsCache.contains(mediaObjectId)) {
        report.addIssue(Issue.URL_NOT_IN_MEDIALIB_CACHE, new Sample(input.getLine(), uri.toString()));
        if (!suppressErrors) {
          warn("Not an existing medialib URL: %s", uri);
        }
        return false;
      }
      else {
        // Try to update the mimetype by doing a lookup in the mimetype cache
        String mimeType = mimetypeCache.getMimeType(mediaObjectId);
        if (mimeType != null) {
          sap.setFormat(mimeType);
        }
      }
    }
  return true;
  }

  /**
   * Convert a String to a URI
   *
   * @param str URI as String
   * @return URI or null when conversion fails
   */
  private URI convertToURI(String str) {
    if (str == null) {
      return null;
    }
    String urlStr = str.strip();
    // Change http urls to https urls, but leave the rest as they are
    if (str.startsWith(MEDIALIB_HTTP_URL) && !str.startsWith(MEDIALIB_HTTPS_URL)) {
      urlStr = urlStr.replace(MEDIALIB_HTTP_URL, MEDIALIB_HTTPS_URL);
    }
    try {
      return new URI(UriEncoder.encode(urlStr));
    } catch (URISyntaxException e) {
      report.addIssue(Issue.INVALID_IMAGE_URL, new Sample(input.getLine(), str));
      if (!suppressErrors) {
        warn("Invalid multimedia URL: %s", str);
      }
      return null;
    }
  }

}
