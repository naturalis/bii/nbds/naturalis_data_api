package nl.naturalis.nba.etl;

import static nl.naturalis.nba.etl.ETLUtil.getLogger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.UUID;
import java.util.function.Supplier;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.common.json.ObjectMapperLocator;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.utils.FileUtil;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("CheckStyle")
public abstract class JsonNDWriter<T extends IDocumentObject> implements DocumentObjectWriter<T> {

  private static final Logger logger = getLogger(JsonNDWriter.class);
  private static final byte[] NEW_LINE = "\n".getBytes();
  @SuppressWarnings("unused")
  private final String sourceFile;
  private final Report report;
  private final ETLStatistics stats;
  private final String sourceSystem;
  private final DocumentType<T> documentType;
  private File file;
  private FileOutputStream fos = null;
  private BufferedOutputStream bos = null;
  private ObjectWriter writer;
  private boolean suppressErrors;

  /**
   * Creates a JsonND (newline delimited JSON) writer for the specified document type. The writer
   * will write all documents to an export file.
   *
   * @param dt           : the documentType of the set
   * @param sourceSystem : the source system
   * @param sourceFile   : the source file
   * @param stats        : the ETLStatistics object
   */
  protected JsonNDWriter(
      DocumentType<T> dt,
      String sourceSystem,
      String sourceFile,
      ETLStatistics stats) throws IOException {
    this.documentType = dt;
    this.sourceSystem = sourceSystem.toLowerCase();
    this.sourceFile = sourceFile;
    this.report = null;
    this.stats = stats;
    createExportFile();
    openFile();
    createWriter();
    logger.info("Writing documents to: {}", file.getAbsolutePath());
  }

  protected JsonNDWriter(
      DocumentType<T> dt,
      String sourceSystem,
      String sourceFile,
      Report report,
      ETLStatistics stats) throws IOException {
    this.documentType = dt;
    this.sourceSystem = sourceSystem.toLowerCase();
    this.sourceFile = sourceFile;
    this.report = report;
    this.stats = stats;
    createExportFile();
    openFile();
    createWriter();
    logger.info("Writing documents to: {}", file.getAbsolutePath());
  }

  private void openFile() throws FileNotFoundException {
    fos = new FileOutputStream(file, true);
    bos = new BufferedOutputStream(fos, 4096);
  }

  private void closeFile() throws IOException {
    if (bos != null) {
      bos.close();
    }
    if (fos != null) {
      fos.close();
    }
  }

  private void createWriter() {
    ObjectMapper mapper =
        ObjectMapperLocator.getInstance().getObjectMapper(documentType.getJavaType());
    this.writer = mapper.writer();
  }

  @Override
  public final void write(Collection<T> objects) {
    if (objects == null || objects.isEmpty()) {
      return;
    }
    for (T object : objects) {
      if (object != null) {
        try {
          // writer.writeValue(bos, object); // Closes the outputstream, therefore:
          bos.write(writer.writeValueAsBytes(object));
          bos.write(NEW_LINE);
          report.documentsCreated++;
          stats.documentsIndexed++;
        } catch (IOException e) {
          if (!suppressErrors) {
            logger.error(e.getMessage());
          }
        }
      }
    }
  }

  @Override
  public void close() {
    try {
      closeFile();
    } catch (IOException e) {
      logger.warn("Tried to close file, but file couldn't be closed / file was not available: {}",
          e.getMessage());
    }
  }

  @Override
  public void flush() {
    close();
  }

  /**
   * Determines whether to suppress ERROR and WARN messages while still letting through INFO
   * messages. This is sometimes helpful if you expect large amounts of well-known errors and
   * warnings that just clog up your log file.
   *
   * @param suppressErrors :
   */
  public void suppressErrors(boolean suppressErrors) {
    this.suppressErrors = suppressErrors;
  }

  private void createExportFile() throws IOException {
    boolean isReport = (report != null && report.getJobId() != null);
    File exportDir = new File(getExportDir(documentType));
    logger.debug("Export directory: {}", exportDir.getAbsolutePath());
    if (!exportDir.isDirectory() && !exportDir.mkdirs()) {
      String msg = "Failed to create export directory: " + exportDir.getAbsolutePath();
      logger.error(msg);
      throw new IOException(msg);
    }
    StringBuilder name = new StringBuilder(100);
    name.append(sourceSystem).append(".");
    name.append(documentType.getName().toLowerCase());
    name.append(".export.");
    // Use jobId or create a random one if there isn't one
    if (isReport) {
      name.append(report.getJobId()).append(".");
    } else {
      name.append(UUID.randomUUID()).append(".");
    }
    Supplier<String> exportDateTime = () -> LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    name.append(exportDateTime.get());
    name.append(".ndjson");
    file = FileUtil.newFile(exportDir, name.toString());
    if (isReport) {
      report.setExportFile(file.getName());
    }
    logger.debug("Export file: {}", file.getAbsolutePath());
  }

  private String getExportDir(DocumentType<?> documentType) {
    File dir = DaoRegistry.getInstance().getConfiguration().getDirectory("nba.etl.install.dir");
    String exportDir = dir.getAbsolutePath() + "/export/";
    if (report != null) {
      exportDir = exportDir + report.getJobId() + "/";
    }
    if ("multimediaobject".equalsIgnoreCase(documentType.getName())) {
      return exportDir.concat("multimedia");
    } else {
      return exportDir.concat(documentType.getName().toLowerCase());
    }
  }
}
