package nl.naturalis.nba.etl.report;

import com.google.common.base.Preconditions;

/**
 * Enum representing issues that may occuring during the ETL process.
 *
 * <p>
 * An issue has one the following levels:
 * - info
 * - warning
 * - error
 * </p>
 * and includes a description.
 *
 * @author Tom Gilissen
 *
 */
public enum Issue {

  // INFO
  PARENT_RECORD_FAILED(
      Level.INFO,
      "Parent record (specimen or taxon) has failed. Record has been skipped."
  ),

  SKIPPED(
      Level.INFO,
      "Record has been configured to be skipped."),

  STATUS_DELETED(
      Level.INFO,
      "Record has status \"deleted\". Record has been skipped."
  ),

  MISSING_ABCD_RECORDBASIS(
      Level.INFO,
      "Missing or empty element <abcd:RecordBasis>. Record has been skipped."
  ),

  // WARNINGS
  EMPTY_RANK(
      Level.WARNING,
      "Empty <rank> element in classification. Monomial has been omitted from the document."),

  EMPTY_NAME(
      Level.WARNING,
      "Empty <name> element in classification. Monomial has been omitted from the document."),

  INVALID_COORDINATE(
      Level.WARNING,
      "Coordinate is incomplete (either latitude or longitude is missing)."
  ),

  LAT_LON_INVALID(
      Level.WARNING,
  "Latitude and/or longitude is invalid. Value is ignored."
  ),

  INVALID_DATE(
      Level.WARNING,
      "Invalid date value. Value has been omitted from the document."
  ),

  INVALID_DATESTAMP(
      Level.WARNING,
      "Invalid date in element <datestamp>."
  ),

  INVALID_NUMBER_IN_ELEMENT(
      Level.WARNING,
      "Invalid number in element."
  ),

  INVALID_REFERENCE_DATE(
      Level.WARNING,
      "Invalid value for <reference_date>. Value has been omitted from the document."
  ),

  INVALID_URI(
      Level.WARNING,
      "Invalid URI. Data has been omitted from the document."
  ),

  MISSING_CLASSIFICATION(
      Level.WARNING,
      "Record has no classification for taxon. "
          + "Classification data has been omitted from the document."),

  MISSING_CONTENT(
      Level.WARNING,
      "Non-essential content is missing from the record"
  ),

  MISSING_MIME_TYPE(
      Level.WARNING,
      "Image data has no information on mime type. The mime type will be guessed."
  ),

  MISSING_SCIENTIFIC_NAME(
      Level.WARNING,
      "No scientific name given in identification"
  ),

  MISSING_SCIENTIFIC_AND_VERNACULAR_NAME(
      Level.WARNING,
      "No scientific name or vernacular name given in identification"
  ),

  MISSING_ABCD_FILE_URI(
      Level.WARNING,
      "Missing or empty element <abcd:fileuri>"
  ),

  MISSING_ABCD_TYPE_STATUS(
      Level.WARNING,
      "Missing or empty element <abcd:TypeStatus>"
  ),

  URL_NOT_IN_MEDIALIB_CACHE(
      Level.WARNING,
      "URL not in medialib cache. URL has been omitted from the document."
  ),

  NON_MEDIALIB_URL(
      Level.WARNING,
      "URL is not a medialibrary url"
  ),

  // ERRORS - Record level
  DATA_INCONSISTENCY(
      Level.ERROR,
      "Data inconsistency. Record had been rejected."
  ),

  IMAGE_ID_NOT_IN_MEDIA_LIBRARY(
      Level.ERROR,
      "The image Id was not found in the media library. "
          + "No multimedia document has been created."),

  INCOMPLETE_MULTIMEDIA_RECORD(
      Level.ERROR,
      "Record has no medialib url. It has been rejected."),

  INVALID_IMAGE_URL(
      Level.ERROR,
      ""
  ),

  INVALID_MEDIALIB_URI(
      Level.ERROR,
      "Medialib URI has invalid syntax and/or format. No multimedia document has been created."),

  INVALID_SPECIMEN_IDENTIFICATION(
    Level.ERROR,
    "Invalid or insufficient specimen identification information"
  ),

  INVALID_RANK(
      Level.ERROR,
      "The taxonomic rank of the record is too generic. Record has been rejected."),

  INVALID_TAXON_STATUS(
      Level.ERROR,
      "The taxonomic status of the record is invalid. Record has been rejected."),

  MEDIALIB_URL_FAILURE(
      Level.ERROR,
      "Could not (re)create medialib URI from provided URI. "
          + "No multimedia document has been created."),

  MISSING_ABCD_COLLECTIONTYPE(
      Level.ERROR,
      "Missing or empty element <abcd:CollectionType>."
  ),

  MISSING_ACCEPTED_NAME(
      Level.ERROR,
      "The accepted name is missing (no <name> elements under <names>). Record has been rejected."),

  MISSING_NCRS_DETERMINATION(
      Level.ERROR,
      "Missing or empty element <ncrsDetermination>."
      ),

  MISSING_ID(
      Level.ERROR,
      "Record has no id. It has been rejected."),

  MISSING_IMAGE_DATA(
      Level.ERROR,
      "Record has no image data. It has been rejected."),

  MISSING_IMAGE_URI_DATA(
      Level.ERROR,
      "The URI is missing from the <image> element. No multimedia document has been created."),

  MISSING_FRM_DIGITALE_BESTANDEN(
      Level.ERROR,
      "Missing or empty element <frmDigitalebestanden>."
  ),

  MISSING_MEDIALIB_URI(
      Level.ERROR,
      "Record has no medialib URI. It has been rejected."),

  MISSING_NAME_TYPE(
      Level.ERROR,
      "The <nametype> element is missing from the <names> element. Record has been rejected."),

  MISSING_NAMES_ELEMENT(
      Level.ERROR,
      "The <names> element is missing. Record has been rejected."),

  MISSING_OAI_DC_ELEMENT(
      Level.ERROR,
      "Record has no content. Element <oai_dc:dc> is missing."
  ),

  MISSING_PARENT_ID(
      Level.ERROR,
      "Record has no parent id. It has been rejected."),

  MISSING_RANK(
      Level.ERROR,
      "The record has no data on taxonomic rank. It has been rejected."),

  MISSING_RECORD_URI(
      Level.ERROR,
      "Record has no record URI (url). It has been rejected."),

  MULTIPLE_ACCEPTED_NAMES(
      Level.ERROR,
      "More than one accepted name was provided (maximum is one). Record has been rejected."),

  ORPHAN_RECORD(
      Level.ERROR,
      "No matching specimen document found. No multimedia document has been created."
  ),

  // ERRORS - General
  CSV_ERROR(
      Level.ERROR,
      "Parsing of the csv source file failed."),

  JSON_ERROR(
      Level.ERROR,
      "Parsing of the json record file failed."),

  SOFTWARE_ERROR(
      Level.ERROR,
      "A software error occurred while the record was being processed. Record has been skipped.");
  


  public final Level level;
  public final String description;

  Issue(Level level, String description) {
    this.level = Preconditions.checkNotNull(level);
    this.description = Preconditions.checkNotNull(description);
  }

  /**
   * Level of importance.
   */
  public enum Level {
    INFO,
    WARNING,
    ERROR
  }
}
