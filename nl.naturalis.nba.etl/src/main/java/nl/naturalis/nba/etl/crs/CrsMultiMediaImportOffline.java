package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.api.model.SourceSystem.CRS;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_ENRICH;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_OUTPUT;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.util.es.ESUtil;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLConstants;
import nl.naturalis.nba.etl.ETLRegistry;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.MimeTypeCacheFactory;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.XMLRecordInfo;
import nl.naturalis.nba.etl.normalize.PhaseOrStageNormalizer;
import nl.naturalis.nba.etl.normalize.SexNormalizer;
import nl.naturalis.nba.etl.normalize.SpecimenTypeStatusNormalizer;
import nl.naturalis.nba.etl.report.ProcessingError;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Status;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * Class that manages the import of CRS multimedia, sourced from files on the
 * local file system. These files have most likely been put there by means of
 * the {@link CrsHarvester}.
 *
 * @author Ayco Holleman
 */
@SuppressWarnings({"CheckStyle", "DuplicatedCode"})
public class CrsMultiMediaImportOffline {

  /**
   * Main.
   */
  public static void main(String[] args) {
    String jobId = UUID.randomUUID().toString();
    try {
      CrsMultiMediaImportOffline importer = new CrsMultiMediaImportOffline(jobId);
      importer.importMultimedia();
    } catch (Throwable t) {
      logger.error("CrsImportAll terminated unexpectedly!", t);
      System.exit(1);
    } finally {
      if (shouldUpdateES) {
        ESUtil.refreshIndex(MULTI_MEDIA_OBJECT);
      }
      ESClientManager.getInstance().closeClient();
    }
  }

  private static final Logger logger;
  private static final boolean shouldUpdateES;
  private static final boolean doEnrich;

  static {
    logger = ETLRegistry.getInstance().getLogger(CrsMultiMediaImportOffline.class);
    shouldUpdateES = !DaoRegistry.getInstance()
        .getConfiguration().get(SYSPROP_ETL_OUTPUT, "es").equals("file");
    doEnrich = DaoRegistry.getInstance()
        .getConfiguration().get(SYSPROP_ETL_ENRICH, "false").equals("true");
  }

  private final boolean suppressErrors;
  private final int esBulkRequestSize;
  private final Report report;

  private ETLStatistics stats;
  private CrsMultiMediaTransformer transformer;
  private DocumentObjectWriter<MultiMediaObject> loader;

  /**
   * Offline processing of previously harvested specimen source files.
   */
  public CrsMultiMediaImportOffline(String jobId) {
    suppressErrors = ConfigObject.isEnabled("suppressErrors");
    String key = ETLConstants.SYSPROP_LOADER_QUEUE_SIZE;
    String val = System.getProperty(key, "1000");
    esBulkRequestSize = Integer.parseInt(val);
    logger.info("shouldUpdateES: {}", shouldUpdateES);
    this.report = new Report(jobId, CRS, MULTI_MEDIA_OBJECT);
    report.initialiseUrls();
    logger.info("ETL Multimedia report: {}", report.getReportName());
  }

  /**
   * Import multimedia from the data directory configured in
   * nda-import.properties.
   */
  public void importMultimedia() {
    long start = System.currentTimeMillis();
    report.setStatus(Status.IN_PROGRESS);
    report.setThemesVersion(ThemeCache.getInstance().getThemesVersion());
    File[] xmlFiles = getXmlFiles();
    if (xmlFiles == null || xmlFiles.length == 0) {
      report.setStatus(Status.FAILED);
      logger.error("No multimedia oai.xml files found. Check nba.properties");
      return;
    }
    if (shouldUpdateES) {
      try {
        ETLUtil.truncate(MULTI_MEDIA_OBJECT, CRS);
      } catch (DaoException e) {
        logger.error("Failed to truncate CRS multimedia index: {}", e.getMessage());
        System.exit(1);
      }
    }
    int cacheFailuresBegin = MimeTypeCacheFactory.getInstance().getCache().getMisses();
    stats = new ETLStatistics();
    stats.setOneToMany(true);
    transformer = new CrsMultiMediaTransformer(report, stats);
    transformer.setSuppressErrors(suppressErrors);

    if (doEnrich) {
      transformer.setEnrich(true);
      report.doEnrich();
      logger.info("Taxonomic enrichment of Specimen documents: true");
    }
    SexNormalizer.getInstance().resetStatistics();
    SpecimenTypeStatusNormalizer.getInstance().resetStatistics();
    PhaseOrStageNormalizer.getInstance().resetStatistics();
    ThemeCache.getInstance().resetMatchCounters();

    try {
      for (File f : xmlFiles) {
        importFile(f);
        report.save();
      }
    } finally {
      logger.info("Finished importing {} files", xmlFiles.length);
      report.setFinished();
    }
    report.addThemesSummary(ThemeCache.getInstance().getThemesSummary());
    logger.info(report.save());

    SexNormalizer.getInstance().logStatistics();
    SpecimenTypeStatusNormalizer.getInstance().logStatistics();
    PhaseOrStageNormalizer.getInstance().logStatistics();
    ThemeCache.getInstance().logMatchInfo();
    stats.logStatistics(logger);
    ETLUtil.logDuration(logger, getClass(), start);
    int cacheFailuresEnd = MimeTypeCacheFactory.getInstance().getCache().getMisses();
    if (cacheFailuresBegin != cacheFailuresEnd) {
      int misses = cacheFailuresEnd - cacheFailuresBegin;
      String fmt = "%d mime type cache lookup failures for CRS multimedia";
      logger.warn(String.format(fmt, misses));
      logger.warn("THE MIME TYPE CACHE IS OUT-OF-DATE!");
    }
  }

  private void importFile(File f) {
    logger.info("Processing file {}", f.getName());
    report.addSourceFile(f);
    if (DaoRegistry.getInstance().getConfiguration().get(SYSPROP_ETL_OUTPUT, "file").equals("file")) {
      logger.info("ETL Output: Writing the multimedia documents to the file system");
      try {
        loader = new CrsMultiMediaJsonNDWriter(f.getName(), report, stats);
      } catch (IOException e) {
        report.addProcessingError(ProcessingError.IO_ERROR, f);
        logger.error(
            "Failed to create export file for source file {}: {}", f.getName(), e.getMessage());
      }
    } else {
      logger.info("ETL Output: loading the multimedia documents into the document store");
      loader = new CrsMultiMediaLoader(esBulkRequestSize, stats);
    }

    CrsExtractor extractor;
    try {
      extractor = new CrsExtractor(f, stats);
    } catch (SAXException e) {
      report.addProcessingError(ProcessingError.PARSING_ERROR, e.getMessage() + ": " + f.getName());
      logger.error("Processing failed!");
      logger.error(e.getMessage());
      return;
    }
    for (XMLRecordInfo extracted : extractor) {
      List<MultiMediaObject> transformed = transformer.transform(extracted);
      loader.write(transformed);
      if (stats.recordsProcessed != 0 && stats.recordsProcessed % 50000 == 0) {
        logger.info("Records processed: {}", stats.recordsProcessed);
        logger.info("Documents indexed: {}", stats.documentsIndexed);
      }
    }
    if (shouldUpdateES) {
      loader.flush();
    }
    IOUtil.close(loader);
  }

  private static File[] getXmlFiles() {
    ConfigObject config = DaoRegistry.getInstance().getConfiguration();
    String path = config.required("crs.data.dir").concat("/multimedia");
    logger.info("Data directory for CRS multimedia import: " + path);
    // TODO: replace with lambda
    File[] files =
        new File(path)
            .listFiles(
                new FilenameFilter() {

                  public boolean accept(File dir, String name) {
                    //if (!name.startsWith("multimedia.")) {
                    if (!name.startsWith("oai-pmh-request")) {
                      return false;
                    }
                    //if (!name.endsWith(".oai.xml")) {
                    return name.endsWith(".xml");
                  }
                });
    logger.debug("Sorting file list");
    if (files != null) {
      Arrays.sort(files);
    }
    return files;
  }
}
