package nl.naturalis.nba.etl;

import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.etl.report.Report;

@SuppressWarnings("unused")
public abstract class AbstractDocumentTransformer<
        INPUT extends IDocumentObject, OUTPUT extends IDocumentObject>
    extends AbstractTransformer<INPUT, OUTPUT> {

  public AbstractDocumentTransformer(Report report, ETLStatistics stats) {
    super(report, stats);
  }
}
