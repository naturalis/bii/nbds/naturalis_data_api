package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.api.model.SourceSystem.BRAHMS;
import static nl.naturalis.nba.dao.DocumentType.SPECIMEN;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_ENRICH;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_OUTPUT;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_LOADER_QUEUE_SIZE;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_SUPPRESS_ERRORS;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_TRUNCATE;
import static nl.naturalis.nba.etl.ETLUtil.getLogger;
import static nl.naturalis.nba.etl.ETLUtil.logDuration;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getCsvFiles;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getDataDir;

import com.univocity.parsers.common.TextParsingException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.util.es.ESUtil;
import nl.naturalis.nba.etl.CSVExtractor;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLRuntimeException;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.normalize.SpecimenTypeStatusNormalizer;
import nl.naturalis.nba.etl.report.ProcessingError;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Export;
import nl.naturalis.nba.etl.report.Report.Status;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.FileUtil;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;

/**
 * Manages the import of Brahms specimens.
 *
 * @author Ayco Holleman
 * @author Tom Gilissen
 */
public class BrahmsSpecimenImporter {

  private static final Logger logger = getLogger(BrahmsSpecimenImporter.class);
  private final Report report;
  private final int loaderQueueSize;
  private final boolean suppressErrors;
  private final boolean shouldUpdateEs;

  public BrahmsSpecimenImporter() {
    this(UUID.randomUUID().toString());
  }

  /**
   * Initialise the BrahmsSpecimenImporter with a jobId.
   *
   * @param jobId of the ETL job
   */
  public BrahmsSpecimenImporter(String jobId) {
    report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.SPECIMEN);
    suppressErrors = ConfigObject.isEnabled(SYSPROP_SUPPRESS_ERRORS);
    String val = System.getProperty(SYSPROP_LOADER_QUEUE_SIZE, "1000");
    loaderQueueSize = Integer.parseInt(val);
    shouldUpdateEs = false;
    //  shouldUpdateEs = DaoRegistry.getInstance()
    //    .getConfiguration()
    //    .get(SYSPROP_ETL_OUTPUT, "es").equals("file") ? false : true;
  }

  /**
   * Main.
   */
  public static void main(String[] args) {
    try {
      BrahmsSpecimenImporter importer = new BrahmsSpecimenImporter();
      if (args.length == 0 || args[0].trim().length() == 0) {
        importer.importCsvFiles();
      } else {
        importer.importCsvFile(args[0]);
      }
    } catch (Throwable t) {
      logger.error("BrahmsSpecimenImporter terminated unexpectedly!", t);
      System.exit(1);
    } finally {
      ESUtil.refreshIndex(SPECIMEN);
      ESClientManager.getInstance().closeClient();
    }
  }

  /**
   * Start the import process with one csv file.
   *
   * @param path of the csv file
   */
  public void importCsvFile(String path) {
    File file;
    if (path.startsWith("/")) {
      file = new File(path);
    } else {
      file = FileUtil.newFile(getDataDir(), path);
    }
    if (!file.isFile()) {
      throw new ETLRuntimeException("No such file: " + file.getAbsolutePath());
    }
    importCsvFiles(new File[]{file});
  }

  /**
   * Iterates over the CSV files in the brahms data directory and imports them.
   */
  public void importCsvFiles() {
    importCsvFiles(getCsvFiles());
  }

  /**
   * Import the csv files given.
   *
   * @param csvFiles : array of csv files
   */
  public void importCsvFiles(File[] csvFiles) {
    long start = System.currentTimeMillis();

    if (csvFiles.length == 0) {
      logger.info("No CSV files to process");
      report.setFinished();
      return;
    }
    report.setStatus(Status.IN_PROGRESS);
    SpecimenTypeStatusNormalizer.getInstance().resetStatistics();
    ThemeCache.getInstance().resetMatchCounters();
    report.setThemesVersion(ThemeCache.getInstance().getThemesVersion());
    ETLStatistics stats = new ETLStatistics();
    if (ConfigObject.isEnabled(SYSPROP_TRUNCATE, true) && !shouldUpdateEs) {
      try {
      ETLUtil.truncate(SPECIMEN, BRAHMS);
      } catch (DaoException e) {
        logger.error("Failed to truncate the Brahms specimen index: {}", e.getMessage());
      }
    }

    // Process all files
    for (File f : csvFiles) {
      processFile(f, report, stats);
    }

    // Report is ready now
    report.addThemesSummary(ThemeCache.getInstance().getThemesSummary());
    report.setFinished();
    logger.info(report.save());
    SpecimenTypeStatusNormalizer.getInstance().logStatistics();
    ThemeCache.getInstance().logMatchInfo();
    stats.logStatistics(logger, "Specimens");
    logDuration(logger, getClass(), start);
  }

  private void processFile(File f, Report report, ETLStatistics globalStats) {

    long start = System.currentTimeMillis();
    report.addSourceFile(f);
    logger.debug("Processing file {}", f.getAbsolutePath());

    ETLStatistics myStats = new ETLStatistics();
    CSVExtractor<BrahmsCsvField> extractor;
    BrahmsSpecimenTransformer transformer;
    DocumentObjectWriter<Specimen> loader = null;
    try {
      extractor = createExtractor(f, myStats);
      transformer = new BrahmsSpecimenTransformer(report, myStats);
      // Temporary (?) modification to allow for enrichment during the specimen import
      if (DaoRegistry.getInstance()
          .getConfiguration().get(SYSPROP_ETL_ENRICH, "false").equals("true")) {
        transformer.setEnrich();
        report.doEnrich();
        logger.debug("Taxonomic enrichment of Specimen documents: true");
      }
      if (DaoRegistry.getInstance()
          .getConfiguration().get(SYSPROP_ETL_OUTPUT, "file").equals("file")) {
        report.setExport(Export.FILE);
        logger.debug("ETL Output: Writing the specimen documents to the file system");
        try {
          loader = new BrahmsSpecimenJsonNDWriter(f.getName(), report, myStats);
        } catch (IOException e) {
          report.addProcessingError(ProcessingError.IO_ERROR, f);
          logger.error(
              "Failed to create export file for source file {}: {}", f.getAbsolutePath(), e);
        }
      } else {
        report.setExport(Export.ES);
        logger.debug("ETL Output: loading the specimen documents into the document store");
        loader = new BrahmsSpecimenLoader(loaderQueueSize, myStats);
      }
      for (CSVRecordInfo<BrahmsCsvField> rec : extractor) {
        if (rec == null) {
          continue;
        }
        loader.write(transformer.transform(rec));
        report.save();
        if (myStats.recordsProcessed != 0 && myStats.recordsProcessed % 50000 == 0) {
          logger.info("Records processed: {}", myStats.recordsProcessed);
          logger.info("Documents indexed: {}", myStats.documentsIndexed);
        }
      }
    } catch (TextParsingException e) {
      report.addProcessingError(
          ProcessingError.PARSING_ERROR,
          String.format("Failed to parse csv file: %s, at line %s", f.getName(), e.getLineIndex()));
      report.setFailed();
      logger.error("Parsing of csv file: {} failed!", f.getAbsolutePath());
      logger.error("Processing ended at line: {}", e.getLineIndex());
    } catch (OutOfMemoryError e) {
      report.addProcessingError(
          ProcessingError.OUT_OF_MEMORY_ERROR,
          String.format("Failed to parse source file: %s", f.getName()));
      logger.error("Parsing of file: {} failed! Cause: {}", f.getAbsolutePath(), e.getMessage());
    } finally {
      loader.flush();
      IOUtil.close(loader);
    }
    report.save(); // Still unfinished but we can print the intermediate stats
    myStats.logStatistics(logger, "Specimens");
    globalStats.add(myStats);
    logDuration(logger, getClass(), start);
    logger.info(" ");
    logger.info(" ");
  }

  private CSVExtractor<BrahmsCsvField> createExtractor(File f, ETLStatistics extractionStats) {
    CSVExtractor<BrahmsCsvField> extractor = new CSVExtractor<>(f, BrahmsCsvField.class, extractionStats);
    extractor.setSkipHeader(true);
    extractor.setDelimiter(',');
    extractor.setCharset(StandardCharsets.UTF_8);
    extractor.setSuppressErrors(suppressErrors);
    return extractor;
  }
}
