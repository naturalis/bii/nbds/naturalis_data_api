package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.api.model.SourceSystem.BRAHMS;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.dao.util.es.ESUtil.getElasticsearchId;
import static nl.naturalis.nba.etl.ETLConstants.LICENCE;
import static nl.naturalis.nba.etl.ETLConstants.LICENCE_TYPE;
import static nl.naturalis.nba.etl.ETLConstants.SOURCE_INSTITUTION_ID;
import static nl.naturalis.nba.etl.MimeTypeCache.MEDIALIB_HTTPS_URL;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.descriptiontext;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.determinationday;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.determinationmonth;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.determinationyear;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.imagelist;
import static nl.naturalis.nba.etl.brahms.BrahmsImportAll.specimenObjectIdsCache;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getDefaultClassification;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getScientificName;
import static nl.naturalis.nba.etl.enrich.EnrichmentUtil.createEnrichments;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.MultiMediaContentIdentification;
import nl.naturalis.nba.api.model.MultiMediaGatheringEvent;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.api.model.TaxonomicEnrichment;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.MedialibIdsCache;
import nl.naturalis.nba.etl.MimeTypeCache;
import nl.naturalis.nba.etl.MimeTypeCacheFactory;
import nl.naturalis.nba.etl.Transformer;
import nl.naturalis.nba.etl.enrich.EnrichmentUtil;
import nl.naturalis.nba.etl.report.Issue;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Sample;

/**
 * The transformer component in the ETL cycle for Brahms multimedia.
 *
 * <p>This class provides a final implementation of the method
 * {@link #doTransform()} defined by the {@link Transformer} interface to validate the input and
 * produce the output of the Brahms multimedia.
 *
 * @author Tom Gilissen
 */
class BrahmsMultiMediaTransformer extends BrahmsTransformer<MultiMediaObject> {

  private static final String DEFAULT_IMAGE_QUALITY = "ac:GoodQuality";
  private final MimeTypeCache mimetypeCache;
  private static final MedialibIdsCache medialibIdsCache = MedialibIdsCache.getInstance();
  private boolean enrich = false;

  BrahmsMultiMediaTransformer(Report report, ETLStatistics stats) {
    super(report, stats);
    mimetypeCache = MimeTypeCacheFactory.getInstance().getCache();
  }

  /**
   * Set enrichment of multimedia document to either true or false.
   *
   * @param enrich true for enriching documents with extra data
   */
  @SuppressWarnings("SameParameterValue")
  void setEnrich(boolean enrich) {
    this.enrich = enrich;
  }

  boolean doEnrich() {
    return enrich;
  }

  @Override
  public final List<MultiMediaObject> transform(CSVRecordInfo<BrahmsCsvField> input) {
    this.input = input;
    report.recordsIngested++; // Here, we're counting the csv line we're processing right now
    stats.recordsProcessed++;
    if (skipRecord()) {
      report.addIssue(Issue.SKIPPED, new Sample(input.getLine()));
      report.recordsSkipped++; // csv line has been skipped
      stats.recordsSkipped++;
      return null;
    }
    objectID = getObjectID();
    if (objectID == null) {
      report.addIssue(Issue.MISSING_ID, new Sample(input.getLine()));
      report.recordsRejected++; // csv line has been rejected
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record skipped: csv record has no object ID");
      }
      return null;
    }
    // Orphan check
    // We do not want to create a multimedia document which has no parent specimen document
    if (!specimenObjectIdsCache.isEmpty() && !specimenObjectIdsCache.contains(objectID)) {
      report.addIssue(Issue.ORPHAN_RECORD, new Sample(input.getLine()));
      report.recordsRejected++;
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record skipped: there is no related specimen document");
      }
      return null;
    }
    String images = input.get(imagelist);
    if (images == null || images.isEmpty()) {
      report.addIssue(Issue.MISSING_MEDIALIB_URI, new Sample(input.getLine()));
      report.recordsRejected++; // csv line has been rejected
      stats.recordsRejected++;
      return null;
    }
    return doTransform();
  }

  @Override
  protected List<MultiMediaObject> doTransform() {
    stats.recordsAccepted++;
    int urlsProcessed = 0;
    int mmObjectsCreated = 0;
    int mmObjectsFailed = 0;

    ArrayList<MultiMediaObject> result = new ArrayList<>(3);
    try {
      String images = input.get(imagelist);
      String[] urls = images.split(",");
      stats.objectsProcessed += urls.length;

      for (String url : urls) {
        urlsProcessed++;
        MultiMediaObject mmo = transformOne(url);
        if (mmo != null) {
          result.add(mmo);
          mmObjectsCreated++;
        } else {
          mmObjectsFailed++;
          stats.objectsRejected++;
        }
      }
    } catch (Throwable t) {
      mmObjectsFailed++;
      report.addIssue(Issue.SOFTWARE_ERROR, new Sample(input.getLine()));
      stats.objectsRejected++;
      if (!suppressErrors) {
        error(t.getMessage());
        error(input.getLine());
      }
    }
    // We're now done with creating multimedia documents
    if (result.isEmpty()) {
      // no mm objects could be created:
      // something was wrong so record rejected
      report.addIssue(Issue.INCOMPLETE_MULTIMEDIA_RECORD, new Sample(input.getLine()));
      report.recordsRejected++;
    } else {
      // at least one mm object could be created: record processed
      report.recordsProcessed++;
    }
    report.urlsIngested += urlsProcessed;
    report.urlsProcessed += mmObjectsCreated;
    report.urlsRejected += mmObjectsFailed;
    return result;
  }

  private MultiMediaObject transformOne(String url) {
    try {
      URI uri = getUri(url);
      MultiMediaObject mmo = newMultiMediaObject();

      // Service Access Point
      String uriHash = String.valueOf(uri.toString().hashCode()).replace('-', '0');
      ServiceAccessPoint sap = newServiceAccessPoint(uri);
      if (sap == null) {
        return null;
      }
      mmo.addServiceAccessPoint(sap);

      // Specimen data
      mmo.setUnitID(objectID + '_' + uriHash);
      mmo.setId(getElasticsearchId(BRAHMS, mmo.getUnitID()));
      mmo.setSourceSystemId(mmo.getUnitID());
      String specimenId = getElasticsearchId(BRAHMS, objectID);
      mmo.setAssociatedSpecimenReference(specimenId);
      List<String> themes = themeCache.lookup(objectID, MULTI_MEDIA_OBJECT, BRAHMS);
      mmo.setTheme(themes);
      String description = input.get(descriptiontext, true);
      if (description != null) {
        mmo.setDescription(description.replaceAll("\u00001", ""));
      }
      mmo.setGatheringEvents(Collections.singletonList(getMultiMediaGatheringEvent(input)));
      mmo.setIdentifications(Collections.singletonList(getIdentification()));
      if (doEnrich()) {
        enrichIdentification(mmo);
      }
      stats.objectsAccepted++;
      return mmo;
    } catch (URISyntaxException e) {
      report.addIssue(Issue.INVALID_MEDIALIB_URI, new Sample(input.getLine(), url));
      stats.objectsRejected++;
      if (!suppressErrors) {
        debug("Invalid image URL: " + url);
      }
      return null;
    } catch (Throwable t) {
      report.addIssue(Issue.SOFTWARE_ERROR, new Sample(input.getLine(), t.getMessage()));
      handleError(t);
      return null;
    }
  }

  /*
   * Temporary (?) modification to allow for enrichment during the specimen import
   *
   * Retrieve taxonomic data from CoL and NSR and add it to the identification(s)
   */
  private void enrichIdentification(MultiMediaObject mmo) {

    // A specimen can have one or more identifications
    // We need to check all identifications

    for (MultiMediaContentIdentification identification : mmo.getIdentifications()) {
      List<Taxon> taxa = EnrichmentUtil.findEnrichingTaxa(identification);
      List<TaxonomicEnrichment> enrichment = createEnrichments(taxa);
      if (enrichment != null && !enrichment.isEmpty()) {
        identification.setTaxonomicEnrichments(enrichment);
      }
    }
  }

  private static MultiMediaObject newMultiMediaObject() {
    MultiMediaObject mmo = new MultiMediaObject();
    mmo.setSourceSystem(BRAHMS);
    mmo.setSourceInstitutionID(SOURCE_INSTITUTION_ID);
    mmo.setOwner(SOURCE_INSTITUTION_ID);
    mmo.setSourceID("Brahms");
    mmo.setLicenseType(LICENCE_TYPE);
    mmo.setLicense(LICENCE);
    mmo.setCollectionType("Botany");
    return mmo;
  }

  private MultiMediaContentIdentification getIdentification() {
    MultiMediaContentIdentification identification = new MultiMediaContentIdentification();
    identification.setTypeStatus(getTypeStatus());
    String y = input.get(determinationyear);
    String m = input.get(determinationmonth);
    String d = input.get(determinationday);
    identification.setDateIdentified(getDate(y, m, d));
    ScientificName sn = getScientificName(input);
    DefaultClassification dc = getDefaultClassification(input);
    identification.setScientificName(sn);
    identification.setDefaultClassification(dc);
    // System classification disabled for specimens and multimedia
    // identification.setSystemClassification(getSystemClassification(dc));
    return identification;
  }

  private MultiMediaGatheringEvent getMultiMediaGatheringEvent(CSVRecordInfo<BrahmsCsvField> record) {

    MultiMediaGatheringEvent ge = new MultiMediaGatheringEvent();
    populateGatheringEvent(ge, record);
    return ge;
  }

  private static URI getUri(String url) throws URISyntaxException {
    url = url.trim().replaceAll(" ", "%20");
    return new URI(url);
  }

  /**
   * newServiceAccessPoint() creates a Service Access Point,
   * based on the URI provided. The provided URI is taken
   * apart into:
   * - the MediaLib object id, and
   * - the format of the item (small, medium or large)
   * <br>
   * The object is checked against the mime type cache. If
   * it's not in the cache, no ServiceAccessPoint will be created;
   * if it is, the according mimetype will be used to create
   * the ServiceAccessPoint.
   * <br>
   * Note that the URI assigned to the ServiceAccessPoint is not
   * the URI provided, but a newly created URI based on:
   * - the object id, and
   * - the format taken from provided URI, and
   * - the mimetype retrieved from the mimetype cache.
   * The newly created URI is always a https uri.
   * <br>
   * E.g.:
   * This URI: <a href="https://medialib.naturalis.nl/file/id/L.2633361/format/large">...</a>
   * will create a ServiceAccessPoint with:
   * - accessURI: <a href="https://medialib.naturalis.nl/file/id/L.2633361/format/large">...</a>
   * - format: large
   * - variant: jpeg
   * <br>
   *
   * @param uri access uri from the source record
   * @return ServiceAccessPoint with accessURI, format and variant
   */
  @SuppressWarnings("AccessStaticViaInstance")
  private ServiceAccessPoint newServiceAccessPoint(URI uri) {
    String mediaObjectId;
    String size;
    String mimeType;

    Pattern pattern = Pattern.compile("^.*id/(.*)/format/(.*)$");
    Matcher matcher = pattern.matcher(uri.getRawPath());
    if (matcher.matches()) {
      mediaObjectId = matcher.group(1);
      if (!medialibIdsCache.contains(mediaObjectId)) {
        report.addIssue(
            Issue.IMAGE_ID_NOT_IN_MEDIA_LIBRARY,
            new Sample(input.getLine(), uri.toString()));
        if (!suppressErrors) {
          warn("Not an existing medialib URL: %s", uri.toString());
        }
        return null;
      }
      size = matcher.group(2);
      mimeType = mimetypeCache.getMimeType(mediaObjectId);
    } else {
      report.addIssue(Issue.INVALID_MEDIALIB_URI, new Sample(input.getLine(), uri.toString()));
      return null;
    }

    URI httpsUri;
    String uriStr = MEDIALIB_HTTPS_URL + mediaObjectId + "/format/" + size;
    try {
      httpsUri = new URI(uriStr);
    } catch (URISyntaxException e) {
      report.addIssue(Issue.MEDIALIB_URL_FAILURE, new Sample(input.getLine(), uriStr));
      warn(
          "Incorrect URI for use in ServiceAccessPoint: %s",
          MEDIALIB_HTTPS_URL + mediaObjectId + "/format/" + size);
      return null;
    }
    return new ServiceAccessPoint(httpsUri, mimeType, DEFAULT_IMAGE_QUALITY);
  }
}
