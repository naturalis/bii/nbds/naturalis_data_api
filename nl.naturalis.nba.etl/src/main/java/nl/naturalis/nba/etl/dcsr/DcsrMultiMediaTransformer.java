package nl.naturalis.nba.etl.dcsr;

import static nl.naturalis.nba.api.model.SourceSystem.DCSR;
import static nl.naturalis.nba.dao.util.es.ESUtil.getElasticsearchId;
import static nl.naturalis.nba.etl.ETLConstants.SOURCE_INSTITUTION_ID;
import static nl.naturalis.nba.etl.ETLUtil.getTestGenera;
import static nl.naturalis.nba.etl.TransformUtil.equalizeNameComponents;
import static nl.naturalis.nba.etl.TransformUtil.guessMimeType;
import static nl.naturalis.nba.etl.dcsr.DcsrImportUtil.val;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import nl.naturalis.nba.api.model.License;
import nl.naturalis.nba.api.model.LicenseType;
import nl.naturalis.nba.api.model.MultiMediaContentIdentification;
import nl.naturalis.nba.api.model.MultiMediaGatheringEvent;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.common.es.ESDateInput;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.etl.AbstractJSONTransformer;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.NameMismatchException;
import nl.naturalis.nba.etl.dcsr.model.Image;
import nl.naturalis.nba.etl.dcsr.model.DcsrTaxon;
import nl.naturalis.nba.etl.report.Issue;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Sample;

/**
 * Transforms and validates DCSR source data.
 *
 * @author Tom Gilissen
 */
class DcsrMultiMediaTransformer extends AbstractJSONTransformer<MultiMediaObject> {

  private static final ObjectMapper objectMapper = new ObjectMapper();
  private static final String DEFAULT_IMAGE_QUALITY = "ac:BestQuality";

  private Taxon taxon; // The taxon object belonging to this multimedia object
  private DcsrTaxon dcsrTaxon;

  private final String[] testGenera;

  public DcsrMultiMediaTransformer(Report report, ETLStatistics stats) {
    super(report, stats);
    report.initialiseUrls();
    /*
     * We only need this because we don't want to swamp the log file with
     * useless log messages in case we are creating a test set.
     */
    testGenera = getTestGenera();
  }

  /**
   * Set the taxon object associated with this multimedia object. The taxon object is extracted from
   * the same JSON record by the {@link DcsrTaxonTransformer}.
   *
   * @param the taxon to be set
   */
  public void setTaxon(Taxon taxon) {
    this.taxon = taxon;
  }

  @Override
  protected String getObjectID() {
    try {
      dcsrTaxon = objectMapper.readValue(input, DcsrTaxon.class);
      return dcsrTaxon.getNsr_id();
    } catch (JsonProcessingException e) {
      report.addIssue(Issue.JSON_ERROR, new Sample(input));
      report.recordsRejected++;
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record rejected! Missing nsr_id");
      }
      return null;
    }
  }

  @Override
  public List<MultiMediaObject> transform(String input) {
    this.input = input;
    report.recordsIngested++;

    if (taxon == null) {
      report.addIssue(Issue.PARENT_RECORD_FAILED, new Sample(input));
      report.recordsSkipped++;
      stats.recordsSkipped++;
      if (logger.isDebugEnabled() && testGenera == null) {
        debug("Ignoring images for skipped or invalid taxon");
      }
      return null;
    }

    stats.recordsProcessed++;
    objectID = getObjectID();
    return doTransform();
  }

  /**
   * Transforms an XML record into one ore more {@code MultiMediaObject}s. The multimedia
   * transformer does not keep track of record-level statistics. The assumption is that if the taxon
   * transformer was able to extract a taxon from the XML record, then the record was OK at the
   * record level.
   */
  @Override
  protected List<MultiMediaObject> doTransform() {

    Image[] images = dcsrTaxon.getImages();
    if (images == null || images.length == 0) {
      report.addIssue(Issue.MISSING_IMAGE_DATA, new Sample(input));
      report.recordsRejected++;
      if (logger.isDebugEnabled()) {
        debug("Skipping taxon without images");
      }
      stats.recordsSkipped++;
      return null;
    }
    report.recordsProcessed++;
    stats.recordsAccepted++;
    List<MultiMediaObject> mmos = new ArrayList<>(images.length);
    for (Image image : images) {
      MultiMediaObject mmo = transformOne(image);
      if (mmo != null) {
        mmos.add(mmo);
        report.urlsProcessed++;
      }
    }
    return mmos.size() == 0 ? null : mmos;
  }

  private MultiMediaObject transformOne(Image image) {
    report.urlsIngested++;
    stats.objectsProcessed++;
    try {
      URI uri = getUri(image);
      if (uri == null) {
        return null;
      }
      MultiMediaObject mmo = newMediaObject();
      String uriHash = String.valueOf(uri.hashCode()).replace('-', '0');
      mmo.setSourceSystemId(objectID + '_' + uriHash);
      mmo.setUnitID(mmo.getSourceSystemId());
      mmo.setId(getElasticsearchId(DCSR, mmo.getUnitID()));
      String format = val(image.getMime_type());
      if (format == null || format.length() == 0) {
        if (!suppressErrors) {
          String fmt = "Missing mime type for image \"%s\" (taxon \"%s\").";
          warn(fmt, uri, taxon.getAcceptedName().getFullScientificName());
        }
        format = guessMimeType(uri.toString());
        report.addIssue(
            Issue.MISSING_MIME_TYPE,
            new Sample(input, uri.toString() + " > guessed mime type: " + format));
      }
      mmo.addServiceAccessPoint(new ServiceAccessPoint(uri, format, DEFAULT_IMAGE_QUALITY));
      mmo.setCreator(val(image.getPhotographer_name()));
      mmo.setCopyrightText(val(image.getCopyright()));
      mmo.setLicenseType(LicenseType.parse(val(image.getLicence_type())));
      mmo.setLicense(License.parse(val(image.getLicence())));
      mmo.setDescription(val(image.getShort_description()));
      mmo.setCaption(mmo.getDescription());
      String date = val(image.getDate_taken());
      if (date != null && date.equalsIgnoreCase("in prep")) {
        report.addIssue(Issue.INVALID_DATE, new Sample(input, JsonUtil.toJson(image)));
        if (logger.isDebugEnabled()) {
          logger.debug("Invalid date: \"{}\"", date);
        }
        date = null;
      }
      String locality = val(image.getGeography());
      if (locality != null || date != null) {
        MultiMediaGatheringEvent ge = new MultiMediaGatheringEvent();
        mmo.setGatheringEvents(Collections.singletonList(ge));
        if (locality != null) {
          ge.setLocalityText(locality);
        }
        if (date != null) {
          ge.setDateTimeBegin(parseDateTaken(date));
          ge.setDateTimeEnd(ge.getDateTimeBegin());
        }
      }
      stats.objectsAccepted++;
      return mmo;
    } catch (NameMismatchException e) {
      report.addIssue(Issue.DATA_INCONSISTENCY, new Sample(input, e.getMessage()));
      report.urlsRejected++;
      return null;
    } catch (Throwable t) {
      report.addIssue(
          Issue.SOFTWARE_ERROR,
          new Sample(input, t.getMessage() + ": " + JsonUtil.toJson(image)));
      report.urlsRejected++;
      handleError(t);
      return null;
    }
  }

  private static final DateTimeFormatter formatter0 = DateTimeFormatter.ofPattern("dd MMMM yyyy");
  private static final DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("d MMMM yyyy");

  private OffsetDateTime parseDateTaken(String date) {
    ESDateInput input = new ESDateInput(date);
    OffsetDateTime odt = input.parseAsLocalDate(formatter0);
    if (odt == null) {
      odt = input.parseAsLocalDate(formatter1);
    }
    if (odt == null) {
      if (!suppressErrors) {
        warn("Invalid input for <date_taken>: " + date);
      }
    }
    return odt;
  }

  private MultiMediaObject newMediaObject() throws NameMismatchException {
    MultiMediaObject mmo = new MultiMediaObject();
    mmo.setSourceSystem(DCSR);
    mmo.setSourceInstitutionID(SOURCE_INSTITUTION_ID);
    mmo.setOwner(SOURCE_INSTITUTION_ID);
    mmo.setSourceID("LNG DCSR");
    String taxonId = getElasticsearchId(DCSR, taxon.getSourceSystemId());
    mmo.setAssociatedTaxonReference(taxonId);
    mmo.setIdentifications(Collections.singletonList(getIdentification()));
    equalizeNameComponents(mmo);
    return mmo;
  }

  private MultiMediaContentIdentification getIdentification() {
    MultiMediaContentIdentification mmci = new MultiMediaContentIdentification();
    mmci.setTaxonRank(taxon.getTaxonRank());
    mmci.setScientificName(taxon.getAcceptedName());
    mmci.setDefaultClassification(taxon.getDefaultClassification());
    mmci.setVernacularNames(taxon.getVernacularNames());
    return mmci;
  }

  private URI getUri(Image image) {
    String url = val(image.getUrl());
    if (url == null) {
      report.addIssue(Issue.MISSING_IMAGE_URI_DATA, new Sample(input));
      report.urlsRejected++;
      stats.objectsRejected++;
      if (!suppressErrors) {
        String sn = taxon.getAcceptedName().getFullScientificName();
        error("Empty <url> element for \"%s\"", sn);
      }
      return null;
    }
    try {
      return new URI(url.trim());
    } catch (URISyntaxException e) {
      report.addIssue(Issue.INVALID_MEDIALIB_URI, new Sample(input, url));
      report.urlsRejected++;
      stats.objectsRejected++;
      if (!suppressErrors) {
        error("Invalid image URL: \"%s\"", url);
      }
      return null;
    }
  }
}
