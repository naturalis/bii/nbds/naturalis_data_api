package nl.naturalis.nba.etl.report;

import java.util.HashMap;
import java.util.Objects;

/**
 * Data structure for tracking samples of issues occuring during
 * ETL transformation.
 *
 * @author Tom Gilissen
 */
public class IssueLogger {

  private final HashMap<Issue, IssueLog> issues;

  public IssueLogger() {
    issues = new HashMap<>();
  }

  /**
   * Adding an issue is actualy creating a new log item to an issue.
   * Or, recording a sample to an occuring issue.
   *
   * @param issue  issue
   * @param sample  sample
   *
   * @author Tom Gilissen
   */
  public void addIssue(Issue issue, Sample sample) {
    Objects.requireNonNull(issue);
    Objects.requireNonNull(sample);
    if (issues.containsKey(issue)) {
      issues.get(issue).addSample(sample);
    } else {
      issues.put(issue, new IssueLog(issue, sample));
    }
  }

  public HashMap<Issue, IssueLog> getIssues() {
    return issues;
  }

}
