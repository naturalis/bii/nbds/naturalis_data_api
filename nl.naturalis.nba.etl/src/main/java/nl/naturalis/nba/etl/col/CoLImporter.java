package nl.naturalis.nba.etl.col;

import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_OUTPUT;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_LOADER_QUEUE_SIZE;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_SUPPRESS_ERRORS;

import java.util.UUID;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.utils.ConfigObject;

abstract class CoLImporter {

  final int loaderQueueSize;
  final boolean suppressErrors;
  final boolean toFile;
  final boolean truncate;
  final String colYear;
  final Report report;

  CoLImporter() {
    suppressErrors = ConfigObject.isEnabled(SYSPROP_SUPPRESS_ERRORS);
    report = new Report(UUID.randomUUID().toString(), SourceSystem.COL, DocumentType.TAXON);
    String val = System.getProperty(SYSPROP_LOADER_QUEUE_SIZE, "1000");
    loaderQueueSize = Integer.parseInt(val);
    colYear = DaoRegistry.getInstance().getConfiguration().required("col.year");
    if (DaoRegistry.getInstance().getConfiguration().hasProperty(SYSPROP_ETL_OUTPUT)) {
      toFile =
          DaoRegistry.getInstance().getConfiguration().get(SYSPROP_ETL_OUTPUT, "file").equals("file");
    } else {
      toFile = true;
    }
    if (DaoRegistry.getInstance().getConfiguration().hasProperty("nl.naturalis.nba.etl.truncate")) {
      truncate =
          DaoRegistry.getInstance()
              .getConfiguration()
              .get("nl.naturalis.nba.etl.truncate", "true")
              .equals("true");
    } else {
      truncate = true;
    }
    System.out.println("truncate: " + truncate);
  }
}
