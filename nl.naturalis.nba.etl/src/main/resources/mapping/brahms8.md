```mermaid
flowchart LR

    specimencategory:::brahms8 --> trim([trim white space]) -- 1 --> recordBasis:::specimen
    typecategory:::brahms8 -- 0..1 ---> typestatus[identifications.typeStatus]:::specimen
    specimenaccession:::brahms8 --> prevUnit(["concatenate using |"]):::object -- 0..1 --> previousUnitsText:::specimen
    specimenbarcode:::brahms8 --> getElasticsearchId(["add '@BRAHMS'"]):::object -- 1 ---> id:::specimen
    specimenbarcode:::brahms8 -- 1 ---> sourceSystemId:::specimen
    specimenbarcode:::brahms8 -- 1 ---> unitID:::specimen
    specimenbarcode:::brahms8 -- 1 ---> unitGUID:::specimen
    oldbarcode:::brahms8--> prevUnit(["concatenate using |"]):::object
    addedby:::brahms8
    addedon:::brahms8
    clusterid:::brahms8
    determinedby:::brahms8 --> agentText[identifiers.agentTextstring]:::object -- 0..1 --> SpecimenIdentification:::object -- 1.. --> identification("identifications"):::specimen
    determinationday:::brahms8 --> dateTime[dateIdentifiedstring ]:::object -- 0..1 -->  SpecimenIdentification:::object
    determinationmonth:::brahms8 --> dateTime[dateIdentifiedstring ]:::object
    determinationyear:::brahms8 --> dateTime[dateIdentifiedstring ]:::object
    imagelist:::brahms8 --> split([split URLs]) -- 0.. --> associatedMultiMediaUris("associatedMultiMediaUris.accessUri"):::specimen
    determinationfamilyname:::brahms8
    determinationcalcfullname:::brahms8
    prefix:::brahms8
    fieldnumber:::brahms8 ----> collectorsFieldNumber:::specimen
    suffix:::brahms8
    collectionyear:::brahms8
    collectionmonth:::brahms8
    collectionday:::brahms8
    collectors:::brahms8
    additionalcollectors:::brahms8
    lldatum:::brahms8
    latitude:::brahms8
    longitude:::brahms8
    elevation:::brahms8 --> altitude:::gatheringEvent
    elevationmax:::brahms8
    localitynotes:::brahms8
    habitattext:::brahms8
    descriptiontext:::brahms8
    majoradminname:::brahms8 --> provinceState:::gatheringEvent
    minoradminname:::brahms8
    localityname:::brahms8
    continent:::brahms8
    countryname:::brahms8 --> country:::gatheringEvent
    genusname:::brahms8
    calcfullname:::brahms8
    calcacceptedname:::brahms8
    synofname:::brahms8
    speciesname:::brahms8
    subspecies:::brahms8
    variety:::brahms8
    forma:::brahms8
    speciesauthorname:::brahms8
    subspeciesauthorname:::brahms8
    varietyauthorname:::brahms8
    formaauthorname:::brahms8
    taxstatus:::brahms8
    genusid:::brahms8
    kingdom:::brahms8
    subkingdom:::brahms8
    phylum:::brahms8
    subphylum:::brahms8
    classname:::brahms8
    subclassname:::brahms8
    ordername:::brahms8
    familyname:::brahms8
    subfamily:::brahms8
    tribe:::brahms8
    subtribe:::brahms8
    genushybrid:::brahms8
    curationspeciesfullname:::brahms8
    museumcode:::brahms8
    
    provinceState:::gatheringEvent --> geObject(gatheringEvent):::object --> gatheringEvent:::specimen
    country:::gatheringEvent --> geObject(gatheringEvent):::object
    altitude:::gatheringEvent --> geObject(gatheringEvent):::object

    subgraph Brahms CSV
        specimencategory:::brahms8
        typecategory:::brahms8
        specimenaccession:::brahms8
        specimenbarcode:::brahms8
        oldbarcode:::brahms8
        addedby:::brahms8
        addedon:::brahms8
        clusterid:::brahms8
        determinedby:::brahms8
        determinationday:::brahms8
        determinationmonth:::brahms8
        determinationyear:::brahms8
        imagelist:::brahms8
        determinationfamilyname:::brahms8
        determinationcalcfullname:::brahms8
        prefix:::brahms8
        fieldnumber:::brahms8
        suffix:::brahms8
        collectionyear:::brahms8
        collectionmonth:::brahms8
        collectionday:::brahms8
        collectors:::brahms8
        additionalcollectors:::brahms8
        lldatum:::brahms8
        latitude:::brahms8
        longitude:::brahms8
        elevation:::brahms8
        elevationmax:::brahms8
        localitynotes:::brahms8
        habitattext:::brahms8
        descriptiontext:::brahms8
        majoradminname:::brahms8
        minoradminname:::brahms8
        localityname:::brahms8
        continent:::brahms8
        countryname:::brahms8
        genusname:::brahms8
        calcfullname:::brahms8
        calcacceptedname:::brahms8
        synofname:::brahms8
        speciesname:::brahms8
        subspecies:::brahms8
        variety:::brahms8
        forma:::brahms8
        speciesauthorname:::brahms8
        subspeciesauthorname:::brahms8
        varietyauthorname:::brahms8
        formaauthorname:::brahms8
        taxstatus:::brahms8
        genusid:::brahms8
        kingdom:::brahms8
        subkingdom:::brahms8
        phylum:::brahms8
        subphylum:::brahms8
        classname:::brahms8
        subclassname:::brahms8
        ordername:::brahms8
        familyname:::brahms8
        subfamily:::brahms8
        tribe:::brahms8
        subtribe:::brahms8
        genushybrid:::brahms8
        curationspeciesfullname:::brahms8
        museumcode:::brahms8
    end

    subgraph Specimen document
        recordBasis:::specimen
        typestatus[identifications.typeStatus]:::specimen
        id:::specimen
        sourceSystemId:::specimen
        unitID:::specimen
        unitGUID:::specimen
        previousUnitsText:::specimen
        recordBasisstring:::specimen
        sourceSystem.codestring:::specimen
        sourceSystem.namestring:::specimen
        sourceSystemIdstring:::specimen
        recordURIstring:::specimen
        unitIDstring:::specimen
        unitGUIDstring:::specimen
        collectorsFieldNumberstring:::specimen
        assemblageIDstring:::specimen
        sourceInstitutionIDstring:::specimen
        sourceIDstring:::specimen
        previousSourceIDstring:::specimen
        ownerstring:::specimen
        licenseTypestring:::specimen
        licensestring:::specimen
        kindOfUnitstring:::specimen
        collectionTypestring:::specimen
        sexstring:::specimen
        phaseOrStagestring:::specimen
        titlestring:::specimen
        notesstring:::specimen
        preparationTypestring:::specimen
        previousUnitsTextstring:::specimen
        numberOfSpecimenstring:::specimen
        fromCaptivitystring:::specimen
        objectPublicstring:::specimen
        multiMediaPublicstring:::specimen
        acquiredFrom.agentTextstring:::specimen
        dynamicPropertiesstring:::specimen
        informationWithheldstring:::specimen
        dataGeneralizationsstring:::specimen
        modifiedstring:::specimen
        theme:::specimen
    end
    
    subgraph AssociatedMultimediaUris
        associatedMultiMediaUris:::specimen
    end
    
    subgraph Identifications
        identification("identifications"):::specimen
    end
    
    subgraph GatheringEvent
        gatheringEvent:::specimen
    end

    classDef brahms8 stroke:#0f0
    classDef specimen stroke :#00f
    classDef gatheringEvent stroke :#f00


%% Multiplicity

%%     0..1 = Zero or one
%%     1 = One only
%%     0.. = Zero or more
%%     1..* = One or more
%%     3 = Three only
%%     0..5 = Zero to Five
%%     5..15 = Five to Fifteen
```