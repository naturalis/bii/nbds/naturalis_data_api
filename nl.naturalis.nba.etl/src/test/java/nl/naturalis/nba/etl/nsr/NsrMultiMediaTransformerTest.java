package nl.naturalis.nba.etl.nsr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.net.URI;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaContentIdentification;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.AbstractTransformer;
import nl.naturalis.nba.etl.AllTests;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.nsr.model.Image;
import nl.naturalis.nba.etl.nsr.model.NsrTaxon;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.utils.CommonReflectionUtil;
import nl.naturalis.nba.utils.reflect.ReflectionUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for NsrMultiMediaTransformer.java
 */
public class NsrMultiMediaTransformerTest {

  private final ObjectMapper objectMapper = new ObjectMapper();
  URL nsrFileURL;
  File nsrFile;

  /**
   * @throws java.lang.Exception Exception
   */
  @Before
  public void setUp() throws Exception {

    System.setProperty(
        "nl.naturalis.nba.etl.testGenera",
        "malus,parus,larus,bombus,rhododendron,felix,tulipa,rosa,canis,passer,trientalis");
    nsrFileURL = AllTests.class.getResource("nsr-export--2020-01-30_1359--05.jsonl");
    nsrFile = new File(nsrFileURL.getFile());
  }

  @After
  public void tearDown() {}

  /**
   * Test method for {@link nl.naturalis.nba.etl.nsr.NsrMultiMediaTransformer#doTransform()}.
   *
   * <p>Test to verify if the doTransform method returns an expected {List<MultiMediaObject> }
   *
   * @throws Exception e
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testDoTransform() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report taxonReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    Report multimediaReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    NsrTaxonTransformer taxonTransformer = new NsrTaxonTransformer(taxonReport, etlStatistics);
    NsrMultiMediaTransformer multimediaTransformer = new NsrMultiMediaTransformer(multimediaReport, etlStatistics);
    multimediaReport.initialiseUrls();

    List<Taxon> taxa;
    List<MultiMediaObject> actual = null;
    LineNumberReader lnr;
    FileReader fr = new FileReader(nsrFile);
    lnr = new LineNumberReader(fr, 4096);

    String json;
    while ((json = lnr.readLine()) != null) {
      NsrTaxon nsrTaxon = objectMapper.readValue(json, NsrTaxon.class);
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "input", json);
      CommonReflectionUtil.setField(NsrTaxonTransformer.class, taxonTransformer, "nsrTaxon", nsrTaxon);
      Object taxonObject = CommonReflectionUtil.callMethod(null, null, taxonTransformer, "doTransform");
      taxa = (List<Taxon>) taxonObject;

      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "input", json);
      multimediaTransformer.setTaxon(taxa.get(0));
      CommonReflectionUtil.setField(NsrMultiMediaTransformer.class, multimediaTransformer, "nsrTaxon", nsrTaxon);
      Object returned = CommonReflectionUtil.callMethod(null, null, multimediaTransformer, "doTransform");
      actual = (List<MultiMediaObject>) returned;
    }

    String expectedAssociatedTaxRef = "D3KF0JNQ0UA@NSR";
    String expectedCreator = "Arnold Wijker";
    String expectedCollectionType = null;
    String expectedCaption = "Adult winter";
    String expectedDescription = "Adult winter";
    String expectedId = "D3KF0JNQ0UA_01740142374@NSR";
    String expectedLicenseType = "Copyright";
    String expectedLicense = "CC BY-NC-ND";
    String expectedOwner = "Naturalis Biodiversity Center";
    String expectedSourceId = "LNG NSR";
    String expectedSourceSystemId = "D3KF0JNQ0UA_01740142374";
    String expectedUnitId = "D3KF0JNQ0UA_01740142374";

    assertNotNull("01", actual);
    assertEquals("02", expectedAssociatedTaxRef, actual.get(0).getAssociatedTaxonReference());
    assertEquals("03", expectedCreator, actual.get(0).getCreator());
    assertEquals("04", expectedCollectionType, actual.get(0).getCollectionType());
    assertEquals("05", expectedCaption, actual.get(0).getCaption());
    assertEquals("06", expectedDescription, actual.get(0).getDescription());
    assertEquals("07", expectedId, actual.get(0).getId());
    assertEquals("08", expectedLicenseType, actual.get(0).getLicenseType().toString());
    assertEquals("09", expectedLicense, actual.get(0).getLicense().toString());
    assertEquals("10", expectedOwner, actual.get(0).getOwner());
    assertEquals("11", expectedSourceId, actual.get(0).getSourceID());
    assertEquals("12", expectedSourceSystemId, actual.get(0).getSourceSystemId());
    assertEquals("13", expectedUnitId, actual.get(0).getUnitID());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.nsr.NsrMultiMediaTransformer#transformOne(String
   * nsrTaxon)}.
   *
   * <p>Test to verify if the transformOne method returns an expected {@link MultiMediaObject}
   * object
   *
   * @throws Exception Exception
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testTransformOne() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report taxonReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    Report multimediaReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    NsrTaxonTransformer taxonTransformer = new NsrTaxonTransformer(taxonReport, etlStatistics);
    NsrMultiMediaTransformer multimediaTransformer = new NsrMultiMediaTransformer(multimediaReport, etlStatistics);

    multimediaReport.initialiseUrls();

    MultiMediaObject actual = null;
    LineNumberReader lnr;
    FileReader fr = new FileReader(nsrFile);
    lnr = new LineNumberReader(fr, 4096);

    String json;
    while ((json = lnr.readLine()) != null) {
      NsrTaxon nsrTaxon = objectMapper.readValue(json, NsrTaxon.class);
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "input", json);
      CommonReflectionUtil.setField(NsrTaxonTransformer.class, taxonTransformer, "nsrTaxon", nsrTaxon);
      Object taxonObject = CommonReflectionUtil.callMethod(null, null, taxonTransformer, "doTransform");
      List<Taxon> taxa = (List<Taxon>) taxonObject;

      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "input", json);
      multimediaTransformer.setTaxon(taxa.get(0));
      CommonReflectionUtil.setField(NsrMultiMediaTransformer.class, multimediaTransformer, "nsrTaxon", nsrTaxon);

      Image[] images = nsrTaxon.getImages();
      Object returned = ReflectionUtil.call(multimediaTransformer, "transformOne", new Class[] {Image.class}, images[0]);
      actual = (MultiMediaObject) returned;
    }

    String expectedAssociatedTaxRef = "D3KF0JNQ0UA@NSR";
    String expectedCreator = "Arnold Wijker";
    String expectedCollectionType = null;
    String expectedCaption = "Adult winter";
    String expectedDescription = "Adult winter";
    String expectedId = "D3KF0JNQ0UA_01740142374@NSR";
    String expectedLicenseType = "Copyright";
    String expectedLicense = "CC BY-NC-ND";
    String expectedOwner = "Naturalis Biodiversity Center";
    String expectedSourceId = "LNG NSR";
    String expectedSourceSystemId = "D3KF0JNQ0UA_01740142374";
    String expectedUnitId = "D3KF0JNQ0UA_01740142374";

    assertNotNull("01", actual);
    assertEquals("02", expectedAssociatedTaxRef, actual.getAssociatedTaxonReference());
    assertEquals("03", expectedCreator, actual.getCreator());
    assertEquals("04", expectedCollectionType, actual.getCollectionType());
    assertEquals("05", expectedCaption, actual.getCaption());
    assertEquals("06", expectedDescription, actual.getDescription());
    assertEquals("07", expectedId, actual.getId());
    assertEquals("08", expectedLicenseType, actual.getLicenseType().toString());
    assertEquals("9", expectedLicense, actual.getLicense().toString());
    assertEquals("10", expectedOwner, actual.getOwner());
    assertEquals("11", expectedSourceId, actual.getSourceID());
    assertEquals("12", expectedSourceSystemId, actual.getSourceSystemId());
    assertEquals("13", expectedUnitId, actual.getUnitID());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.nsr.NsrMultiMediaTransformer#parseDateTaken(String
   * date)}.
   *
   * <p>Test to verify if the parseDateTaken method returns an expected {OffsetDateTime} object
   *
   */
  @Test
  public void testParseDateTaken() {

    String jobId = UUID.randomUUID().toString();
    Report multimediaReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    NsrMultiMediaTransformer mediaTransformer = new NsrMultiMediaTransformer(multimediaReport, etlStatistics);

    Object returned = ReflectionUtil.call(mediaTransformer, "parseDateTaken", new Class[] {String.class}, "10 February 2013");
    OffsetDateTime date = (OffsetDateTime) returned;

    String expectedDateString = "2013-02-10T00:00Z";
    assertNotNull(date);
    assertEquals(expectedDateString, date.toString());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.nsr.NsrMultiMediaTransformer#newMediaObject()}.
   *
   * <p>Test to verify if the newMediaObject method returns an expected {MultiMediaObject} object
   *
   * @throws Exception e
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testNewMediaObject() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report taxonReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    Report multimediaReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    NsrTaxonTransformer taxonTransformer = new NsrTaxonTransformer(taxonReport, etlStatistics);
    NsrMultiMediaTransformer multimediaTransformer = new NsrMultiMediaTransformer(multimediaReport, etlStatistics);
    multimediaReport.initialiseUrls();

    LineNumberReader lnr;
    FileReader fr = new FileReader(nsrFile);
    lnr = new LineNumberReader(fr, 4096);

    MultiMediaObject actual = null;

    String json;
    while ((json = lnr.readLine()) != null) {
      NsrTaxon nsrTaxon = objectMapper.readValue(json, NsrTaxon.class);
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "input", json);
      CommonReflectionUtil.setField(NsrTaxonTransformer.class, taxonTransformer, "nsrTaxon", nsrTaxon);
      Object taxonObject = CommonReflectionUtil.callMethod(null, null, taxonTransformer, "doTransform");
      List<Taxon> taxa = (List<Taxon>) taxonObject;

      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "input", json);
      multimediaTransformer.setTaxon(taxa.get(0));
      CommonReflectionUtil.setField(NsrMultiMediaTransformer.class, multimediaTransformer, "nsrTaxon", nsrTaxon);

      Object returned = CommonReflectionUtil.callMethod(null, null, multimediaTransformer, "newMediaObject");
      actual = (MultiMediaObject) returned;
    }

    String expectedAssociatedTaxRef = "D3KF0JNQ0UA@NSR";
    String expectedCollectionType = null;
    String expectedOwner = "Naturalis Biodiversity Center";
    String expectedSourceId = "LNG NSR";

    assertNotNull("01", actual);
    assertEquals("02", expectedAssociatedTaxRef, actual.getAssociatedTaxonReference());
    assertEquals("03", expectedCollectionType, actual.getCollectionType());
    assertEquals("04", expectedOwner, actual.getOwner());
    assertEquals("05", expectedSourceId, actual.getSourceID());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.nsr.NsrMultiMediaTransformer#getIdentification()}.
   *
   * <p>Test to verify if the getIdentification method returns an expected
   * {#MultiMediaContentIdentification} object
   *
   * @throws Exception e
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testGetIdentification() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report taxonReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    Report multimediaReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    NsrTaxonTransformer taxonTransformer = new NsrTaxonTransformer(taxonReport, etlStatistics);
    NsrMultiMediaTransformer multimediaTransformer = new NsrMultiMediaTransformer(multimediaReport, etlStatistics);
    multimediaReport.initialiseUrls();

    LineNumberReader lnr;
    FileReader fr = new FileReader(nsrFile);
    lnr = new LineNumberReader(fr, 4096);

    MultiMediaContentIdentification actual = null;
    String json;
    while ((json = lnr.readLine()) != null) {
      NsrTaxon nsrTaxon = objectMapper.readValue(json, NsrTaxon.class);
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "input", json);
      CommonReflectionUtil.setField(NsrTaxonTransformer.class, taxonTransformer, "nsrTaxon", nsrTaxon);
      Object taxonObject = CommonReflectionUtil.callMethod(null, null, taxonTransformer, "doTransform");
      List<Taxon> taxa = (List<Taxon>) taxonObject;

      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "input", json);
      multimediaTransformer.setTaxon(taxa.get(0));
      CommonReflectionUtil.setField(NsrMultiMediaTransformer.class, multimediaTransformer, "nsrTaxon", nsrTaxon);

      Object returned = CommonReflectionUtil.callMethod(null, null, multimediaTransformer, "getIdentification");
      actual = (MultiMediaContentIdentification) returned;
    }

    String expectedTaxonRank = "subspecies";
    String expectedFullScientificName = "Larus argentatus argentatus Pontoppidan, 1763";
    String expectedScientificNameGroup = "larus argentatus argentatus";
    String expectedGenusOrMonomial = "Larus";

    assertNotNull("01", actual);
    assertEquals("02", expectedTaxonRank, actual.getTaxonRank());
    assertEquals("03", expectedFullScientificName, actual.getScientificName().getFullScientificName());
    assertEquals("04", expectedScientificNameGroup, actual.getScientificName().getScientificNameGroup());
    assertEquals("05", expectedGenusOrMonomial, actual.getScientificName().getGenusOrMonomial());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.nsr.NsrMultiMediaTransformer#getUri(Image image)}.
   *
   * <p>Test to verify if the getUri method returns an expected {#URI} object
   *
   * @throws Exception e
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testGetUri() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report taxonReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    Report multimediaReport = new Report(jobId, SourceSystem.NSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    NsrTaxonTransformer taxonTransformer = new NsrTaxonTransformer(taxonReport, etlStatistics);
    NsrMultiMediaTransformer multimediaTransformer = new NsrMultiMediaTransformer(multimediaReport, etlStatistics);
    multimediaReport.initialiseUrls();

    LineNumberReader lnr;
    FileReader fr = new FileReader(nsrFile);
    lnr = new LineNumberReader(fr, 4096);

    URI actual = null;
    String json;
    while ((json = lnr.readLine()) != null) {
      NsrTaxon nsrTaxon = objectMapper.readValue(json, NsrTaxon.class);
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, taxonTransformer, "input", json);
      CommonReflectionUtil.setField(NsrTaxonTransformer.class, taxonTransformer, "nsrTaxon", nsrTaxon);
      Object taxonObject = CommonReflectionUtil.callMethod(null, null, taxonTransformer, "doTransform");
      List<Taxon> taxa = (List<Taxon>) taxonObject;

      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "objectID", "D3KF0JNQ0UA");
      CommonReflectionUtil.setField(AbstractTransformer.class, multimediaTransformer, "input", json);
      multimediaTransformer.setTaxon(taxa.get(0));
      CommonReflectionUtil.setField(NsrMultiMediaTransformer.class, multimediaTransformer, "nsrTaxon", nsrTaxon);

      Image[] imageElems = nsrTaxon.getImages();
      Object returned = ReflectionUtil.call(multimediaTransformer, "getUri", new Class[] {Image.class}, imageElems[0]);
      actual = (URI) returned;
    }

    String expectedURI = "https://images.naturalis.nl/original/104527_zilvermeeuw-20130210-egmond_aan_zee-001arnold_wijker.jpg";
    assertNotNull("01", actual);
    assertEquals("02", expectedURI, actual.toString());
  }
}
