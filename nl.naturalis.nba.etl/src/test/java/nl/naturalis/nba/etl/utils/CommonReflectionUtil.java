package nl.naturalis.nba.etl.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class CommonReflectionUtil {

  /**
   * Generic Reflection method for methods
   *
   * @param param       the parameter used in the method
   * @param paramClass  the class of the parameter used
   * @param objectType  the object the method is part of
   * @param methodname  the name of the method to be tested
   *
   * @return Object
   * @throws Exception  when the class of the object doesn't match
   */
  public static <T> T callMethod(
      Object param, Class<T> paramClass, Object objectType, Object methodname) throws Exception {

    T obj;
    Method method;
    if (param != null) {
      method = Class.forName(
                objectType.getClass().getName()).getDeclaredMethod(methodname.toString(), paramClass
              );
      method.setAccessible(true);
      obj = (T) method.invoke(objectType, param);
    } else {
      method = Class.forName(
                objectType.getClass().getName()).getDeclaredMethod(methodname.toString()
              );
      method.setAccessible(true);
      obj = (T) method.invoke(objectType);
    }
    return obj;
  }

  /**
   * Generic Reflection method to access private field.
   *
   * @param className   the class
   * @param object      the object
   * @param methodName  the method name
   *
   * @throws Exception  when the class doesn't contain the specified method
   */
  public static <T> void setField(
      Class<T> className, Object object, String fieldName, Object filedValue) throws Exception {

    Field field = className.getDeclaredField(fieldName);
    field.setAccessible(true);
    field.set(object, filedValue);
  }
}
