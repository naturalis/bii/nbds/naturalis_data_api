package nl.naturalis.nba.etl.report;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.DocumentType;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for Report.java
 */
public class ReportTest {

  @Before
  public void setUp() {
  }

  public void tearDown() {}

  @Test
  public void testReport() {

    String jobId = "1234-5678-9101";
    SourceSystem sourceSystem = SourceSystem.NSR;
    DocumentType<?> documentType = DocumentType.TAXON;
    Report report = new Report(jobId, sourceSystem, documentType);

    assertEquals(jobId, report.getJobId());
    assertEquals(sourceSystem.getCode(), report.getSourceSystem());
    assertEquals(documentType.getName(), report.getDocumentType());
  }

  @Test
  public void testSamples() throws JsonProcessingException {

    String jobId = "1234-5678-9101";
    SourceSystem sourceSystem = SourceSystem.NSR;
    DocumentType<?> documentType = DocumentType.TAXON;
    Report report = new Report(jobId, sourceSystem, documentType);

    Sample s1 = new Sample("in prep.");
    Sample s2 = new Sample("in prep.");
    Sample s3 = new Sample("September 13th, 1911");
    assertEquals(s1, s2);
    assertNotEquals(s1, s3);

    report.addIssue(Issue.INVALID_REFERENCE_DATE, s1);
    report.addIssue(Issue.INVALID_REFERENCE_DATE, s2);
    report.addIssue(Issue.INVALID_REFERENCE_DATE, s3);

    String json = JsonUtil.toJson(report);
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode jsonNode = objectMapper.readTree(json);
    jsonNode = jsonNode.get("issues").get("INVALID_REFERENCE_DATE");
    assertEquals(3, jsonNode.get("count").intValue());
    assertEquals(2, jsonNode.get("samples").size());

  }

}
