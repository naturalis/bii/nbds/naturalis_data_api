package nl.naturalis.nba.etl.nsr;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
  NsrImportUtilTest.class,
  NsrMultiMediaTransformerTest.class,
  NsrTaxonTransformerTest.class,
})
public class AllTests {}
