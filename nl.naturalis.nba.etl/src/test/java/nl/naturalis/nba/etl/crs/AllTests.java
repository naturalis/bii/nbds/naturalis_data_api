package nl.naturalis.nba.etl.crs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
  CrsImportUtilTest.class,
  CrsSpecimenTransformerTest.class,
  CrsMultiMediaTransformerTest.class,
})
public class AllTests {}
