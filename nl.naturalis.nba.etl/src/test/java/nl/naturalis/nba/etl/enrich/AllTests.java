package nl.naturalis.nba.etl.enrich;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
  MultimediaTaxonomicEnricherTest.class,
  SpecimenMultimediaEnricherTest.class,
  SpecimenTaxonomicEnricherTest.class,
})
public class AllTests {}
