package nl.naturalis.nba.etl.dcsr;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    DcsrMultiMediaTransformerTest.class,
    DcsrTaxonTransformerTest.class,
})
public class AllTests {}
