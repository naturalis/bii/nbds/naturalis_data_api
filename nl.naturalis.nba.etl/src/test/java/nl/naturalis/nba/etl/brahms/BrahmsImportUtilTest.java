package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.calcfullname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.classname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.familyname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.forma;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.formaauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.genusname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.kingdom;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.ordername;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.phylum;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.speciesauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.speciesname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subclassname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subfamily;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subkingdom;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subphylum;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subspecies;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subspeciesauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subtribe;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.tribe;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.variety;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.varietyauthorname;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.Monomial;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.etl.CSVRecordInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test class for BrahmsImportUtil.java
 */
public class BrahmsImportUtilTest {

  @Before
  public void setUp() {
    BrahmsImportUtil.removeBackupExtension(); // clears the backup extension after each run
  }

  @After
  public void tearDown() {
    BrahmsImportUtil.removeBackupExtension(); // clears the backup extension after each run
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#getCsvFiles()}.
   * <p>
   * Test for the method getCSV files to verify CSV files are read from the local system
   *
   */
  @Ignore("Test is of little use. Should be revised.")
  @Test
  public void testGetCsvFiles() {

    // Think of another test, because this test is of little use
    // It should not be dependent on some file to exists on the file system!

    File[] actualFiles = BrahmsImportUtil.getCsvFiles();
    assertTrue(   "01", Arrays.stream(actualFiles).findFirst().isPresent());
    String file = Arrays.stream(actualFiles).findFirst().get().getName();
    assertNotNull("02", file);
    assertEquals( "03", ".CSV", (file.substring(file.indexOf("."))));
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#backup()}.
   * <p>
   * Test to verify backup() method is called.
   */
  @Ignore("TODO: unfinished unit test")
  @Test
  public void testBackup() {
    // Unfinished unit test
    assertTrue(true);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#removeBackupExtension()}.
   * <p>
   * Test to verify removeExtension() method is called.
   */
  @Ignore("TODO: unfinished unit test")
  @Test
  public void testRemoveBackupExtension() {
    // Unfinished unit test
    assertTrue(true);
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#getScientificName(nl.naturalis.nba.etl.CSVRecordInfo)}.
   * <p>
   * Test to verify getScientificName method returns the expected {@link ScientificName} object
   */
  @Test
  public void testGetScientificName() {

    @SuppressWarnings("unchecked")
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    when(record.get(calcfullname)).thenReturn("Coprosma ernodeoides var. mauiensis H.St.John");
    when(record.get(genusname)).thenReturn("Coprosma");
    when(record.get(speciesname)).thenReturn("ernodeoides");
    when(record.get(forma)).thenReturn("NULL");
    when(record.get(formaauthorname)).thenReturn("NULL");
    when(record.get(variety)).thenReturn("mauiensis");
    when(record.get(varietyauthorname)).thenReturn("H.St.John");
    when(record.get(subspecies)).thenReturn("NULL");
    when(record.get(subspeciesauthorname)).thenReturn("NULL");
    when(record.get(speciesauthorname)).thenReturn("A.Gray");

    ScientificName expected = new ScientificName();
    expected.setFullScientificName("Coprosma ernodeoides var. mauiensis H.St.John");
    expected.setAuthorshipVerbatim("H.St.John");
    expected.setGenusOrMonomial("Coprosma");
    expected.setSpecificEpithet("ernodeoides");
    expected.setInfraspecificMarker("var.");
    expected.setInfraspecificEpithet("mauiensis");

    ScientificName actual = BrahmsImportUtil.getScientificName(record);
    assertNotNull("01", actual);
    assertEquals("02", expected.getFullScientificName(), actual.getFullScientificName());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#getDefaultClassification(nl.naturalis.nba.etl.CSVRecordInfo)}.
   * <p>
   * Test to verify the getDefault Classification to see if the expcted
   * {@link DefaultClassification} object is returned
   *
   */
  @Test
  public void testGetDefaultClassification() {

    @SuppressWarnings("unchecked")
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    when(record.get(kingdom)).thenReturn("kingdom");
    when(record.get(BrahmsCsvField.subkingdom)).thenReturn("subkingdom");
    when(record.get(BrahmsCsvField.phylum)).thenReturn("phylum");
    when(record.get(BrahmsCsvField.subphylum)).thenReturn("subphylum");
    when(record.get(classname)).thenReturn("class");
    when(record.get(BrahmsCsvField.subclassname)).thenReturn("subclass");
    when(record.get(BrahmsCsvField.ordername)).thenReturn("order");
    when(record.get(BrahmsCsvField.familyname)).thenReturn("family");
    when(record.get(BrahmsCsvField.subfamily)).thenReturn("subfamily");
    when(record.get(BrahmsCsvField.tribe)).thenReturn("tribe");
    when(record.get(BrahmsCsvField.subtribe)).thenReturn("subtribe");
    when(record.get(BrahmsCsvField.genusname)).thenReturn("genus");
    when(record.get(BrahmsCsvField.speciesname)).thenReturn("species");

    DefaultClassification classification = BrahmsImportUtil.getDefaultClassification(record);
    assertNotNull("01", classification);
    assertEquals("02", "kingdom", classification.getKingdom());
    assertEquals("03", "subkingdom", classification.getSubKingdom());
    assertEquals("04", "phylum", classification.getPhylum());
    assertEquals("05", "subphylum", classification.getSubPhylum());
    assertEquals("06", "class", classification.getClassName());
    assertEquals("07", "subclass", classification.getSubClass());
    assertEquals("08", "order", classification.getOrder());
    assertEquals("09", "family", classification.getFamily());
    assertEquals("10", "subfamily", classification.getSubFamily());
    assertEquals("11", "tribe", classification.getTribe());
    assertEquals("12", "subtribe", classification.getSubTribe());
    assertEquals("13", "genus", classification.getGenus());
    assertEquals("14", "species", classification.getSpecificEpithet());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#getSystemClassification(nl.naturalis.nba.api.model.DefaultClassification)}.
   * <p>
   * Test to verify the testGetSystemClassification() returns the expected List<@link Monomial>
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testGetSystemClassification() {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    when(record.get(kingdom)).thenReturn("kingdom");
    when(record.get(BrahmsCsvField.subkingdom)).thenReturn("subkingdom");
    when(record.get(BrahmsCsvField.phylum)).thenReturn("phylum");
    when(record.get(BrahmsCsvField.subphylum)).thenReturn("subphylum");
    when(record.get(classname)).thenReturn("class");
    when(record.get(BrahmsCsvField.subclassname)).thenReturn("subclass");
    when(record.get(BrahmsCsvField.ordername)).thenReturn("order");
    when(record.get(BrahmsCsvField.familyname)).thenReturn("family");
    when(record.get(BrahmsCsvField.subfamily)).thenReturn("subfamily");
    when(record.get(BrahmsCsvField.tribe)).thenReturn("tribe");
    when(record.get(BrahmsCsvField.subtribe)).thenReturn("subtribe");
    when(record.get(BrahmsCsvField.genusname)).thenReturn("genus");
    when(record.get(BrahmsCsvField.speciesname)).thenReturn("species");

    DefaultClassification classification = BrahmsImportUtil.getDefaultClassification(record);
    List<Monomial> generatedList = BrahmsImportUtil.getSystemClassification(classification);

    List<Monomial> expectedList = getMonomials();

    Map<String, String> actualResult = generatedList
            .stream()
            .collect(Collectors.toMap(Monomial::getName, Monomial::getRank));
    Map<String, String> expectedResult = expectedList
            .stream()
            .collect(Collectors.toMap(Monomial::getName, Monomial::getRank));

    assertNotNull("01", generatedList);
    assertEquals("02", expectedList.size(), generatedList.size());
    assertEquals("03", expectedResult, actualResult);
    assertArrayEquals("04", actualResult.keySet().toArray(), expectedResult.keySet().toArray());
    assertArrayEquals("05", actualResult.values().toArray(), expectedResult.values().toArray());
  }

  private static List<Monomial> getMonomials() {
    List<Monomial> expectedList = new ArrayList<>();
    Monomial monomial_1 = new Monomial();
    monomial_1.setName("kingdom");
    monomial_1.setRank("kingdom");
    expectedList.add(monomial_1);
    Monomial monomial_2 = new Monomial();
    monomial_2.setName("order");
    monomial_2.setRank("order");
    expectedList.add(monomial_2);
    Monomial monomial_3 = new Monomial();
    monomial_3.setName("family");
    monomial_3.setRank("family");
    expectedList.add(monomial_3);
    Monomial monomial_4 = new Monomial();
    monomial_4.setName("genus");
    monomial_4.setRank("genus");
    expectedList.add(monomial_4);
    Monomial monomial_5 = new Monomial();
    monomial_5.setName("species");
    monomial_5.setRank("species");
    expectedList.add(monomial_5);
    return expectedList;
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#getAuthorShipVerbatim(nl.naturalis.nba.etl.CSVRecordInfo)}.
   * <p>
   * Test to check if getAuthorShipVerbatim() returns the correct field value
   */
  @Test
  public void testGetAuthor() {

    @SuppressWarnings("unchecked")
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    when(record.get(forma)).thenReturn("forma");
    when(record.get(formaauthorname)).thenReturn("formaauthorname");
    ScientificName actual = BrahmsImportUtil.getScientificName(record);
    assertEquals("01", "formaauthorname", actual.getAuthorshipVerbatim());

    when(record.get(forma)).thenReturn("null");
    when(record.get(variety)).thenReturn("variety");
    when(record.get(varietyauthorname)).thenReturn("varietyauthorname");
    actual = BrahmsImportUtil.getScientificName(record);
    assertEquals("02", "varietyauthorname", actual.getAuthorshipVerbatim());

    when(record.get(variety)).thenReturn("NULL");
    when(record.get(subspecies)).thenReturn("subspecies");
    when(record.get(subspeciesauthorname)).thenReturn("subspeciesauthorname");
    actual = BrahmsImportUtil.getScientificName(record);
    assertEquals("03", "subspeciesauthorname", actual.getAuthorshipVerbatim());
  }

  /**
   * Test to check if getInfraspecificMarker() returns the correct value.
   */
  @Test
  public void testGetInfraspecificMarker() {

    @SuppressWarnings("unchecked")
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);

    when(record.get(forma)).thenReturn("forma");
    ScientificName actual = BrahmsImportUtil.getScientificName(record);
    assertEquals("01", "f.", actual.getInfraspecificMarker());

    when(record.get(forma)).thenReturn("");
    when(record.get(variety)).thenReturn("variety");
    actual = BrahmsImportUtil.getScientificName(record);
    assertEquals( "02", "var.", actual.getInfraspecificMarker());

    when(record.get(variety)).thenReturn("Null");
    when(record.get(subspecies)).thenReturn("subspecies");
    actual = BrahmsImportUtil.getScientificName(record);
    assertEquals( "03", "subsp.", actual.getInfraspecificMarker());

  }

  /**
   * Test to check the private getInfraspecificEpithet() returns the correct value.
   */
  @Test
  public void testGetInfraspecificEpithet() {

    @SuppressWarnings("unchecked")
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);

    when(record.get(forma)).thenReturn(" forma ");
    ScientificName actual = BrahmsImportUtil.getScientificName(record);
    assertEquals("01", "forma", actual.getInfraspecificEpithet());

    when(record.get(forma)).thenReturn(" ");
    when(record.get(variety)).thenReturn(" variety ");
    actual = BrahmsImportUtil.getScientificName(record);
    assertEquals("02", "variety", actual.getInfraspecificEpithet());

    when(record.get(variety)).thenReturn(" ");
    when(record.get(subspecies)).thenReturn("subspecies ");
    actual = BrahmsImportUtil.getScientificName(record);
    assertEquals("03", "subspecies", actual.getInfraspecificEpithet());

    when(record.get(subspecies)).thenReturn("");
    actual = BrahmsImportUtil.getScientificName(record);
    assertNull("04", actual.getInfraspecificEpithet());


  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.brahms.BrahmsImportUtil#getTaxonRank(nl.naturalis.nba.etl.CSVRecordInfo)}.
   * <p>
   * Test to verify if getTaxonRank returns the expected value
   */
//  @Ignore("Test must be revised after B8 migration")
  @Test
  public void testGetTaxonRank() {

    @SuppressWarnings("unchecked")
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);

    when(record.get(forma)).thenReturn("forma");
    String actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("01", "f.", actual);

    when(record.get(forma)).thenReturn(null);
    when(record.get(speciesname)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("02", "species", actual);

    when(record.get(speciesname)).thenReturn(null);
    when(record.get(genusname)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("03", "genus", actual);

    when(record.get(genusname)).thenReturn(null);
    when(record.get(subtribe)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("04", "subtribe", actual);

    when(record.get(subtribe)).thenReturn(null);
    when(record.get(tribe)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("05", "tribe", actual);

    when(record.get(tribe)).thenReturn(null);
    when(record.get(subfamily)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("06", "subfamily", actual);

    when(record.get(subfamily)).thenReturn(null);
    when(record.get(familyname)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("07", "family", actual);

    when(record.get(familyname)).thenReturn(null);
    when(record.get(ordername)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("08", "order", actual);

    when(record.get(ordername)).thenReturn(null);
    when(record.get(subclassname)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("09", "subclass", actual);

    when(record.get(subclassname)).thenReturn(null);
    when(record.get(classname)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("10", "class", actual);

    when(record.get(classname)).thenReturn(null);
    when(record.get(subphylum)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("11", "subphylum", actual);

    when(record.get(subphylum)).thenReturn(null);
    when(record.get(phylum)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("12", "phylum", actual);

    when(record.get(phylum)).thenReturn(null);
    when(record.get(subkingdom)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("13", "subkingdom", actual);

    when(record.get(subkingdom)).thenReturn(null);
    when(record.get(kingdom)).thenReturn("not empty");
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertEquals("14", "kingdom", actual);

    when(record.get(kingdom)).thenReturn(null);
    actual = BrahmsImportUtil.getTaxonRank(record);
    assertNull("15", actual);
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void testRemoveFungusPrefix() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

    Method indexOfMethod = BrahmsImportUtil.class.getDeclaredMethod("removeFungusPrefix", String.class);
    indexOfMethod.setAccessible(true);

    String actual = (String) indexOfMethod.invoke(BrahmsImportUtil.class, "Fungi-Basidiomycota");
    assertEquals("Basidiomycota", actual);

    actual = (String) indexOfMethod.invoke(BrahmsImportUtil.class, "fungi-Basidiomycota");
    assertEquals("Basidiomycota", actual);

    actual = (String) indexOfMethod.invoke(BrahmsImportUtil.class, "BasidioFungi-mycota");
    assertEquals("BasidioFungi-mycota", actual);

    actual = (String) indexOfMethod.invoke(BrahmsImportUtil.class, "BasidiomycotaFungi-");
    assertEquals("BasidiomycotaFungi-", actual);

    actual = (String) indexOfMethod.invoke(BrahmsImportUtil.class, "And now for something completely different");
    assertEquals("And now for something completely different", actual);

    String fungi = "fungi";
    assertNull(indexOfMethod.invoke(BrahmsImportUtil.class, fungi));

    String empty = null;
    assertNull(indexOfMethod.invoke(BrahmsImportUtil.class, empty));
  }
}
