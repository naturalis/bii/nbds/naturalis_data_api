package nl.naturalis.nba.dao;

import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.dao.util.es.ESUtil;
import org.junit.BeforeClass;

public class DaoTestBase {

  @BeforeClass
  public static void destroyAndCreateIndexes() {
    ESUtil.deleteAllIndices();
    ESUtil.createAllIndices();
  }

  protected static void save(Specimen... specimens) {
    DaoTestUtil.saveSpecimens(specimens);
  }

  protected static void save(Taxon... taxa) {
    DaoTestUtil.saveTaxa(taxa);
  }
}
