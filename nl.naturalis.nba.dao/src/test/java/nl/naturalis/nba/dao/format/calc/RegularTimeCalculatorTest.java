package nl.naturalis.nba.dao.format.calc;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import nl.naturalis.nba.api.model.GatheringEvent;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.format.CalculationException;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import org.junit.Before;
import org.junit.Test;

public class RegularTimeCalculatorTest {

  private static final Specimen specimen;

  static {
    specimen = new Specimen();
  }

  @Before
  public void init() {}

  /**
   * Unit test for the RegularTimeCalculator.
   *
   * @throws CalculatorInitializationException when an invalid path (field) is being used
   * @throws CalculationException when calculateValue() fails
   */
  @Test
  public void testCalculateValue() throws CalculatorInitializationException, CalculationException {

    String dateField = "gatheringEvent.dateTimeBegin";
    String strExpected = "11:34";

    RegularTimeCalculator calculator = new RegularTimeCalculator();
    Map<String, String> args = new HashMap<>();
    args.put(null, dateField);
    calculator.initialize(Specimen.class, args);

    // date / time string from source system
    String dateTimeStr = "1994-04-26T11:34:26";
    LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr);
    OffsetDateTime date = OffsetDateTime.of(dateTime, ZoneOffset.UTC);
    GatheringEvent event = new GatheringEvent();
    event.setDateTimeBegin(date);
    specimen.setGatheringEvent(event);
    EntityObject entity = new EntityObject(specimen);

    String strCalculated = calculator.calculateValue(entity);
    assertEquals(strExpected, strCalculated);
  }

  @Test(expected = CalculatorInitializationException.class)
  public void testInvalidFieldCalculation() throws CalculatorInitializationException {

    String field = "invalid.field";
    RegularTimeCalculator calculator = new RegularTimeCalculator();
    Map<String, String> args = new HashMap<>();
    args.put(null, field);
    calculator.initialize(Specimen.class, args);

  }

}
