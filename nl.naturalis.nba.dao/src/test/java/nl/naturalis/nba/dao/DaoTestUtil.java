package nl.naturalis.nba.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import nl.naturalis.nba.api.QueryResult;
import nl.naturalis.nba.api.model.GeoArea;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.util.es.ESUtil;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;

public class DaoTestUtil {

  private static final DaoRegistry registry;

  @SuppressWarnings("unused")
  private static final Logger logger;

  static {
    registry = DaoRegistry.getInstance();
    logger = registry.getLogger(DaoTestUtil.class);
  }

  /**
   * Asserts that the specified {@code QueryBuilder} instance has the same JSON
   * representation as the contents of the specified file.
   *
   * @param queryBuilder  the QueryBuilder instance to be tested
   * @param file  the file containing the JSON representation
   * @return true when the QueryBuilder and file representation match; false otherwise
   */
  public static boolean queryEquals(QueryBuilder queryBuilder, String file) {
    InputStream is = DaoTestUtil.class.getResourceAsStream(file);
    Map<String, Object> expected = JsonUtil.deserialize(is);
    Map<String, Object> actual = JsonUtil.deserialize(queryBuilder.toString());
    return actual.equals(expected);
  }

  /**
   * Checks that the provided query result contains a document with the provided
   * document ID. Useful if you expect the document to be there, but you can't
   * rely on the sort order of the returned documents.
   *
   * @param <T>  the query result
   * @param id  the id
   * @return true when the query result contains the specified id; false otherwise
   */
  public static <T extends IDocumentObject> boolean containsId(QueryResult<T> result, String id) {
    return result.stream().anyMatch(item -> item.getItem().getId().equals(id));
  }

  /**
   * Asserts that the specified JSON string is equal the JSON in the specified
   * file. Both JSON strings are first read into a map so formatting differences
   * don't play a role.
   *
   * @param unitTestClass  the unit test class
   * @param jsonString  the json string to be tested
   * @param jsonFile  a file containing a JSON representation
   * @return true when JSON string and file match; false otherwise
   */
  public static boolean jsonEquals(Class<?> unitTestClass, String jsonString, String jsonFile) {
    InputStream is = unitTestClass.getResourceAsStream(jsonFile);
    Map<String, Object> expected = JsonUtil.deserialize(is);
    Map<String, Object> actual = JsonUtil.deserialize(jsonString);
    return actual.equals(expected);
  }

  public static void saveSpecimens(Specimen... specimens) {
    DocumentType<?> dt = DocumentType.forClass(Specimen.class);
    ESUtil.disableAutoRefresh(dt.getIndexInfo());
    for (Specimen specimen : specimens) {
      saveSpecimen(specimen, false);
    }
    ESUtil.refreshIndex(dt.getIndexInfo());
  }

  public static void saveSpecimens(Collection<Specimen> specimens) {
    DocumentType<?> dt = DocumentType.forClass(Specimen.class);
    ESUtil.disableAutoRefresh(dt.getIndexInfo());
    for (Specimen specimen : specimens) {
      saveSpecimen(specimen, false);
    }
    ESUtil.refreshIndex(dt.getIndexInfo());
  }

  public static void saveSpecimen(Specimen specimen, boolean refreshIndex) {
    String id;
    if (specimen.getId() == null && specimen.getUnitID() != null && specimen.getSourceSystem() != null) {
      id = specimen.getUnitID() + "@" + specimen.getSourceSystem().getCode();
    } else {
      id = specimen.getId();
      specimen.setId(null);
    }
    saveObject(id, specimen, refreshIndex);
    specimen.setId(id);
  }

  public static void saveTaxa(Taxon... taxa) {
    DocumentType<?> dt = DocumentType.forClass(Taxon.class);
    ESUtil.disableAutoRefresh(dt.getIndexInfo());
    for (Taxon taxon : taxa) {
      saveTaxon(taxon, false);
    }
    ESUtil.refreshIndex(dt.getIndexInfo());
  }

  @SuppressWarnings("unused")
  public static void saveTaxa(Collection<Taxon> taxa) {
    DocumentType<?> dt = DocumentType.forClass(Taxon.class);
    ESUtil.disableAutoRefresh(dt.getIndexInfo());
    for (Taxon taxon : taxa) {
      saveTaxon(taxon, false);
    }
    ESUtil.refreshIndex(dt.getIndexInfo());
  }

  public static void saveTaxon(Taxon taxon, boolean refreshIndex) {
    String id;
    if (taxon.getId() == null
        && taxon.getSourceSystemId() != null
        && taxon.getSourceSystem() != null) {
      id = taxon.getSourceSystemId() + "@" + taxon.getSourceSystem().getCode();
    } else {
      id = taxon.getId();
      taxon.setId(null);
      saveObject(id, taxon, refreshIndex);
      taxon.setId(id);
    }
    taxon.setId(id);
  }

  @SuppressWarnings("unused")
  public static void saveMultiMediaObject(MultiMediaObject mmo, boolean refreshIndex) {
    if (mmo.getId() == null) {
      String id = mmo.getUnitID() + "@" + mmo.getSourceSystem().getCode();
      saveObject(id, mmo, refreshIndex);
    } else {
      String id = mmo.getId();
      mmo.setId(null);
      saveObject(id, mmo, refreshIndex);
      mmo.setId(id);
    }
  }

  public static void saveGeoAreas(GeoArea... areas) {
    DocumentType<?> dt = DocumentType.forClass(GeoArea.class);
    ESUtil.disableAutoRefresh(dt.getIndexInfo());
    for (GeoArea area : areas) {
      saveGeoArea(area, false);
    }
    ESUtil.refreshIndex(dt.getIndexInfo());
  }

  public static void saveGeoArea(GeoArea area, boolean refreshIndex) {
    if (area.getId() == null) {
      String id = area.getSourceSystemId() + "@" + area.getSourceSystem().getCode();
      saveObject(id, area, refreshIndex);
    } else {
      String id = area.getId();
      area.setId(null);
      saveObject(id, area, refreshIndex);
      area.setId(id);
    }
  }

  @SuppressWarnings("unused")
  public static void saveObject(IDocumentObject object, boolean refreshIndex) {
    saveObject(null, object, refreshIndex);
  }

  public static void saveObject(String id, IDocumentObject obj, boolean refreshIndex) {
    DocumentType<?> dt = DocumentType.forClass(obj.getClass());
    String index = dt.getIndexInfo().getName();
    IndexRequest request = new IndexRequest();
    request.index(index);
    if (id != null) {
      request.id(id);
    }
    byte[] source = JsonUtil.serialize(obj);
    request.source(source, XContentType.JSON);
    try {
      ESUtil.esClient().index(request, RequestOptions.DEFAULT);
      if (refreshIndex) {
        ESUtil.refreshIndex(dt);
      }
    } catch (IOException e) {
      throw new DaoException(
          String.format("Failed to index document in index \"%s\": %s", index, e.getMessage()));
    }
  }

  @SuppressWarnings("unused")
  private static RestHighLevelClient client() {
    return ESClientManager.getInstance().getClient();
  }
}
