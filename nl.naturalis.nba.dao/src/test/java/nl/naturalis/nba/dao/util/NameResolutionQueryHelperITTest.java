package nl.naturalis.nba.dao.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.NameResolutionQuerySpec;
import nl.naturalis.nba.api.NameResolutionRequest;
import nl.naturalis.nba.api.NameResolutionRequest.NameType;
import nl.naturalis.nba.api.QueryResult;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.DaoTestBase;
import nl.naturalis.nba.dao.SpecimenDao;
import nl.naturalis.nba.dao.SpecimenDao_NameResolutionTest;
import nl.naturalis.nba.dao.TaxonDao;
import nl.naturalis.nba.dao.exception.DaoException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static nl.naturalis.nba.dao.DaoTestUtil.containsId;

public class NameResolutionQueryHelperITTest extends DaoTestBase {

  /*
   * Just to generate some URLs to paste into browser
   */
  public static void main(String[] args) throws URISyntaxException {
    URIBuilder ub =
        new URIBuilder("http://145.136.242.167:8080/v2/specimen/queryWithNameResolution");
    NameResolutionQuerySpec qs = new NameResolutionQuerySpec();
    NameResolutionRequest nrs = new NameResolutionRequest();
    qs.setNameResolutionRequest(nrs);
    nrs.setSearchString("parus");
    nrs.setUseCoL(Boolean.TRUE);
    ub.addParameter("__explain", "true");
    ub.addParameter("__detail", "DEBUG");
    ub.addParameter("_querySpec", JsonUtil.toJson(qs));
    System.out.println(ub.build());
  }

  @Before
  public void before() {
    new SpecimenDao_NameResolutionTest().indexTestData();
  }

  @Ignore
  @Test
  public void testPost() throws URISyntaxException, ClientProtocolException, IOException {
    CloseableHttpClient client = HttpClients.createDefault();
    URIBuilder ub =
        new URIBuilder("http://145.136.242.167:8080/v2/specimen/queryWithNameResolution");
    ub.addParameter("__explain", "true");
    ub.addParameter("__level", "trace");
    System.out.println(ub.build());
    HttpPost post = new HttpPost(ub.build());
    NameResolutionQuerySpec qs = new NameResolutionQuerySpec();
    NameResolutionRequest nrs = new NameResolutionRequest();
    qs.setNameResolutionRequest(nrs);
    nrs.setSearchString("Larus Fuscus fuscus");
    nrs.setUseCoL(Boolean.TRUE);
    ArrayList<NameValuePair> params = new ArrayList<>();
    params.add(new BasicNameValuePair("_querySpec", JsonUtil.toJson(qs)));
    post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
    HttpResponse response = client.execute(post);
    System.out.println("Status: " + response.getStatusLine());
    response.getEntity().writeTo(System.out);
  }

  @Test
  public void test_01() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("FOO");
    query.setNameResolutionRequest(nrr);
    QueryResult<Taxon> result = new TaxonDao().query(NameResolutionUtils.convertToQuerySpec(nrr));
    assertEquals("01", 3, result.getTotalSize().intValue());
    assertTrue("02", containsId(result, "A"));
    assertTrue("03", containsId(result, "B"));
    assertTrue("04", containsId(result, "F"));
  }

  @Test
  public void test_02() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("FOO");
    nrr.setNameTypes(Set.of(NameType.ACCEPTED_NAME));
    query.setNameResolutionRequest(nrr);
    QueryResult<Taxon> result = new TaxonDao().query(NameResolutionUtils.convertToQuerySpec(nrr));
    assertEquals("01", 0, result.getTotalSize().intValue());
  }

  @Test
  public void test_03() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("FOO");
    nrr.setNameTypes(Set.of(NameType.VERNACULAR_NAME));
    query.setNameResolutionRequest(nrr);
    QueryResult<Taxon> result = new TaxonDao().query(NameResolutionUtils.convertToQuerySpec(nrr));
    assertEquals("01", 2, result.getTotalSize().intValue());
    assertTrue("02", containsId(result, "A"));
    assertTrue("04", containsId(result, "F"));
  }

  @Test
  public void test_04() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("FOO");
    nrr.setNameTypes(Set.of(NameType.SYNONYM));
    query.setNameResolutionRequest(nrr);
    QueryResult<Taxon> result = new TaxonDao().query(NameResolutionUtils.convertToQuerySpec(nrr));
    assertTrue("02", containsId(result, "A"));
    assertTrue("03", containsId(result, "B"));
  }

  @Test
  public void test_05() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("Sylve");
    nrr.setNameTypes(Set.of(NameType.SYNONYM));
    query.setNameResolutionRequest(nrr);
    QueryResult<Taxon> result = new TaxonDao().query(NameResolutionUtils.convertToQuerySpec(nrr));
    assertEquals("01", 0, result.getTotalSize().intValue());
  }

  @Test
  public void test_06() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("Sylve");
    nrr.setNameTypes(Set.of(NameType.ACCEPTED_NAME));
    query.setNameResolutionRequest(nrr);
    QueryResult<Taxon> result = new TaxonDao().query(NameResolutionUtils.convertToQuerySpec(nrr));
    assertEquals("01", 2, result.getTotalSize().intValue());
    assertTrue("02", containsId(result, "A"));
    assertTrue("03", containsId(result, "B"));
  }

  /*
   * This one is tricky b/c we are going to call CoLPlus so we can't make very
   * strong assertions.
   */
  @Test
  public void test_07() {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("Larus fuscus"); // safe bet
    nrr.setUseCoL(true);
    nrr.setFuzzyMatching(true);
    query.setNameResolutionRequest(nrr);
    NameResolutionQueryHelper helper = new NameResolutionQueryHelper(new SpecimenDao(), query);
    try {
      List<ScientificName> names = helper.callCoL();
      assertFalse("01", names.isEmpty());
    } catch (DaoException e) {
      if (e.getMessage().startsWith(NameResolutionQueryHelper.ERR_COLPLUS_CALL_FAILED)) {
        // Can't allow that to ruin the maven build
        System.err.println("WARNING: " + e.getMessage());
        return;
      }
      throw e;
    }
  }
}
