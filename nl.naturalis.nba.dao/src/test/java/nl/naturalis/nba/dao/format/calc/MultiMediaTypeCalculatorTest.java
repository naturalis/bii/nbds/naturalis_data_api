package nl.naturalis.nba.dao.format.calc;

import static org.junit.Assert.assertEquals;

import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.dao.format.EntityObject;
import org.junit.Test;

public class MultiMediaTypeCalculatorTest {

  private static MultiMediaObject multiMediaObject;

  static {
    multiMediaObject = new MultiMediaObject();
    multiMediaObject.setSourceSystem(SourceSystem.BRAHMS);
    multiMediaObject.setSourceSystemId("L.3403013_01106733566");
    multiMediaObject.setId("L.3403013_01106733566@BRAHMS");
  }

  @Test
  public void test_calculate_01() {
    multiMediaObject.setServiceAccessPoints(null);
    multiMediaObject.addServiceAccessPoint(
        "https://someurl.org", "application/pdf", "ac:GoodQuality");
    EntityObject entity = new EntityObject(multiMediaObject);
    MultiMediaTypeCalculator calculator = new MultiMediaTypeCalculator();

    String format = calculator.calculateValue(entity);
    assertEquals("01", "Text", format);
  }

  @Test
  public void test_calculate_02() {
    multiMediaObject.setServiceAccessPoints(null);
    multiMediaObject.addServiceAccessPoint("https://someurl.org", "audio/mp3", "ac:BestQuality");
    EntityObject entity = new EntityObject(multiMediaObject);
    MultiMediaTypeCalculator calculator = new MultiMediaTypeCalculator();

    String format = calculator.calculateValue(entity);
    assertEquals("01", "Sound", format);
  }

  @Test
  public void test_calculate_03() {
    multiMediaObject.setServiceAccessPoints(null);
    multiMediaObject.addServiceAccessPoint("https://someurl.org", "video/mp4", "ac:MediumQuality");
    EntityObject entity = new EntityObject(multiMediaObject);
    MultiMediaTypeCalculator calculator = new MultiMediaTypeCalculator();

    String format = calculator.calculateValue(entity);
    assertEquals("01", "MovingImage", format);
  }

  @Test
  public void test_calculate_04() {
    multiMediaObject.setServiceAccessPoints(null);
    multiMediaObject.addServiceAccessPoint("https://someurl.org", "image/jpeg", "Best Quality");
    EntityObject entity = new EntityObject(multiMediaObject);
    MultiMediaTypeCalculator calculator = new MultiMediaTypeCalculator();

    String format = calculator.calculateValue(entity);
    assertEquals("01", "StillImage", format);
  }

  @Test
  public void test_calculate_05() {
    multiMediaObject.setServiceAccessPoints(null);
    multiMediaObject.addServiceAccessPoint("https://someurl.org", "image/png", "ac:BestQuality");
    EntityObject entity = new EntityObject(multiMediaObject);
    MultiMediaTypeCalculator calculator = new MultiMediaTypeCalculator();

    String format = calculator.calculateValue(entity);
    assertEquals("01", "StillImage", format);
  }

  @Test
  public void test_calculate_06() {
    multiMediaObject.setServiceAccessPoints(null);
    multiMediaObject.addServiceAccessPoint("https://someurl.org", "not defined", "not defined");
    EntityObject entity = new EntityObject(multiMediaObject);
    MultiMediaTypeCalculator calculator = new MultiMediaTypeCalculator();

    String format = calculator.calculateValue(entity);
    assertEquals("01", "StillImage", format);
  }
}
