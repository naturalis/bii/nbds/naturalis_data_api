package nl.naturalis.nba.dao.util;

import java.util.Arrays;
import java.util.stream.Stream;
import nl.naturalis.common.check.Check;
import nl.naturalis.nba.api.*;
import static java.util.stream.Collectors.toList;
import static nl.naturalis.common.ObjectMethods.*;
import static nl.naturalis.common.check.CommonChecks.empty;
import static nl.naturalis.common.check.CommonChecks.gte;
import static nl.naturalis.common.check.CommonChecks.lte;
import static nl.naturalis.common.check.CommonChecks.nullOr;
import static nl.naturalis.nba.api.LogicalOperator.OR;
/*
 * Not part of the public API b/c the user never gets to see or create one. This subclass stores the
 * "search after" values. It is also vermy much geared towards creating as small a token as possible.
 *
 */
public class BatchQuerySpec extends QuerySpec {

  public static final int MIN_BATCH_SIZE = 10;
  public static final int MAX_BATCH_SIZE = 5000;
  public static final int DEFAULT_BATCH_SIZE = 1000;

  static BatchQuerySpec fromBlob(Object[] blob) {
    BatchQuerySpec qs = new BatchQuerySpec();
    qs.eat(blob);
    return qs;
  }

  private Object[] searchAfter;

  public BatchQuerySpec() {
    this.constantScore = Boolean.TRUE;
    this.size = DEFAULT_BATCH_SIZE;
  }

  BatchQuerySpec(QuerySpec query) throws InvalidQueryException {
    super(query);
    // Override (ignore) user input:
    this.constantScore = Boolean.TRUE;
    this.size = ifNull(size, DEFAULT_BATCH_SIZE);
    Check.with(InvalidQueryException::new, this, "query")
        .has(QuerySpec::getFrom, "from", nullOr(), 0)
        .has(QuerySpec::getSize, "size", gte(), MIN_BATCH_SIZE)
        .has(QuerySpec::getSize, "size", lte(), MAX_BATCH_SIZE)
        .has(QuerySpec::getSortFields, "sortFields", empty());
  }

  public Object[] getSearchAfter() {
    return searchAfter;
  }

  public void setSearchAfter(Object[] values) {
    this.searchAfter = values;
  }

  Object[] toBlob() {
    Object[] blob = new Object[5];
    blob[0] = ifNotNull(fields, x -> x.stream().map(Path::toString).toArray(String[]::new));
    blob[1] = ifNotEmpty(conditions, x -> x.stream().map(this::blob).toArray());
    blob[2] = logicalOperator == OR ? (byte) 1 : null;
    blob[3] = nullIf(size, DEFAULT_BATCH_SIZE);
    blob[4] = searchAfter;
    return blob;
  }

  private void eat(Object[] blob) {
    fields = ifNotNull(blob[0], x -> Arrays.stream((String[]) x).map(Path::new).collect(toList()));
    conditions = ifNotNull(blob[1], x -> stream(x).map(this::unblob).collect(toList()));
    doIf(blob[2] != null, () -> logicalOperator = OR);
    doIf(blob[3] != null, () -> size = (Integer) blob[3]);
    searchAfter = (Object[]) blob[4];
  }

  private Object[] blob(QueryCondition c) {
    Object[] blob = new Object[6];
    blob[0] = ifNotNull(c.getNot(), x -> (byte) 1);
    blob[1] = ifNotNull(c.getField(), Path::toString);
    blob[2] = ifNotNull(c.getOperator(), Enum::toString);
    blob[3] = c.getValue();
    blob[4] = ifNotEmpty(c.getAnd(), x -> x.stream().map(this::blob).toArray());
    blob[5] = ifNotEmpty(c.getOr(), x -> x.stream().map(this::blob).toArray());
    return blob;
  }

  private QueryCondition unblob(Object blob) {
    Object[] objs = (Object[]) blob;
    QueryCondition c = new QueryCondition();
    c.setNot(ifNotNull(objs[0], x -> UnaryBooleanOperator.NOT));
    c.setField(ifNotNull(objs[1], x -> new Path(x.toString())));
    c.setOperator(ifNotNull(objs[2], x -> ComparisonOperator.parse(x.toString())));
    c.setValue(objs[3]);
    c.setAnd(ifNotNull(objs[4], x -> stream(x).map(this::unblob).collect(toList())));
    c.setOr(ifNotNull(objs[5], x -> stream(x).map(this::unblob).collect(toList())));
    return c;
  }

  private static Stream<Object> stream(Object x) {
    return Arrays.stream((Object[]) x);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Arrays.deepHashCode(searchAfter);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (!super.equals(obj)) {
      return false;
    } else if (!(obj instanceof BatchQuerySpec)) {
      return false;
    }
    BatchQuerySpec other = (BatchQuerySpec) obj;
    return Arrays.deepEquals(searchAfter, other.searchAfter);
  }
}
