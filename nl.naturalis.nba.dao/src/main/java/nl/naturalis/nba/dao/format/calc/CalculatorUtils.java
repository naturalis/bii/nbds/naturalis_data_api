package nl.naturalis.nba.dao.format.calc;

import nl.naturalis.nba.api.NoSuchFieldException;
import nl.naturalis.nba.api.Path;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.common.PathUtil;
import nl.naturalis.nba.common.es.map.MappingFactory;
import nl.naturalis.nba.common.es.map.MappingInfo;

/**
 * CalculatorUtils contains utility methods to be used by classes that
 * implement ICalculator.
 */
@SuppressWarnings("DuplicatedCode")
public class CalculatorUtils {

  /**
   * getPathToField will return a the full path to the first instance of the given field. E.g.:
   * identifications.defaultClassification.subgenus will be converted to:
   * identifications.0.defaultClassification.subgenus
   * This is because identifications is of type array and we're only interested in the first
   * item of the array.
   *
   * @param docType  the documentType of the documents
   * @param field  the field the calculator will use to collect the value from
   * @return Path
   */
  static Path getPathToField(Class<? extends IDocumentObject> docType, String field) {

    MappingInfo<? extends IDocumentObject> mapping =
        new MappingInfo<>(MappingFactory.getMapping(docType));

    try {
      Path parent = new Path(mapping.getNestedPath(field));
      String path = field.substring(parent.toString().length() + 1);
      if (PathUtil.isArray(parent, MappingFactory.getMapping(docType))) {
        // path = fieldname - parent
        return getPathToField(docType, parent.toString()).append("0").append(path);
      }
      // path = fieldname - parent
      return getPathToField(docType, parent.toString()).append(path);
    } catch (NullPointerException | NoSuchFieldException e) {
      return new Path(field);
    }
  }


}
