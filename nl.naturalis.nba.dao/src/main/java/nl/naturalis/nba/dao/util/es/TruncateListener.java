package nl.naturalis.nba.dao.util.es;

import nl.naturalis.nba.dao.DaoRegistry;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.index.reindex.BulkByScrollResponse;

/**
 * TruncateListener : listener for an asynchronous DeleteByQueryRequest
 *
 * @author Tom Gilissen
 */
class TruncateListener implements ActionListener<BulkByScrollResponse> {

  private static final Logger logger = DaoRegistry.getInstance().getLogger(ESUtil.class);

  private boolean ready = false;
  private boolean errors = false;
  private String errorMessage;

  public boolean isReady() {
    return ready;
  }

  public boolean hasErrors() {
    return errors;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  @Override
  public void onResponse(BulkByScrollResponse bulkByScrollResponse) {
    logger.info("Truncate finished in: {}", bulkByScrollResponse.getTook().toString());
    logger.info("Documents deleted: {}", bulkByScrollResponse.getTotal());
    ready = true;
  }

  @Override
  public void onFailure(Exception e) {
    logger.error("Truncate failed: {}", e.getMessage());
    errors = true;
    errorMessage = e.getMessage();
    ready = true;
  }
}
