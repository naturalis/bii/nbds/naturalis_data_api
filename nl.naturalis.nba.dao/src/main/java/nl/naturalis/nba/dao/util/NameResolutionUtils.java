package nl.naturalis.nba.dao.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.utils.ConfigObject;
import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.Logger;
import nl.naturalis.nba.api.*;
import nl.naturalis.nba.api.NameResolutionRequest.NameType;
import nl.naturalis.nba.api.NameResolutionRequest.SearchType;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.Taxon;
import static java.util.stream.Collectors.toList;
import static nl.naturalis.common.ObjectMethods.ifEmpty;
import static nl.naturalis.common.ObjectMethods.ifNull;
import static nl.naturalis.nba.api.ComparisonOperator.*;
import static nl.naturalis.nba.api.NameResolutionRequest.NameType.ACCEPTED_NAME;
import static nl.naturalis.nba.api.NameResolutionRequest.NameType.SYNONYM;
import static nl.naturalis.nba.api.NameResolutionRequest.NameType.VERNACULAR_NAME;
import static nl.naturalis.nba.dao.DaoUtil.getLogger;

class NameResolutionUtils {

  private static final Logger logger = getLogger(NameResolutionUtils.class);
  private static final ConfigObject config = DaoRegistry.getInstance().getConfiguration();

  private NameResolutionUtils() {}

  /**
   * Converts the provided NameResolutionRequest into a QuerySpec for the taxon index.
   *
   * @param nrr a NameResolutionRequest
   * @return QuerySpec
   */
  static QuerySpec convertToQuerySpec(NameResolutionRequest nrr) {
    logger.debug("Translating NameResolutionRequest into QuerySpec for taxon index");
    ComparisonOperator operator;
    switch (ifNull(nrr.getSearchType(), SearchType.CONTAINS)) {
      case EXACT:
        operator = EQUALS;
        break;
      case STARTS_WITH:
        operator = STARTS_WITH;
        break;
      case CONTAINS:
        operator = CONTAINS;
        break;
      case WHOLE_WORDS:
      default:
        operator = MATCHES;
    }
    String searchString = nrr.getSearchString();
    Set<NameType> targets = ifEmpty(nrr.getNameTypes(), EnumSet.allOf(NameType.class));
    QueryCondition first = null;
    if (targets.contains(ACCEPTED_NAME)) {
      first = new QueryCondition("acceptedName.fullScientificName", operator, searchString);
    }
    if (targets.contains(SYNONYM)) {
      QueryCondition next =
          new QueryCondition("synonyms.fullScientificName", operator, searchString);
      first = first == null ? next : first.or(next);
    }
    if (targets.contains(VERNACULAR_NAME)) {
      QueryCondition next = new QueryCondition("vernacularNames.name", operator, searchString);
      first = first == null ? next : first.or(next);
    }
    QuerySpec taxonQuery = new QuerySpec();
    taxonQuery.addCondition(first);
    taxonQuery.setFrom(ifNull(nrr.getFrom(), 0));
    taxonQuery.setSize(ifNull(nrr.getSize(), 10));
    return taxonQuery;
  }

  /**
   * Converts the provided NameResolutionRequest into a ColPlus call.
   *
   * @param nrr a NameResolutionRequest
   * @return URI
   * @throws URISyntaxException when the URI of the CoL PLus Webservice
   * (defined in nba.properties) is not a valid URI
   */
  static URI convertToColPlusRequest(NameResolutionRequest nrr) throws URISyntaxException {
    logger.debug("Translating NameResolutionRequest into call to Catalogue of Life");
    String colPlusWebService = config.get("nl.naturalis.nba.dao.colplus.nameusage.service");
    URIBuilder ub = new URIBuilder(colPlusWebService);
    ub.addParameter("q", nrr.getSearchString());
    ub.addParameter("offset", String.valueOf(ifNull(nrr.getFrom(), 0)));
    ub.addParameter("limit", String.valueOf(ifNull(nrr.getSize(), 10)));
    switch (ifNull(nrr.getSearchType(), SearchType.CONTAINS)) {
      case EXACT:
        ub.addParameter("type", "exact");
        break;
      case WHOLE_WORDS:
        ub.addParameter("type", "whole_words");
        break;
      case CONTAINS:
      case STARTS_WITH:
      default:
        ub.addParameter("type", "prefix");
        break;
    }
    ub.addParameter("fuzzy", String.valueOf(nrr.fuzzyMatching()));
    ub.addParameter("maxRank", "species");
    Set<NameType> nameTypes = ifEmpty(nrr.getNameTypes(), EnumSet.allOf(NameType.class));
    if (nameTypes.contains(ACCEPTED_NAME)) {
      ub.addParameter("content", "scientificName");
      if (!nameTypes.contains(SYNONYM)) {
        ub.addParameter("status", "accepted");
        ub.addParameter("status", "provisionally accepted");
      }
    } else if (nameTypes.contains(SYNONYM)) {
      ub.addParameter("content", "scientificName");
      ub.addParameter("status", "synonym");
      ub.addParameter("status", "ambiguous synonym");
      ub.addParameter("status", "misapplied");
      ub.addParameter("status", "_NULL"); // we are also going to look for bare names
    }
    if (nameTypes.contains(VERNACULAR_NAME)) {
      ub.addParameter("content", "vernacularName");
    }
    return ub.build();
  }

  /**
   * Converts the provided scientific names (obtained from a query on the taxon index) into an extra
   * query condition for the specimen index.
   *
   * @param names scientific names to create an extra condition with
   * @return queryCondition
   */
  static QueryCondition createExtraQueryCondition(List<ScientificName> names) {
    return names
        .stream()
        .map(NameResolutionUtils::toQueryCondition)
        .filter(Objects::nonNull)
        .reduce(new QueryCondition(false), QueryCondition::or);
  }

  /**
   * Extracts all accepted names and synonyms from the provided query result.
   *
   * @param result the query result to use a starting point
   * @return List of all ScientificNames in the given queryResult
   */
  static List<ScientificName> extractNames(QueryResult<Taxon> result) {
    return result
        .stream()
        .map(QueryResultItem::getItem)
        .map(NameResolutionUtils::extractNames)
        .flatMap(List::stream)
        .collect(toList());
  }

  /** Generates a query condition for the specimen index from a single scientific name. */
  private static QueryCondition toQueryCondition(ScientificName sn) {
    final String genus = "identifications.scientificName.genusOrMonomial";
    final String species = "identifications.scientificName.specificEpithet";
    final String infra = "identifications.scientificName.infraspecificEpithet";
    if (sn.getGenusOrMonomial() == null || sn.getSpecificEpithet() == null) {
      return null; // higher taxon, not interested
    }
    QueryCondition condition = new QueryCondition(genus, EQUALS_IC, sn.getGenusOrMonomial());
    condition.and(species, EQUALS_IC, sn.getSpecificEpithet());
    if (sn.getInfraspecificEpithet() != null) {
      condition.and(infra, EQUALS_IC, sn.getInfraspecificEpithet());
    }
    return condition;
  }

  /**
   * Extracts the accepted name and synonyms (if any) from a single taxon.
   *
   * @param t a taxon
   * @return a List containing the accepted name and synonyms (if any)
   */
  private static List<ScientificName> extractNames(Taxon t) {
    List<ScientificName> names = new ArrayList<>(5);
    names.add(t.getAcceptedName());
    if (t.getSynonyms() != null) {
      names.addAll(t.getSynonyms());
    }
    return names;
  }
}
