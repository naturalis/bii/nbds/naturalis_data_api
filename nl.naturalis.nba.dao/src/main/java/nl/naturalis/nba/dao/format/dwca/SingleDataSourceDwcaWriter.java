package nl.naturalis.nba.dao.format.dwca;

import static nl.naturalis.nba.dao.DaoUtil.getLogger;
import static nl.naturalis.nba.utils.TimeUtil.getDuration;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import nl.naturalis.common.io.REZipOutputStream;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.dao.format.DataSetConfigurationException;
import nl.naturalis.nba.dao.format.DataSetWriteException;
import nl.naturalis.nba.dao.format.Entity;
import nl.naturalis.nba.dao.util.es.IScroller;
import org.apache.logging.log4j.Logger;

/**
 * Manages the assembly and creation of DarwinCore archives. Use this class if all CSV files in the
 * archive can be generated from a single query (specified using the &lt;shared-data-source&gt;
 * element in the dataset configuration file). See also the XSD for dataset configuration files in
 * src/main/resources.
 *
 * @author Ayco Holleman
 */
@SuppressWarnings("CheckStyle")
class SingleDataSourceDwcaWriter implements IDwcaWriter {

  private static final Logger logger = getLogger(SingleDataSourceDwcaWriter.class);

  private final DwcaConfig cfg;
  private final OutputStream out;

  SingleDataSourceDwcaWriter(DwcaConfig dwcaConfig, OutputStream out) {
    this.cfg = dwcaConfig;
    this.out = out;
  }

  /** Writes a DwCA archive for a user-defined query (a&#46;k&#46;a&#46; "dynamic DwCA"). */
  @Override
  public void writeDwcaForQuery(QuerySpec query)
      throws InvalidQueryException, DataSetConfigurationException, DataSetWriteException {
    long start = System.currentTimeMillis();
    logger.info("Generating DarwinCore archive for user-defined query");
    DwcaPreparator dwcaPreparator = new DwcaPreparator(cfg);
    dwcaPreparator.prepare();
    IScroller scroller = cfg.createScroller(query);
    /*
     * OK, we are going to send bytes over the line, we're past the point
     * that we can respond with an error message if anything goes wrong
     */
    logger.info("Writing CSV file(s)");
    try (REZipOutputStream rezos = createREZipOutputStream()) {
      try {
        SingleDataSourceSearchHitHandler handler = new SingleDataSourceSearchHitHandler(cfg, rezos);
        handler.printHeaders();
        scroller.scroll(handler);
        handler.logStatistics();
      } catch (Exception e) {
        addErrorFile(rezos, e);
        return;
      }
      addMetaDataFiles(rezos, dwcaPreparator);
      String took = getDuration(start);
      logger.info("DarwinCore archive generated (took {})", took);
    } catch (Exception e) {
      logger.error(e);
    }
  }

  /**
   * Writes a DwCA archive for a predefined dataset (the query to be executed is in the XML
   * configuration file for the dataset).
   */
  @Override
  public void writeDwcaForDataSet() throws DataSetConfigurationException, DataSetWriteException {
    long start = System.currentTimeMillis();
    String fmt = "Creating DwC archive for data set \"{}\"";
    logger.info(fmt, cfg.getDataSetName());
    String nio = System.getProperty("nl.naturalis.common.io.REZipOutputStream.nio", "false");
    logger.debug("Use java.nio: " + nio.equalsIgnoreCase("true"));
    DwcaPreparator dwcaPreparator = new DwcaPreparator(cfg);
    dwcaPreparator.prepare();
    QuerySpec query = cfg.getDataSet().getSharedDataSource().getQuerySpec();
    IScroller scroller;
    try {
      scroller = cfg.createScroller(query);
    } catch (InvalidQueryException e) {
      // Not user's fault, query comes from the configuration file
      throw new DataSetConfigurationException(e);
    }
    // OK, we are going to send bytes over the line, we're past the point
    // that we can respond with an error message if anything goes wrong
    try (REZipOutputStream rezos = createREZipOutputStream()) {
      try {
        SingleDataSourceSearchHitHandler handler = new SingleDataSourceSearchHitHandler(cfg, rezos);
        logger.info("Adding CSV files");
        handler.printHeaders();
        scroller.scroll(handler);
        handler.logStatistics();
      } catch (Exception e) {
        addErrorFile(rezos, e);
        return;
      }
      addMetaDataFiles(rezos, dwcaPreparator);
      String took = getDuration(start);
      logger.info("DwC archive created (took {})", took);
    } catch (Exception e) {
      String msg = "Error creating DwC archive for dataset " + cfg.getDataSetName();
      logger.error(msg, e);
    }
  }

  private void addMetaDataFiles(REZipOutputStream rezos, DwcaPreparator dwcaPreparator)
      throws IOException, DataSetConfigurationException {
    try (ZipOutputStream zos = rezos.mergeEntries()) {
      logger.info("Adding meta.xml");
      zos.putNextEntry(new ZipEntry("meta.xml"));
      zos.write(dwcaPreparator.getMetaXml());
      logger.info("Adding eml.xml ({})", cfg.getEmlFile());
      zos.putNextEntry(new ZipEntry("eml.xml"));
      zos.write(dwcaPreparator.getEml());
    }
  }

  private static void addErrorFile(REZipOutputStream rezos, Exception e) {
    try (ZipOutputStream zos = rezos.mergeEntries()) {
      zos.putNextEntry(new ZipEntry("__ERROR__.txt"));
      e.printStackTrace(new PrintStream(zos));
    } catch (Exception e2) {
      logger.error(e2);
    }
  }

  private REZipOutputStream createREZipOutputStream()
      throws DataSetConfigurationException, IOException {
    Entity coreEntity = cfg.getCoreEntity();
    String fileName = cfg.getCsvFileName(coreEntity);
    logger.info("Core entity for REZipOutputStream: " + fileName);
    REZipOutputStream.Builder builder = REZipOutputStream.withMainEntry(fileName, out);
    HashSet<String> fileNames = new HashSet<>();
    for (Entity e : cfg.getDataSet().getEntities()) {
      // Multiple entities may get written to the same zip entry (e.g. taxa and synonyms are both
      // written to taxa.txt). So we must make sure to create unique zip entries.
      fileName = cfg.getCsvFileName(e);
      if (fileNames.contains(fileName)) {
        continue;
      }
      fileNames.add(fileName);
      if (e.getName().equals(coreEntity.getName())) {
        continue;
      }
      builder.addEntry(fileName, 1024 * 1024);
    }
    return builder.build();
  }

}
