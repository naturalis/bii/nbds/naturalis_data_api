package nl.naturalis.nba.dao.format.dwca;

import static nl.naturalis.nba.utils.TimeUtil.getDuration;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.io.REZipOutputStream;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.dao.format.DataSetConfigurationException;
import nl.naturalis.nba.dao.format.DataSetWriteException;
import nl.naturalis.nba.dao.format.Entity;
import nl.naturalis.nba.dao.util.es.FileScroller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JsonDataSourceDwcaWriter implements IDwcaWriter {

  private static final Logger logger = LogManager.getLogger(JsonDataSourceDwcaWriter.class);

  private final DwcaConfig dwcaConfig;
  private final OutputStream fos;

  JsonDataSourceDwcaWriter(DwcaConfig dwcaConfig, OutputStream fos) {
    this.dwcaConfig = dwcaConfig;
    this.fos = fos;
  }

  @SuppressWarnings("CheckStyle")
  public void writeDwcaForDataSet() throws DataSetWriteException, DataSetConfigurationException {

    long start = System.currentTimeMillis();
    logger.info("Generating DarwinCore archive from multiple data sets");
    String nio = System.getProperty("nl.naturalis.common.io.REZipOutputStream.nio", "false");
    logger.debug("Use java.nio: {}", nio.equalsIgnoreCase("true"));

    DwcaPreparator dwcaPreparator = new DwcaPreparator(dwcaConfig);
    dwcaPreparator.prepare();

    try (REZipOutputStream rezos = createREZipOutputStream()) {
      try {
        writeCsvFilesForDataSet(rezos);
        try (ZipOutputStream zos = rezos.mergeEntries()) {
          logger.info("Writing meta.xml");
          zos.putNextEntry(new ZipEntry("meta.xml"));
          zos.write(dwcaPreparator.getMetaXml());
          logger.info("Writing eml.xml ({})", dwcaConfig.getEmlFile());
          zos.putNextEntry(new ZipEntry("eml.xml"));
          zos.write(dwcaPreparator.getEml());
        }
      } catch (Exception e) {
        logger.error(ExceptionMethods.getDetailedMessage(e));
        try (ZipOutputStream zos = rezos.mergeEntries()) {
          zos.putNextEntry(new ZipEntry("__ERROR__.txt"));
          e.printStackTrace(new PrintStream(zos));
        } catch (Exception e2) {
          logger.error(ExceptionMethods.getDetailedMessage(e));
        }
      }
    } catch (IOException e1) {
      throw ExceptionMethods.uncheck(e1);
    }
    String took = getDuration(start);
    logger.info("DarwinCore archive generated (took {})", took);
    logger.info("Finished writing DarwinCore archive for multiple data sets");
  }

  private void writeCsvFilesForDataSet(REZipOutputStream rezos)
      throws DataSetConfigurationException, IOException {

    // Keeps track of whether the header for an entity has been printed already
    HashSet<String> done = new HashSet<>();
    for (Entity entity : dwcaConfig.getDataSet().getEntities()) {
      logNextEntity(entity);
      String fileName = dwcaConfig.getCsvFileName(entity);
      FileScroller scroller = new FileScroller(entity);
      JsonDataSourceLineHandler handler = handler(rezos, entity, fileName);
      if (!done.contains(fileName)) {
        handler.printHeaders();
        done.add(fileName);
      }
      scroller.scroll(handler);
      handler.logStatistics();
      fos.flush();
    }
  }

  @SuppressWarnings({"CheckStyle:AbbreviationAsWordInName", "CheckStyle"})
  private REZipOutputStream createREZipOutputStream() throws DataSetConfigurationException, IOException {

    Entity[] entities = dwcaConfig.getDataSet().getEntities();
    Entity firstEntity = entities[0];
    String fileName = dwcaConfig.getCsvFileName(firstEntity);
    REZipOutputStream.Builder builder = REZipOutputStream.withMainEntry(fileName, fos);
    // Multiple entities may get written to the same zip entry (e.g. taxa and synonyms are both
    // written to taxa.txt). So we must make sure to create only unique zip entries.
    HashSet<String> fileNames = new HashSet<>();
    for (Entity e : entities) {
      fileName = dwcaConfig.getCsvFileName(e);
      if (fileNames.contains(fileName)) {
        continue;
      }
      fileNames.add(fileName);
      if (e.getName().equals(firstEntity.getName())) {
        continue;
      }
      builder.addEntry(fileName);
    }
    return builder.build();
  }

  private static void logNextEntity(Entity e) {
    String fmt = "Writing csv file for entity {} ({})";
    logger.info(fmt, e.getName(), e.getDataSource().getDocumentType());
  }

  private static JsonDataSourceLineHandler handler(
      REZipOutputStream rezos, Entity entity, String fileName) {
    return new JsonDataSourceLineHandler(entity, fileName, rezos);
  }

  @Override
  public void writeDwcaForQuery(QuerySpec querySpec) {
    // Not implemented
  }

}
