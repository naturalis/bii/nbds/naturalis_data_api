package nl.naturalis.nba.dao;

import static nl.naturalis.nba.dao.DaoUtil.getLogger;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.utils.debug.DebugUtil.printCall;

import nl.naturalis.nba.api.IMultiMediaObjectAccess;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.NameResolutionQuerySpec;
import nl.naturalis.nba.api.QueryResult;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.dao.util.NameResolutionQueryHelper;
import org.apache.logging.log4j.Logger;

public class MultiMediaObjectDao extends NbaDao<MultiMediaObject>
    implements IMultiMediaObjectAccess {

  private static final Logger logger = getLogger(MultiMediaObjectDao.class);

  public MultiMediaObjectDao() {
    super(MULTI_MEDIA_OBJECT);
  }

  @Override
  public QueryResult<MultiMediaObject> queryWithNameResolution(NameResolutionQuerySpec query)
      throws InvalidQueryException {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("queryWithNameResolution", query));
    }
    return new NameResolutionQueryHelper(this, query).executeQuery();
  }

  @Override
  public String[] getUniqueKeyFields() {
    return new String[] {"unitID"};
  }
}
