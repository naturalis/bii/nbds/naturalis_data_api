package nl.naturalis.nba.dao.aggregation;

import static nl.naturalis.nba.dao.aggregation.AggregationQueryUtils.getAggregationFrom;
import static nl.naturalis.nba.dao.aggregation.AggregationQueryUtils.getAggregationSize;

import java.util.List;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.api.SortField;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.dao.DocumentType;

/**
 * Abstract class representing aggregation queries on a specific field and group.
 */
public abstract class GetDistinctValuesPerGroupAggregation<T extends IDocumentObject, U>
    extends GetDistinctValuesAggregation<T, U> {

  String group;
  int aggSize;
  int from;

  GetDistinctValuesPerGroupAggregation(
      DocumentType<T> dt, String field, String group, QuerySpec querySpec)
      throws InvalidQueryException {
    super(dt, field, querySpec);
    this.group = group;
    aggSize = getAggregationSize(querySpec);
    from = getAggregationFrom(querySpec);
    validateSortFields();
  }

  @Override
  void validateSortFields() throws InvalidQueryException {
    if (querySpec != null) {
      List<SortField> sortFields = querySpec.getSortFields();

      if (sortFields == null || sortFields.size() == 0) return; // there is no sorting required

      String first = sortFields.get(0).getPath().toString();
      if (!(first.equals(field) || first.equals(group))) {
        throw new InvalidQueryException(
            String.format("Field '%s' is an illegal sort field for this query.", first));
      }
      if (sortFields.size() == 1) return;

      String second = sortFields.get(1).getPath().toString();
      if (group != null && !(second.equals(field) || second.equals(group))) {
        throw new InvalidQueryException(
            String.format("Field '%s' is an illegal sort field for this query.", second));
      }

      // More than two sort fields doesn't make sense
      if (sortFields.size() > 2) {
        throw new InvalidQueryException(
            "Invalid number of sort fields. Maximum number for getDistinctValuesPerGroup queries is 2");
      }
    }
  }
}
