package nl.naturalis.nba.dao.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterOutputStream;
import org.apache.logging.log4j.Logger;
import com.esotericsoftware.kryo.kryo5.Kryo;
import com.esotericsoftware.kryo.kryo5.io.Input;
import com.esotericsoftware.kryo.kryo5.io.Output;
import com.google.common.annotations.VisibleForTesting;
import nl.naturalis.common.check.Check;
import nl.naturalis.common.path.PathWalker;
import nl.naturalis.nba.api.*;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.common.json.JsonDeserializationException;
import nl.naturalis.nba.dao.NbaDao;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.translate.QuerySpecTranslator;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static nl.naturalis.common.CollectionMethods.shrink;
import static nl.naturalis.common.check.CommonChecks.notEmpty;
import static nl.naturalis.nba.dao.DaoUtil.createSearchResult;
import static nl.naturalis.nba.dao.DaoUtil.getLogger;

public class BatchQueryHelper<T extends IDocumentObject> {

  /*
   * Make sure it's false!! But nice for quick testing if set to true
   */
  static final boolean URL_ENCODE = false;

  private static final Logger logger = getLogger(NbaDao.class);

  private static final Kryo kryo = configureKryo(new Kryo());

  private final NbaDao<T> dao;

  public BatchQueryHelper(NbaDao<T> dao) {
    this.dao = dao;
  }

  public BatchQueryResult<T> callFirst(QuerySpec query) throws InvalidQueryException {
    BatchQuerySpec batchQuery = new BatchQuerySpec(query);
    /*
     * We are going to ask for one extra document. It will not be included in the query result, but
     * if we get it, it means we must generate a resumption token.
     */
    batchQuery.setSize(1 + batchQuery.getSize());
    batchQuery.setSortFields(getSortFields());
    return executeQuery(batchQuery);
  }

  public BatchQueryResult<T> callNext(String token) throws InvalidQueryException {
    Check.that(token, "token").is(notEmpty());
    BatchQuerySpec batchQuery;
    try {
      batchQuery = parseResumptionToken(token);
      batchQuery.setConstantScore(true);
      batchQuery.setSortFields(getSortFields());
    } catch (IOException | JsonDeserializationException e) {
      throw new InvalidQueryException("Error parsing resumption token");
    }
    return executeQuery(batchQuery);
  }

  /*
   * With search_after queries, the fields containing the search-after values must apparently be
   * included in the QuerySpec.fields list. So if the user excluded them, we re-include them and
   * then, once we got the result from Elasticsearch, "manually" remove them from the result. Urggh.
   */
  private BatchQueryResult<T> executeQuery(BatchQuerySpec query) throws InvalidQueryException {
    List<Path> origFields = query.getFields();
    addSearchAfterFields(query);
    QuerySpecTranslator translator = new QuerySpecTranslator(query, dao.getDocumentType());
    QueryResult<T> result = createSearchResult(translator, dao.getDocumentType());
    query.setFields(origFields);
    BatchQueryResult<T> batch;
    if (result.size() == query.getSize()) {
      List<QueryResultItem<T>> docs = shrink(result.getResultSet());
      PathWalker pw = new PathWalker(dao.getUniqueKeyFields());
      T lastDocInBatch = docs.get(docs.size() - 1).getItem();
      query.setSearchAfter(pw.readValues(lastDocInBatch));
      removeSearchAfterFields(docs, origFields);
      batch = new BatchQueryResult<>(docs);
      batch.setToken(createResumptionToken(query));
    } else { // We're done
      List<QueryResultItem<T>> docs = result.getResultSet();
      removeSearchAfterFields(docs, origFields);
      batch = new BatchQueryResult<T>(docs);
    }
    return batch;
  }

  @VisibleForTesting
  static String createResumptionToken(BatchQuerySpec batchQuery) {
    logger.info("Generating resumption token");
    byte[] buf;
    try (Output output = new Output(255, -1)) {
      kryo.writeObject(output, batchQuery.toBlob());
      buf = output.toBytes();
    }
    Deflater def = new Deflater(Deflater.BEST_COMPRESSION, true);
    ByteArrayOutputStream baos = new ByteArrayOutputStream(255);
    try (DeflaterOutputStream dos = new DeflaterOutputStream(baos, def)) {
      dos.write(buf);
    } catch (IOException e) {
      throw new DaoException(e);
    }
    byte[] base64 = Base64.getEncoder().encode(baos.toByteArray());
    String raw = new String(base64, US_ASCII);
    return URL_ENCODE ? URLEncoder.encode(raw, US_ASCII) : raw;
  }

  /*
   * NB We don't need to URL-decode here b/c we get the URL-decoded token for free from JSX-RS
   */
  @VisibleForTesting
  static BatchQuerySpec parseResumptionToken(String token) throws IOException {
    Inflater inf = new Inflater(true);
    byte[] zipped = Base64.getDecoder().decode(token.getBytes(US_ASCII));
    ByteArrayOutputStream unzipped = new ByteArrayOutputStream(zipped.length * 2);
    try (InflaterOutputStream unzipper = new InflaterOutputStream(unzipped, inf)) {
      unzipper.write(zipped);
    }
    Input input = new Input(unzipped.toByteArray());
    return BatchQuerySpec.fromBlob(kryo.readObject(input, Object[].class));
  }

  private List<SortField> getSortFields() {
    return Arrays.stream(dao.getUniqueKeyFields()).map(SortField::new).collect(toList());
  }

  private void addSearchAfterFields(BatchQuerySpec query) {
    if (query.getFields() != null) {
      Set<Path> fields = new HashSet<>(query.getFields());
      Arrays.stream(dao.getUniqueKeyFields()).map(Path::new).forEach(fields::add);
      query.setFields(new ArrayList<>(fields));
    }
  }

  private void removeSearchAfterFields(List<QueryResultItem<T>> docs, List<Path> origFields) {
    if (origFields != null) {
      Set<String> includes = origFields.stream().map(Object::toString).collect(toSet());
      Set<String> keyfields = new HashSet<>(List.of(dao.getUniqueKeyFields()));
      keyfields.removeAll(includes);
      if (!keyfields.isEmpty()) {
        PathWalker pw = new PathWalker(keyfields.toArray(String[]::new));
        Object[] allNull = new Object[keyfields.size()];
        docs.stream().map(QueryResultItem::getItem).forEach(doc -> pw.writeValues(doc, allNull));
      }
    }
  }

  private static Kryo configureKryo(Kryo kryo) {
    //    kryo.register(BatchQuerySpec.class);
    //    kryo.register(QueryCondition.class);
    //    kryo.register(Path.class);
    //    kryo.register(SortField.class);
    //    kryo.register(UnaryBooleanOperator.class);
    //    kryo.register(LogicalOperator.class);
    //    kryo.register(ComparisonOperator.class);
    kryo.register(String[].class);
    kryo.register(Object[].class);
    kryo.register(List.class);
    kryo.register(ArrayList.class);
    // TODO: Register more classes that might be need to (de)serialize for BatchQuerySpec,
    // QueryCondition, etc.
    return kryo;
  }
}
