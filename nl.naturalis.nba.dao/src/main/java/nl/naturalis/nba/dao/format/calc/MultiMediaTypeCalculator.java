package nl.naturalis.nba.dao.format.calc;

import static java.lang.String.format;
import static nl.naturalis.nba.dao.format.FormatUtil.EMPTY_STRING;

import java.util.List;
import java.util.Map;
import nl.naturalis.nba.api.Path;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.common.InvalidPathException;
import nl.naturalis.nba.common.PathUtil;
import nl.naturalis.nba.common.PathValueReader;
import nl.naturalis.nba.common.es.map.MappingFactory;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import nl.naturalis.nba.dao.format.ICalculator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The MultiMediaTypeCalculator establishes the DWCA field "type"
 * (http://purl.org/dc/elements/1.1/type) based on the serviceAccessPoints.format
 * and the following link table:
 *
 * <p>format            -> type
 *  ----------------------------------
 * "application/pdf"  -> "Text"
 * "audio/mp3"        -> "Sound"
 * "video/mp4"        -> "MovingImage"
 * "image/jpeg"       -> "StillImage"
 * "image/png"        -> "StillImage"
 *
 * <p>In theory, a MultiMediaObject can have more than one ServiceAccessPionts. In that
 * case, the calculator will return several types, seperated by the pipe character ("|").
 * If there's only one ServiceAccessPoint, just one type will be returned.
 *
 * <p>Calculator definition:
 *
 * <pre>
 * <calculator>
 *     <java-class>MultiMediaTypeCalculator</java-class>
 * </calculator>
 * </pre>
 *
 * <p>Returning just one type can also be forced, by adding am extra argument to the
 * Calculator.The argument needs to be the field serviceAccessPoints.format and pointing
 * to which one needs to be taken. E.g.:
 * <arg>serviceAccessPoints.0.format</arg> for the 1st value
 * <arg>serviceAccessPoints.1.format</arg> for the 2nd, etc...
 *
 * <p>The Calculator definition will then be something like:
 *
 * <pre>
 * <calculator>
 *     <java-class>MultiMediaTypeCalculator</java-class>
 *     <arg>serviceAccessPoints.0.format</arg>
 * </calculator>
 * </pre>
 */
public class MultiMediaTypeCalculator implements ICalculator {

  private PathValueReader pathValueReader;

  private static final Logger logger = LogManager.getLogger(MultiMediaTypeCalculator.class);

  @Override
  public void initialize(Class<? extends IDocumentObject> docType, Map<String, String> args)
      throws CalculatorInitializationException {
    if (args == null || args.size() == 0) {
      return;
    }
    if (args.size() > 1) {
      throw new CalculatorInitializationException(
          "You can only provide 1 argument for the MulitMediaType Calculator!");
    }
    String arg = args.values().iterator().next();
    Path path = new Path(arg);
    try {
      PathUtil.validate(path, MappingFactory.getMapping(docType));
    } catch (InvalidPathException e) {
      String msg = format("Invalid path in DWCA Config: %s", arg);
      throw new CalculatorInitializationException(msg);
    }
    pathValueReader = new PathValueReader(path);
  }

  @Override
  public String calculateValue(EntityObject entity) {

    // DWCA Config provides an argument. E.g.: <arg>serviceAccessPoints.0.format</arg>
    if (pathValueReader != null) {
      try {
        Object obj = pathValueReader.read(entity.getDocument());
        if (obj != null) {
          String format = obj.toString();
          return getType(format);
        }
        return EMPTY_STRING;
      } catch (InvalidPathException e) {
        // A check for the validity of this field has been done in the initialiser,
        // so we do not need have to deal with errors here again.
        logger.error(e.getMessage());
      }
    }

    // DWCA Config has no argument
    MultiMediaObject multiMediaObject = (MultiMediaObject) entity.getDocument();
    List<ServiceAccessPoint> saps = multiMediaObject.getServiceAccessPoints();
    if (saps == null) {
      return EMPTY_STRING;
    }
    StringBuilder sb = new StringBuilder(15 * saps.size());
    int i = 0;
    String format;
    for (ServiceAccessPoint sap : saps) {
      format = sap.getFormat();
      sb.append(getType(format));
      if (++i < saps.size()) {
        sb.append('|');
      }
    }
    return sb.toString();
  }

  private static String getType(String format) {
    String type;
    switch (format) {
      case "application/pdf":
        type = "Text";
        break;
      case "audio/mp3":
      case "audio/wav":
        type = "Sound";
        break;
      case "video/mp4":
        type = "MovingImage";
        break;
      default:
        type = "StillImage";
    }
    return type;
  }
}
