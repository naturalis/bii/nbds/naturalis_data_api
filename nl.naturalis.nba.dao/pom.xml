<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.naturalis.nba</groupId>
    <artifactId>nba-motherpom</artifactId>
    <version>2.30-SNAPSHOT</version>
  </parent>

  <artifactId>nl.naturalis.nba.dao</artifactId>
  <name>NBA DAO layer</name>
  <description>The data access layer of the NBA. Regulates interaction with the Elasticsearch backend</description>

  <properties>
    <main.basedir>${project.parent.basedir}</main.basedir>
  </properties>

  <dependencies>

    <dependency>
      <groupId>nl.naturalis.nba</groupId>
      <artifactId>nl.naturalis.nba.api</artifactId>
      <version>2.30-SNAPSHOT</version>
    </dependency>

    <dependency>
      <groupId>nl.naturalis.nba</groupId>
      <artifactId>nl.naturalis.nba.utils</artifactId>
      <version>2.30-SNAPSHOT</version>
    </dependency>

    <dependency>
      <groupId>nl.naturalis.nba</groupId>
      <artifactId>nl.naturalis.nba.common</artifactId>
      <version>2.30-SNAPSHOT</version>
    </dependency>

    <dependency>
      <groupId>nl.naturalis.common</groupId>
      <artifactId>naturalis-common</artifactId>
    </dependency>

    <!-- JAXB API -->
    <dependency>
      <groupId>javax.xml.bind</groupId>
      <artifactId>jaxb-api</artifactId>
    </dependency>

    <!-- JAXB Runtime : This dependency is needed, even though "mvn dependency:analyze" tells us it isn't -->
    <dependency>
      <groupId>org.glassfish.jaxb</groupId>
      <artifactId>jaxb-runtime</artifactId>
      <version>2.3.3</version>
    </dependency>

      <!-- APACHE -->
    <dependency>
      <groupId>commons-codec</groupId>
      <artifactId>commons-codec</artifactId>
      <version>1.15</version>
    </dependency>

    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient</artifactId>
      <exclusions>
        <exclusion>
          <groupId>commons-codec</groupId>
          <artifactId>commons-codec</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpcore</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.lucene</groupId>
      <artifactId>lucene-core</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.lucene</groupId>
      <artifactId>lucene-join</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-text</artifactId>
    </dependency>

    <!-- Elasticsearch -->
    <dependency>
      <groupId>org.elasticsearch.client</groupId>
      <artifactId>elasticsearch-rest-client</artifactId>
    </dependency>

    <dependency>
      <groupId>org.elasticsearch.client</groupId>
      <artifactId>elasticsearch-rest-high-level-client</artifactId>
    </dependency>

    <dependency>
        <groupId>org.elasticsearch</groupId>
        <artifactId>elasticsearch-x-content</artifactId>
    </dependency>

    <dependency>
        <groupId>org.elasticsearch</groupId>
        <artifactId>elasticsearch-core</artifactId>
    </dependency>

    <dependency>
        <groupId>org.elasticsearch</groupId>
        <artifactId>elasticsearch-geo</artifactId>
    </dependency>

    <!-- Jackson -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-core</artifactId>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-annotations</artifactId>
    </dependency>

    <dependency>
      <groupId>de.grundid.opendatalab</groupId>
      <artifactId>geojson-jackson</artifactId>
    </dependency>

    <!-- Other -->
    <dependency>
        <groupId>com.google.guava</groupId>
        <artifactId>guava-annotations</artifactId>
    </dependency>

    <dependency>
      <groupId>com.univocity</groupId>
      <artifactId>univocity-parsers</artifactId>
    </dependency>

    <dependency>
      <groupId>com.esotericsoftware.kryo</groupId>
      <artifactId>kryo5</artifactId>
    </dependency>

    <!-- TEST SCOPE -->

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>

  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>exec-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>delete-api-install-dir</id>
            <phase>clean</phase>
            <goals>
              <goal>exec</goal>
            </goals>
            <configuration>
              <executable>rm</executable>
              <arguments>
                <argument>-rf</argument>
                <argument>${nba.api.install.dir}</argument>
              </arguments>
            </configuration>
          </execution>
          <execution>
            <id>make-es-dir</id>
            <phase>clean</phase>
            <goals>
              <goal>exec</goal>
            </goals>
            <configuration>
              <executable>mkdir</executable>
              <commandlineArgs>-p "${nba.api.install.dir}/es"</commandlineArgs>
            </configuration>
          </execution>
          <execution>
            <id>copy-docker-compose-yml</id>
            <phase>clean</phase>
            <goals>
              <goal>exec</goal>
            </goals>
            <configuration>
              <executable>cp</executable>
              <arguments>
                <argument>${project.basedir}/../docker-compose.yml.tpl</argument>
                <argument>${nba.api.install.dir}/es/docker-compose.yml</argument>
              </arguments>
            </configuration>
          </execution>

          <execution>
            <id>clone-dwca-config-repo</id>
            <phase>clean</phase>
            <goals>
              <goal>exec</goal>
            </goals>
            <configuration>
              <executable>git</executable>
              <arguments>
                <argument>clone</argument>
                <argument>https://github.com/naturalis/DWCAConfig</argument>
                <argument>${nba.api.install.dir}/dwca</argument>
              </arguments>
            </configuration>
          </execution>
          <execution>
            <id>clone-nba-fieldinfo-metadata-repo</id>
            <phase>clean</phase>
            <goals>
              <goal>exec</goal>
            </goals>
            <configuration>
              <executable>git</executable>
              <arguments>
                <argument>clone</argument>
                <argument>https://gitlab.com/naturalis/bii/nbds/nba-fieldinfo-metadata.git</argument>
                <argument>${nba.api.install.dir}/metadata</argument>
              </arguments>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <forkCount>1</forkCount>
          <reuseForks>false</reuseForks>
          <redirectTestOutputToFile>false</redirectTestOutputToFile>
          <argLine>-Xmx256m
            -Dnba.conf.file=${project.basedir}/../nba.properties
            -Delasticsearch.index.default.suffix=_integration_test
            -Dlog4jConfigurationFile=${project.basedir}/../log4j2-test.xml
          </argLine>
          <runOrder>random</runOrder>
          <includes>
            <include>**/*Test.java</include>
          </includes>
          <excludes>
            <!-- Still no solid way to run test classes of Java client -->
            <exclude>**/nl.naturalis.nba.client.*.java</exclude>
          </excludes>
        </configuration>
      </plugin>

    </plugins>
  </build>

</project>